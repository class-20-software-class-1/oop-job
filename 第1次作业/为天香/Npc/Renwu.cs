﻿using System;

namespace ConsoleApp1
{
    class Renwu : Npc
    {
        private string _task;

        public string Task { get => _task; set => _task = value; }
        public Renwu() { }
        public Renwu(string name, string type, string task)
        {
            this.Name = name;
            this.Type = type;
            this.Task = task;

        }
        public override void Function()
        {
            Console.WriteLine("你好，旅行者，我是"+this.Name+"我听说"+this.Task);
        }
    }
}
