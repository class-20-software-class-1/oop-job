﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp3
{

    class CookRobot : Robot
    {
        enum cooktype
        {
            川菜,
            湘菜,
            粤菜

        }
        public CookRobot() { }
        public override void Write()
        {
            Console.WriteLine("我们的机器人会以下的菜请选择"+cooktype.川菜+cooktype.湘菜+cooktype.粤菜);
        }
    }
}
