﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    class IronNPC : NPC
    {
        private string rw;
        public string Rw
        {
            get { return this.rw; }
            set { this.rw = value; }
        }
        public IronNPC(string name,NPCType type,string rw) :base(name, type) {
            this.rw = rw;
        }
        public override void Speak()
        {
            Console.WriteLine("我这有铁锹，NPC:{0},任务：{1}",this.Name,this.rw);
        }
    }
}
