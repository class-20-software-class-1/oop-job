﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    enum NPCType
        {/// <summary>
        /// 任务NPC
        /// </summary>
        Task,
        /// <summary>
        /// 商贩NPC
        /// </summary>
        Shop,
       /// <summary>
       /// 铁匠NPC
       /// </summary>
        Iron
    }
    abstract class NPC
    {

        private string name;
        private NPCType type;

        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }
        public NPCType Type
        {
            get { return this.type; }
            set { this.type = value; }
        }
        public NPC()
        {

        }
        public NPC(string name,NPCType type) {
            this.name = name;
            this.type = type;
        }
        public abstract void Speak();
    }
}
