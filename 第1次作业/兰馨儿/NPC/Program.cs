﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    class Program
    {
        static void Main(string[] args)
        {
            TaskNPC npc1 = new TaskNPC("小红帽",NPCType.Task,"送蛋糕");
            npc1.Speak();
            ShopNPC npc2 = new ShopNPC("光头强", NPCType.Shop, "砍树");
            npc2.Speak();
            IronNPC npc3 = new IronNPC("灰太狼", NPCType.Task, "抓羊");
            npc3.Speak();
        }
    }
}
