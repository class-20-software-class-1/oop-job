﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    class ShopNPC : NPC
    {
        private string info;

        public string ShopNpc
        {
            get { return this.info; }
            set { this.info = value; }
        }
        public ShopNPC(string name,NPCType type,string taskInfo) : base(name, type)
        {
            this.info = taskInfo;
        }
        public override void Speak()
        {
            Console.WriteLine("我这有物资，NPC：{0}，任务:{1}",this.Name,this.info);
        }
    }
}
