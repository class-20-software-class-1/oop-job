﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    class TaskNPC : NPC
    {
        private string taskInfo;

        public string TaskInfo
        {
            get { return this.taskInfo; }
            set { this.taskInfo = value; }
        }
        public TaskNPC(string name, NPCType type, string taskInfo) : base(name, type)
        {
            this.taskInfo = taskInfo;
        }
        public override void Speak()
        {
            Console.WriteLine("快来帮帮我，NPC:{0},任务：{1}",this.Name,this.taskInfo);
        }
    }
}
