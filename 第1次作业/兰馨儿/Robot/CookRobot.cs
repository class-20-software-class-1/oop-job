﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//在CookRobot中添加一个代表菜的类型的字段，用枚举类型

namespace ConsoleApp1
{
    enum Menu
    {
        拍黄瓜,
        烤鱼,
        小炒肉
    }
    class CookRobot:Robot
    {
        private Menu menu;
        public CookRobot()
        {

        }

        internal Menu Menu { get => menu; set => menu = value; }

        public override void Working()
        {   
            Console.WriteLine("我是炒菜机器人,我会做{1}",Menu.小炒肉,Menu.拍黄瓜,Menu.烤鱼);
        }
    }
}
