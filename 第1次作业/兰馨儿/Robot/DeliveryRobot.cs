﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class DeliveryRobot : Robot
    {
        private int hours;
        public int Hours
        {
            get { return this.hours; }
            set { this.hours = value; }
        }
        public DeliveryRobot()
        {

        }
        public override void Working()
        {
            Console.WriteLine("我是传菜机器人,我连续工作{1}小时",this.hours);
        }
    }
}
