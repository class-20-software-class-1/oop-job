﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    enum Type
    {
        炒菜,
        传菜
    }
    abstract class Robot
    {
        private string name;
        private Type type;
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        internal Type Type { get => type; set => type = value; }

        public Robot()
        {

        }
        public Robot(string name ,Type type)
        {
            this.name = name;
            this.type = type;
        }
       
        public abstract void Working();

    }

}
