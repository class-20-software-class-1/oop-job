﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp10
{

    enum TypeNPC
    {
        Task,
        Shop,
        Iron


    }
   abstract class NPC
    {
        public string name;
        public TypeNPC type;
        public string Name {
            get { return this.name; }
            set { this.name = value; }
            
        }
        public TypeNPC Type {
            get { return this.type; }
            set { this.type = value; }
        }
         public NPC() { }
        public NPC(string name,TypeNPC type)
        {
            this.name = name;
            this.type = type;


        }
        public abstract void Speak();
        
        
    }
}
