﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp10
{
    class Shop:NPC
    {
        private string item;
        public string Item {
            get { return this.item; }
            set { this.item = value; }
        }
        public Shop(string name,TypeNPC type,string item):base(name,type) {

            this.item = item;
        }
        public override void Speak()
        {
            Console.WriteLine("瞧一瞧！看一看！NPC:{0},物品：{1}",this.name,this.item);
        }
    }
}
