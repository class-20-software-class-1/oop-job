﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp10
{
    class Task:NPC
    {
        private string taskInfo;
        public string TaskInfo {
            get { return this.taskInfo; }
            set { this.taskInfo = value; }
        }
        public Task(string name,TypeNPC type,string taskInfo) : base(name, type) {
            this.taskInfo = taskInfo;
            
        }
        public override void Speak()
        {
            Console.WriteLine("快帮帮我！NPC:{0}，任务:{1}",this.name,this.taskInfo);
        }


    }
   
}
