﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp10
{
    class Tiejaing:NPC
    {
        private string repair;
        public string Repair { get { return this.repair; } set { this.repair = value; } }
        public Tiejaing(string name,TypeNPC type,string repair):base(name,type) { this.repair = repair; }
        public override void Speak()
        {
            Console.WriteLine("有什么需要吗？NPC:{0},修复：{1}",this.name,this.repair);
        }
    }
}
