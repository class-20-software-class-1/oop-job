﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp11
{ enum Typefood {
        porridge,
        meat,
        vegetables
    }
    class CookRobot:Robot
    {
        public Typefood type;
        public Typefood Type { get { return this.type; } set { this.type = value; } }
        //public CookRobot(string name, Typefood type):base(name){
        //    this.type = type;
        //}

        public CookRobot()
        {
        }

        public override void Working()
        {
            Console.WriteLine("你好，我是做菜机器人！");
        }

    }
}
