﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    enum Work
    {
        炒菜,
        传菜
    }
    abstract class Robot
    {
        private string name;
        private Work wk;
        public string Name
        { 
            get { return this.name; }
            set { this.name = value; }
        }
        public Work Wk
        {
            get { return this.wk; }
            set { this.wk = value; }
        }
        public Robot()
        { }
        public Robot(string name,Work wk)
        {
            this.name = name;
            this.wk = wk;
        }
        public abstract void Working();
    }
}
