﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                print();
            }
        }
        public static void print()
        {
            Console.WriteLine("请选择NPC类型：1、任务 2、商贩 3、铁匠");
            int key = int.Parse(Console.ReadLine());

            switch (key)
            {
                case 1:
                    Console.WriteLine("请输入您的选择：1、送信 2、打怪 3、采集");
                    int a = int.Parse(Console.ReadLine());
                    switch (a)
                    {
                        case 1:
                            Task t = new Task("A", "任务NPC", "送信给B");
                            t.Speak();
                            break;
                        case 2:
                            Task y = new Task("C", "任务NPC", "击杀D");
                            y.Speak();
                            break;
                        case 3:
                            Task u = new Task("E", "任务NPC", "采集十个F");
                            u.Speak();
                            break;
                    }
                    break;
                case 2:
                    Console.WriteLine("请输入您的选择：1、武器 2、材料 3、食物");
                    int b = int.Parse(Console.ReadLine());
                    switch (b)
                    {
                        case 1:
                            Shop i = new Shop("A", "商贩NPC", "全场二百五一把。");
                            i.Speak();
                            break;
                        case 2:
                            Shop o = new Shop("A", "商贩NPC", "全场二百五一个。");
                            o.Speak();
                            break;
                        case 3:
                            Shop p = new Shop("A", "商贩NPC", "全场二百五一份。");
                            p.Speak();
                            break;
                    }
                    break;
                case 3:
                    Console.WriteLine("请输入您的选择：1、修补 2、强化 3、打造");
                    int c = int.Parse(Console.ReadLine());
                    switch (c)
                    {
                        case 1:
                            Iron z = new Iron("奥恩", "铁匠NPC", "小锤40，大锤80");
                            z.Speak();
                            break;
                        case 2:
                            Iron x = new Iron("奥恩", "铁匠NPC", "小锤400，大锤800");
                            x.Speak();
                            break;
                        case 3:
                            Iron v = new Iron("奥恩", "铁匠NPC", "小锤4000，大锤8000");
                            v.Speak();
                            break;
                    }
                    break;
                default:
                    break;
                }
            }
        }
    }
}
