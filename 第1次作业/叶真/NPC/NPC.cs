﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    enum NPCType
    {
        任务,
        商人,
        铁匠
    }
    abstract class NPC
    {
        private string name;

        private NPCType type;

        public string Name 
        {
            get { return this.name; }
            set { this.name = value; } 
        }

        public NPCType Type 
        {
            get { return this.type; }
            set { this.type = value; }
        }

        public NPC() { }

        public NPC(string name, NPCType type) 
        {
            this.name = name;
            this.type = type;
        }

        public abstract void Talk();
    }
}
