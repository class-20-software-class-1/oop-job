﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            TaskNPC task = new TaskNPC("小白兔",NPCType.任务,"帮我找两颗大萝卜。");
            task.Talk();

            Console.WriteLine();

            ShopNPC shop = new ShopNPC("灰太狼",NPCType.商人,"治疗药水");
            shop.Talk();

            Console.WriteLine();

            BlacksmithNPC blacksmith = new BlacksmithNPC("喜羊羊", NPCType.铁匠, "修理装备");
            blacksmith.Talk();
        }
    }
}
