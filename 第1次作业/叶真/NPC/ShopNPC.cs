﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class ShopNPC : NPC
    {
        private string item;

        public string Item 
        {
            get { return this.item; } 
            set { this.item = value; }
        }

        public ShopNPC(string name,NPCType type,string item):base(name,type)
        {
            this.item = item;
        }

        public override void Talk()
        {
            Console.WriteLine("{0}：我是一个商人，我出售{1}，你要买吗？",base.Name,this.Item);
        }
    }
}
