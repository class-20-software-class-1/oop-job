﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class TaskNPC:NPC
    {
        private string task;

        public string Task 
        {
            get { return this.task; }

            set { this.task = value; } 
        }

        public TaskNPC(string name,NPCType type,string task):base(name,type)
        {
            this.task = task;
        }

        public override void Talk()
        {
            Console.WriteLine("{0}:请帮帮我，{1}。",base.Name,this.Task);
        }
    }
}
