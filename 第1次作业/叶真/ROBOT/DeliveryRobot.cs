﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class DeliveryRobot : Robot
    {
        private int hours;

        public int Hours 
        {
            get {return this.hours; }
            set { this.hours = value; } 
        }

        public override void working(string key)
        {
            Console.WriteLine("机器人已工作{0}个小时。",key);
        }
    }
}
