﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp3
{
    class DeliveryRobot:Robot
    {
        private int hours;

        public int Hours 
        {
            get { return this.hours;  }
            set { this.hours = value; }
        }
        public DeliveryRobot() {
            hours = 5;
        }
        public override void Write()
        {
            Console.WriteLine("这个机器人已经传菜传了{0}个小时了",Hours);
        }
    }
}
