﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp3
{
    abstract class Robot
    {
        private string name;

        public string Name 
        {
            get { return this.name;  }
            set { this.name = value; }
        }
        public abstract void Write();
    }
}
