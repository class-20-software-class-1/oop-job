﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Iron2 : NPC
    {
        private string test;

        public string Test
        {
            get { return this.test; }
            set { this.test = value; }
        }

        public Iron2(string name, string type, string test) : base(name, type)
        {
            this.test = test;
        }

        public override void ASD()
        {
            Console.WriteLine("NPC:{0},交钱:{1}", this.Name, this.Test);
        }
    }
}
