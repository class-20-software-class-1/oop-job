﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    abstract class NPC
    {
        private string name;
        private string type;

        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }
        public string Type
        {
            get { return this.type; }
            set { this.type = value; }
        }

        public NPC() { }
        public abstract void ASD();

        public NPC(string name, string type)
        {
            this.name = name;
            this.type = type;
        }
    }
}
