﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{

    abstract class NPC
    {
        private string name;
        private string type;
        public string Name { get => name; set => name = value; }
        public string Type { get => type; set => type = value; }

        public abstract void Taking();

        public NPC(string name, string type)
        {

        }
    }
}

