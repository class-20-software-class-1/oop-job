﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入你想交流的NPC类型：1.任务类 2.商贩类 3.铁匠类");
            int num = int.Parse(Console.ReadLine());
            switch (num)
            {
                case 1:
                    RenWu renwu = new RenWu("呜呜呜", "任务NPC","送信","杀怪","采集");
                    renwu.Taking();
                    break;
                case 2:
                    ShangFan shangfan = new ShangFan("哇哇哇", "商贩NPC", "武器", "材料", "食物");
                    shangfan.Taking();
                    break;
                case 3:
                    TieJiang tiejiang = new TieJiang("呱呱呱", "铁匠NPC", "修补", "强化", "打造");
                    tiejiang.Taking();
                    break;

            }
        }
    }
}
