﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class RenWu : NPC
    {
        private string songxin;
        private string shaguai;
        private string caiji;
        public string Songxin { get => songxin; set => songxin = value; }
        public string Shaguai { get => shaguai; set => shaguai = value; }
        public string Caiji { get => caiji; set => caiji = value; }

        public RenWu(string name, string type, string songxin, string shaguai, string caiji) : base(name, type)
        {
            this.Name = name;
            this.Type = type;
            this.Songxin = songxin;
            this.Shaguai = shaguai;
            this.Caiji = caiji;
        }
        public override void Taking()
        {
            Console.WriteLine("我叫：{0}，我是：{1}",base.Name,base.Type);
            Console.WriteLine();
            Console.WriteLine("你能帮帮我吗？,1：{0}，2：{1}，3：{2}",this.Songxin,this.Shaguai,this.Caiji);  
        }
    }
}
