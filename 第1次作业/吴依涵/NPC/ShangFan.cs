﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class ShangFan :NPC
    {
        private string wuqi;
        private string cailiao;
        private string shiwu;

        public string Wuqi { get => wuqi; set => wuqi = value; }
        public string Cailiao { get => cailiao; set => cailiao = value; }
        public string Shiwu { get => shiwu; set => shiwu = value; }

        public ShangFan(string name, string type, string wuqi, string cailiao, string shiwu) : base(name, type)
        {
            this.Name = name;
            this.Type = type;
            this.wuqi = wuqi;
            this.cailiao = cailiao;
            this.shiwu = shiwu;
        }
        public override void Taking()
        {
            Console.WriteLine("我叫：{0}，我是：{1}", base.Name, base.Type);
            Console.WriteLine();
            Console.WriteLine("你要买东西吗？,1：{0}，2：{1}，3：{2}", this.wuqi, this.cailiao, this.shiwu);
        }
    }
}
