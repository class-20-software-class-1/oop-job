﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3 
{
    class TieJiang : NPC
    {
        private string xiubu;
        private string qianghua;
        private string dazao;
        public string Xiubu { get => xiubu; set => xiubu = value; }
        public string Qianghua { get => qianghua; set => qianghua = value; }
        public string Dazao { get => dazao; set => dazao = value; }

        public TieJiang(string name, string type, string xiubu, string qianghua, string dazao) : base(name, type)
        {
            this.Name = name;
            this.Type = type;
            this.xiubu = xiubu;
            this.qianghua = qianghua;
            this.dazao = dazao;
        }



        public override void Taking()
        {
            Console.WriteLine("我叫：{0}，我是：{1}", base.Name, base.Type);
            Console.WriteLine();
            Console.WriteLine("你要修理物品吗？,1：{0}，2：{1}，3：{2}", this.xiubu, this.qianghua, this.dazao);
        }
    }
}
