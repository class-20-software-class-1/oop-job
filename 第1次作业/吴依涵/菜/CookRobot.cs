﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    enum cai
    {
        西红柿炒蛋,
        青椒炒肉,
        梅菜扣肉,
        宫保鸡丁
    }
    class CookRobot : Robot
    {
        public override void Working(string cai)
        {
            Console.WriteLine("您点的菜是：{0}", cai);
            Console.WriteLine();
        }
    }
}
