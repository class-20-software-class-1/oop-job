﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class BlacksmithNPC : NPC
    {
        private string work;

        public string Work 
        {
            get { return this.work; }
            set { this.work = value; } 
        }

        public BlacksmithNPC(string name,NPCType type,string work):base(name,type)
        {
            this.work = work;
        }

        public override void Talk()
        {
            Console.WriteLine("{0}：我是铁匠，我可以帮你{1}。",base.Name,this.work);
        }
    }
}
