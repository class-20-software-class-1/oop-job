﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    abstract class Npc
    {
        private string name;
        private string type;
        private string info;

        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }
        public string Type
        {
            get { return this.type; }
            set { this.name = value; }
        }
        public string Info
        {
            get { return this.info; }
            set { this.info = value; }
        }
        public abstract void Write();
        public Npc(string name, string type, string info)
        {
            this.name = name;
            this.type = type;
            this.info = info;
        }
    }
}