﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Task task = new Task("耶大头", "任务", "救救我");
            Shop shop = new Shop("ASH","商店","你要榴弹吗");

            NPC n;
            Console.WriteLine("请输入要查询的NPC的类型");
            string str = Console.ReadLine();

            switch (str)
            {
                case "任务":
                    n = task;
                    break;
                case "铁匠":
                    n = blacksmith;
                    break;
                case "商人":
                    n = deal;
                    break;
                default:
                    n = task;
                    break;
            }
            Test(n);
        }

        static void Test(Npc n)
        {
            n.Write();
        }
    }
}
