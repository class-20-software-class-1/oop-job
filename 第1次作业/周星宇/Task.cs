﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Task:NPC
    {
        public override void Write()
        {
            Console.WriteLine("我的名字是{0}，我是{1}NPC，{2}", Name, Type, Info);
        }
        public Task(string name, string type, string info) : base(name, type, info)
        {
        }
     }
}
