﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Class3:Class1
    {
        private string mdx;
        public string Mdx { get => mdx; set => mdx = value; }
        public Class3(string name, string lei, string mdx) : base(name, lei)
        {
            this.mdx = mdx;
        }

        

        public override void shape()
        {
            Console.WriteLine("亲爱的儿子找你的野爹买什么东西,{0}{1}：{2}", base.Name, this.Lei, this.mdx);
        }
    }
}
