﻿using System;

namespace ConsoleApp2
{
    class Program
    {
        //enum ANPC
        //{
        //    任务,
        //    商人,
        //    铁匠
        //}
        static void Main(string[] args)
        {
            

            Task task = new Task("李四","任务","帮我买点东西");
            //task.Write();

            Deal deal = new Deal("李三", "商人", "你要出售什么东西");
            //deal.Write();

            Blacksmith blacksmith = new Blacksmith("李五", "铁匠", "需要我帮你打造武器吗");
            //blacksmith.Write();

            Npc n;
            Console.WriteLine("请输入要查询的NPC的类型");
            string str = Console.ReadLine();

            switch (str)
            {
                case "任务" :
                    n = task;
                    break;
                case "铁匠":
                    n = blacksmith;
                    break;
                case "商人":
                    n = deal;
                    break;
                default:
                    n = task;
                    break;
            }
            Test(n);
           
        }

        static void Test(Npc n) 
        {
            n.Write();
        }
    }
}
