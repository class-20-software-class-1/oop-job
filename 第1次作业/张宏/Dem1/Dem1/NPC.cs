﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dem1
{
    enum NpcType
    {
        Text,
        buy,
        Tie
    }
    abstract class NPC
    {
        private string name;
        private NpcType type;

        public string Name
        {
           get { return this.name; }
           set { this.name = value; }
        }
        public NpcType Type
        {
            get { return this.type; }
            set { this.type = value; }
        }
        public NPC() { }
        public NPC(string name,NpcType type)
        {
            this.name = name;
            this.type = type;
        }

        public abstract void Tell();
    }
}
