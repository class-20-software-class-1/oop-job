﻿using System;

namespace Dem1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine();

            TeskNpc npc1 = new TeskNpc("邓夏晖",NpcType.Text,"要给你家猪猪我生一百个孩子!!!");
            npc1.Tell();

            Console.WriteLine();

            ShopNpc npc2 = new ShopNpc("张宏", NpcType.buy, "邓夏晖");
            npc2.Tell();

            Console.WriteLine();

            TIENpc npc3 = new TIENpc("邓夏晖", NpcType.Tie ,"张宏");
            npc3.Tell();
        }
    }
}
