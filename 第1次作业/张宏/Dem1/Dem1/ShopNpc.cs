﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dem1
{
    class ShopNpc:NPC
    {
        private string teakInfo2;

        public string TeakInfo2
        {
            get { return this.teakInfo2; }
            set { this.teakInfo2 = value; }
        }
        public ShopNpc(string name,NpcType type,string teakInfo2):base(name,type)
        {
            this.teakInfo2 = teakInfo2;
        }

        public override void Tell()
        {
            Console.WriteLine("宣誓人:{0},任务:我发誓会一直对{1}好的,我不会让你受一点委屈的!!!",base.Name,this.teakInfo2);
        }
    }
}
