﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dem1
{
    class TIENpc:NPC
    {
        private string teakInfo3;

        public string TeakInfo3
        {
            get { return this.teakInfo3; }
            set { this.teakInfo3 = value; }
        }
        public TIENpc(string name,NpcType type ,string teakInfo3):base(name,type)
        {
            this.teakInfo3 = teakInfo3;
        }

        public override void Tell()
        {
            Console.WriteLine("宣誓人:{0}          宣誓人{1}",base.Name,this.teakInfo3);
        }
    }
}
