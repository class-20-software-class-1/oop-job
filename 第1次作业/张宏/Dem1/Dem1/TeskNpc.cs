﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dem1
{
    class TeskNpc:NPC
    {
        private string teaskInfo;

        public string TeaskInfo
        {
            get { return this.teaskInfo; }
            set { this.teaskInfo = value; }
        }
        public TeskNpc(string name,NpcType type , string teaskInfo):base(name,type)
        {
            this.teaskInfo = teaskInfo;
        }

        public override void Tell()
        {
            Console.WriteLine("宣誓人:{0},任务:{1}",base.Name,this.teaskInfo);
        }
    }
}
