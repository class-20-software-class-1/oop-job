﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Demo2
{
    enum Caitype
    {
        beef,
        egg,
        apple
    }
    
    class CookRobot:Robot
    {
        private Caitype type;
        

        public Caitype Type
        {
            get { return this.type; }
            set { this.type = value; }
        }
        public CookRobot(string name,Caitype type):base(name)
        {
            this.type = type; 
        }

        public CookRobot(string name, Caitype type, string v) : this(name, type)
        {
            this.type = type;
        }

        public override void Reading()
        {
            Console.WriteLine("我是{0}，我要端的菜是：{1}",base.Name,this.type);
        }
    }
    
}
