﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Demo2
{
    class DeliveryRobot:Robot
    {
        private string hours;

        public string Hours 
        {
            get { return this.hours; }
            set { this.hours = value; }
        }
        public DeliveryRobot(string  name ,string hours) :base(name)
        {
            this.hours = hours;
        }

        public override void Reading()
        {
            Console.WriteLine("我是{0}，我的工作时间为{1}", base.Name, this.hours);
        }
    }
    
}
