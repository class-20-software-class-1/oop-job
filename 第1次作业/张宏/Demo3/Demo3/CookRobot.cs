﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Demo3
{
    enum CaiType
    {
        beef,
        apple,
        egg
    }
    class CookRobot:Robot
    {
        private CaiType type;
        
        public CaiType Type
        {
            get { return this.type; }
            set { this.type = value; }
        }
        public CookRobot(string name,CaiType type, string v) :base(name)
        {
            this.type = type;
        }

        public override void Working()
        {
            Console.WriteLine("我是:{0},我端的菜是:{1}",base.Name,this.type);
        }
    }
}
