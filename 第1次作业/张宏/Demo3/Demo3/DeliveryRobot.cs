﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Demo3
{
    class DeliveryRobot:Robot
    {
        private string hours;

        public string Hours
        {
            get { return this.hours; }
            set { this.hours = value; }
        }
        public DeliveryRobot(string name, CaiType beef, string hours):base(name)
        {
            this.hours = hours;
        }

        public override void Working()
        {
            Console.WriteLine("主人,我是:{0},我要端的菜是:{1}",base.Name,this.hours);
        }
    }
}
