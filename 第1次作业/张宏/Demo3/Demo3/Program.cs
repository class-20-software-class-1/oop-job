﻿using System;

namespace Demo3
{
    class Program
    {
        static void Main(string[] args)
        {
            CookRobot rot1 = new CookRobot("小爱同学机器人",CaiType.apple, "苹果");
            rot1.Working();

            DeliveryRobot rot2 = new DeliveryRobot("杨贵妃机器人", CaiType.beef, "牛肉");
            rot2.Working();
        }
    }
}
