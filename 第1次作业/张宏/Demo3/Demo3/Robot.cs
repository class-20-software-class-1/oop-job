﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Demo3
{
    abstract class Robot
    {
        private string name;

        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }
        public Robot() { }
        public Robot(string name)
        {
            this.name = name;
        }
        public abstract void Working();
    }
}
