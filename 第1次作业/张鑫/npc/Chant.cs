﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Chant : NPC
    {
        public Chant(string name, string taskInfo ,NpcType npcType)
            :base(name, taskInfo, npcType)
        { 
            
        }
        public override void speak()
        {
            Console.WriteLine("{0}：{1}", npctype, name);

            Console.WriteLine("你好，能帮我点忙吗？");
            Console.WriteLine("接取任务");
            Console.WriteLine(taskInfo);
        }
    }
}
