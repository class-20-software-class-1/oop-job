﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    enum NpcType
    {
        任务 = 1,
        商贩,
        铁匠
    }
    abstract class NPC
    {
        protected string name;
        protected string taskInfo;
        protected NpcType npctype;
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }
        public string TaskInfo
        {
            get { return this.taskInfo; }
            set { this.taskInfo = value; }
        }
        public NpcType Npctype
        {
            get { return this.npctype; }
            set { this.npctype = value; }
        }

        public NPC(string name, string taskInfo, NpcType npcType)
        {
            this.name = name;
            this.taskInfo = taskInfo;
            this.npctype = npcType;
        }
        public abstract void speak();

    }




}
