﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Smith : NPC
    {

        public Smith(string name, string taskInfo, NpcType npcType)
            : base(name, taskInfo, npcType)
        {

        }

        public override void speak()
        {
            Console.WriteLine(npctype + "：" + name);

            Console.WriteLine("那边的，过来一下");
            Console.WriteLine("接取任务");
            Console.WriteLine("来帮我个忙，给你报酬。" + taskInfo);
        }
    }
}
