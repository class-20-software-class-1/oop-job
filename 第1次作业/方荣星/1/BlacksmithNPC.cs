﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class BlacksmithNPC : NPCfather
    {
        private string weapon;

 
        public string Weapon { get => weapon; set => weapon = value; }
        public BlacksmithNPC(string name, NPCtyep tyep, string weapon) : base(name, tyep)
        {
            this.weapon = weapon;
        }
        public override void SayHi()
        {
            Console.WriteLine("我是{0}，我修理{1}",this.Name,this.Weapon);
        }
    }
}
