﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    enum NPCtyep
    {
        TaskNPC,
        ShotNPC,
        BlacksmithNPC
    }
    abstract class NPCfather
    {
      
        private string name;
        private NPCtyep tyep;

        public string Name { get => name; set => name = value; }
        private NPCtyep Tyep { get => tyep; set => tyep = value; }
        public NPCfather(string name, NPCtyep tyep) {
            this.Name = name;
            this.Tyep = tyep;
        }
        public abstract void SayHi(); 
    }
}
