﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class ShotNPC : NPCfather
    {
        private string item;

        public string Item { get => item; set => item = value; }

        public ShotNPC(string name, NPCtyep tyep, string item) : base(name, tyep)
        {
            this.Item = item;
        
        }
        public override void SayHi()
        {
            Console.WriteLine("我是{0}，我卖{1}",this.Name,this.item);
        }
    }
}
