﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class TaskNPC : NPCfather
    {
        private string taskInfo;
     

        public string TaskInfo { get => taskInfo; set => taskInfo = value; }
        public TaskNPC(string name, string taskInfo, NPCtyep tyep) : base(name, tyep)
        {
            this.TaskInfo = taskInfo;
        }

 
        public override void SayHi()
        {
            Console.WriteLine("我是{0}，任务是{1}",this.Name,this.taskInfo);
        }
    }
}
