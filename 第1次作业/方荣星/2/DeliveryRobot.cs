﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    
    class DeliveryRobot : Robot
    {
        private int hours;

        public int Hours { get => hours; set => hours = value; }
        public DeliveryRobot(string name, int hours) : base(name)
        {
            this.Hours = hours;        
        }
        public override void Working()
        {

            Console.WriteLine("你好，我是端盘机器人！");
        }

        public static explicit operator DeliveryRobot(string v)
        {
            throw new NotImplementedException();
        }
    }
}
