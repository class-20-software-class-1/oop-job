﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
   abstract class Robot
    {
        private string name;

        public string Name { get => name; set => name = value; }
        public Robot(string name) {
            this.Name = name;
        }
        public void roBot(string name)
        {
            if (name is "1")
                ((CookRobot)name).Working();
            else if (name is "2")
                ((DeliveryRobot)name).Working();


        }
        public abstract void Working();
    }
}
