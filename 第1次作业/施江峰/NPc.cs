﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    abstract class NPc
    {
        private string name;
        private string npcType;

        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }
        public string NpcTYpe
        {
            get { return this.npcType; }
            set { this.npcType = value; }
        }
        public abstract void Speak(string npcType,string name);
    }
}
