﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    abstract class Robot
    {
        private string robotname;
        public string Robotname
        {
            get { return this.robotname; }
            set { this.robotname = value; }
        }

        public abstract void Working(string robotname);
    }
}
