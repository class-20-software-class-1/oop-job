﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    enum Cookc
    { 
    梅菜扣肉,
    竹笋炒肉,
    酱猪肘子
    }
    class CookRobot : Robot
    {
        private Cookc cc;
        public Cookc Cc
        {
            get { return this.cc; }
            set { this.cc = value; }
        }
        public CookRobot()
        {
        }
        public CookRobot(string name,Cookc cc,Work wk):base(name,wk)
        {
            this.cc = cc;
        }
        public override void Working()
        {
            Console.WriteLine("你好，我是做菜机器人，我会做{0}",this.Cc);
        }
    }
}
