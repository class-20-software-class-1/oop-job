﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    //1、编写一个程序，以实现机器人的层次结构，此层次结构将至少包含抽象类机器人类Robot、炒菜机器人类CookRobot、传菜机器人类DeliveryRobot。
    //Robot类应包含机器人姓名name字段、机器人工作的方法Working()，该方法应该在子类中被实现，机器人工作的方式很多，所以Working()方法应该被定义为抽象方法。

    //可以在CookRobot中添加一个代表菜的类型的字段，用枚举类型；在DeliveryRobot中添加一个代表连续工作时长的字段hours。
    //CookRobot和DeliveryRobot应实现具体的Working()方法。

    //在主类中定义一个方法，形参数据类型是string，返回值数据类型是Robot，方法中实现:如果传入的字符串是“炒菜”，那就返回CookRobot的实例（当然是要用Robot的引用指向的），如果传入的字符串是“传菜”，那就返回DeliveryRobot的实例。

    //Main方法中：用户输入所选择的机器人的功能，根据用户的输入Robot执行对应的功能。
    class Program
    {
        /// <summary>
        /// 机器人功能枚举
        /// </summary>
        static void Main(string[] args)
        {
            //DeliveryRobot de = new DeliveryRobot();
            //CookRobot co = new CookRobot();
            while (true)
            {
                Console.WriteLine("请输入你想要做什么：");
                string want = Console.ReadLine();
                //Work wk = (Work)Enum.Parse(typeof(Work), want);
                take(want).Working();
            }
        }
        public static Robot take(string w)
        {
            Robot ro;
            switch (w)
            {
                case "炒菜":
                    ro = new CookRobot();
                    break;
                case "传菜":
                    ro = new DeliveryRobot();
                    break;
                default:
                    ro = new CookRobot();
                    break;
            }
            return ro;
            //if (w == "炒菜")
            //{
            //    ro = new CookRobot();
            //}
            //else if (w == "传菜")
            //{
            //    ro = new DeliveryRobot();
            //}
            //else 
            //{
            //    Console.WriteLine("输入错误，请重新输入！");
            //}         
        }
    }
}
