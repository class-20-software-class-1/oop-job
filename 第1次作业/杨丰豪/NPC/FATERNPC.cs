﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
   abstract class FATERNPC
    {
        private string name;
        private string name1;
        private string name2;
        private string nfcinfo;
        private string nfcinfo1;
        private string nfcinfo2;

        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }
        public string Name1
        {
            get { return this.name1; }
            set { this.name1 = value; }
        }
        public string Name2
        {
            get { return this.name2; }
            set { this.name2 = value; }
        }
        public string Nfcinfo
        {
            get { return this.nfcinfo; }
            set { this.nfcinfo = value; }
        }
        public string Nfcinfo1
        {
            get { return this.nfcinfo1; }
            set { this.nfcinfo1 = value; }
        }
        public string Nfcinfo2
        {
            get { return this.nfcinfo2; }
            set { this.nfcinfo2 = value; }
        }
        public FATERNPC(string Name,string Name1,string Name2,string Nfcinfo, string Nfcinfo1, string Nfcinfo2)
        {
            this.name = Name;
            this.name1 = Name1;
            this.name2 = Name2;
            this.nfcinfo = Nfcinfo;
            this.nfcinfo1 = Nfcinfo1;
            this.nfcinfo2 = Nfcinfo2;
        }

        public abstract void Speack();
    }
}
