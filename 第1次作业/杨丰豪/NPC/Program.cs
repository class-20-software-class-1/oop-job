﻿using ConsoleApp3;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        public enum NPCType
        {
            任务 = 1,
            商贩,
            铁匠
        }

        static void Main(string[] args)
        {
            Console.WriteLine("请输入需要的NPC：1.任务 2.商贩 3.铁匠");
            string num = Console.ReadLine();

            NPCType nPCTyp = (NPCType)NPCType.Parse(typeof(NPCType), num);
            dong(nPCTyp);
        }

        public static void dong(NPCType nPCTyp)
        {
            switch (nPCTyp)
            {
                case NPCType.任务:
                    task task = new task("小宝","小坤","小鑫", "邮件员", "冒险者","采集员");
                    task.Speack();
                    break;
                case NPCType.商贩:
                    Shop shop = new Shop("格拉姆", "拉姆", "芙蕾雅", "武器店", "材料店", "饭馆");
                    shop.Speack();
                    break;
                case NPCType.铁匠:
                    blacksmith blacksmith = new blacksmith("","","","","","");
                    blacksmith.Speack();
                    break;
                default:
                    break;
            }
        }
    }
}
        
    


