﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Shop : FATERNPC
    {
        public Shop(string name, string name1, string name2, string nfcinfo, string nfcinfo1, string nfcinfo2):base(name,name1,name2,nfcinfo,nfcinfo1,nfcinfo2)
        {

        }
        public override void Speack()
        {
            Console.WriteLine("请选择商人型NPC指令：1.武器 2.材料 3.食物");
            string str = Console.ReadLine();
            switch (str)
            {
                case "1":
                    Console.WriteLine("你好！我是{0},这里是{1},我这里有很多武器，如铁剑，斧头，长枪，法杖等等，这些常用武器我的商店都有卖！", this.Name, this.Nfcinfo);
                    break;
                case "2":
                    Console.WriteLine("你好！我是{0},这里是{1},我这里有很多各种常见的材料，请问需要什么材料？", this.Name1, this.Nfcinfo1);
                    break;
                case "3":
                    Console.WriteLine("你好！我是{0},这里是{1}，我这里有很多美食，欢迎你购买！ ", this.Name2, this.Nfcinfo2);
                    break;
                default:
                    break;
            }
        }
    }
}
