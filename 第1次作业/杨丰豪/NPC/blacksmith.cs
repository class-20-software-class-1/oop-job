﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class blacksmith : FATERNPC
    {
        public blacksmith(string name, string name1, string name2, string nfcinfo, string nfcinfo1, string nfcinfo2) : base(name, name1, name2, nfcinfo, nfcinfo1, nfcinfo2)
        {

        }
        public override void Speack()
        {
            Console.WriteLine("我是铁匠，请问你需要什么服务？1.修补 2.强化 3.打造");
            string str = Console.ReadLine();
            switch (str)
            {
                case "1":
                    Console.WriteLine("叮！叮！叮！你的武器已经修补完成");
                    break;
                case "2":
                    Console.WriteLine("叮!叮!叮!你的武器已经获得强化，武器等级+1");
                    break;
                case "3":
                    Console.WriteLine("叮！叮！叮！你需要的武器已经打造完成");
                    break;
                default:
                    break;
            }
        }
    }
}
