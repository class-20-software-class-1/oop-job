﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class CookRobot : Robot
    {
        public enum COOK
        {
            麻婆豆腐 = 1,
            糖醋排骨,
            北京烤鸭,
            螺蛳粉,
        }
        public CookRobot(string name) : base(name)
        {

        }

        public override void Working()
        {
            Console.WriteLine("我叫{0},是个炒菜机器人！",this.Name);
            Console.WriteLine("现在，请输入你需要的菜：1.麻婆豆腐 2.糖醋排骨 3.北京考鸭 4.螺蛳粉");
            string str = Console.ReadLine();
            COOK cOOK = (COOK)COOK.Parse(typeof(COOK), str);

            switch (cOOK)
            {
                case COOK.麻婆豆腐:
                    Console.WriteLine("正在制作麻婆豆腐！");
                    break;
                case COOK.糖醋排骨:
                    Console.WriteLine("正在制作糖醋排骨！");
                    break;
                case COOK.北京烤鸭:
                    Console.WriteLine("正在制作北京烤鸭！");
                    break;
                case COOK.螺蛳粉:
                    Console.WriteLine("正在制作螺蛳粉！");
                    break;
                default:
                    break;
            }
        }
    }
}