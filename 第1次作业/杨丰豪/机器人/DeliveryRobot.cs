﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public enum COOK
    {
        麻婆豆腐 = 1,
        糖醋排骨,
        北京烤鸭,
        螺蛳粉,
    }
    class DeliveryRobot:Robot
    {
        private int hours;

        public int Hours {
            get { return this.hours; }
            set { this.hours = value;}
        }

        public DeliveryRobot(string name) : base(name)
        {

        }
        public override void Working()
        {
            Console.WriteLine("我叫{0},是一个传菜机器人",this.Name);
            Console.WriteLine("请输入需要传的菜：1.麻婆豆腐 2.糖醋排骨 3.北京考鸭 4.螺蛳粉");
            string str = Console.ReadLine();
            COOK cOOK = (COOK)COOK.Parse(typeof(COOK), str);

            switch (cOOK)
            {
                case COOK.麻婆豆腐:
                    Console.WriteLine("麻婆豆腐已制作好,传菜需要{0}秒,请等待！", this.hours) ;
                    break;
                case COOK.糖醋排骨:
                    Console.WriteLine("糖醋排骨已制作好,传菜需要{0}秒,请等待！", this.hours);
                    break;
                case COOK.北京烤鸭:
                    Console.WriteLine("北京烤鸭已制作好,传菜需要{0}秒,请等待！", this.hours);
                    break;
                case COOK.螺蛳粉:
                    Console.WriteLine("螺蛳粉已制作好,传菜需要{0}秒,请等待！", this.hours);
                    break;
                default:
                    break;
            }
        }
    }
}
