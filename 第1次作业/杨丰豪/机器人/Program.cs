﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入所需要的功能：1.炒菜  2.传菜");
            string str = Console.ReadLine();

            switch (str)
            {
                case "1":
                    CookRobot cookRobot = new CookRobot("东东");
                    cookRobot.Working();
                    break;
                case "2":
                    DeliveryRobot deliveryRobot = new DeliveryRobot("昌宝");
                    deliveryRobot.Hours = 30;
                    deliveryRobot.Working();
                    break;
            }
        }
    }
}
