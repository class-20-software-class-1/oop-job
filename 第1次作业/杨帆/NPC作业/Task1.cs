﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Task1 : NPC
    {
        private string test;

        public string Test
        {
            get { return this.test; }
            set { this.test = value; }
        }

        public Task1(string name, string type, string test) : base(name, type)
        {
            this.test = test;
        }

        public override void ASD()
        {
            Console.WriteLine("NPC:{0},任务:{1}", this.Name, this.Test);
        }
    }
}
