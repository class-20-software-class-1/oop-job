﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    enum Cai
    {
        番茄炒蛋 = 1,
        蛋炒番茄,
        青椒炒肉,
        绿豆汤
    }
    class CookRobot : Robot
    {
        public CookRobot()
        {

        }
        public override void Working(string cai)
        {
            Console.WriteLine("您点的菜是{0}，请稍等。", cai);
            Console.WriteLine();
            string a = cai;
            Console.WriteLine(a);
        }
    }
}
