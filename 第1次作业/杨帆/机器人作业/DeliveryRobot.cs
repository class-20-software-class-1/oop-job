﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class DeliveryRobot : Robot
    {
        private int hours;

        public int Hours
        {
            get { return this.hours; }
            set { this.hours = value; }
        }
        public DeliveryRobot()
        {

        }
        public override void Working(string cai)
        {
            Console.WriteLine("传菜机器人已经工作了{0}个小时。",cai);
        }
    }
}
