﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        //在游戏中会出现很多种不同用途的 NPC，这些 NPC 有各自的存在的价值和作用，同时又具备一些共性的东西。
        //在开发 NPC 系统的时候，往往是需要提取共性，独立出一个父类，然后子类继承实现不同作用的 NPC。
        //分析：任务 NPC，商贩 NPC，铁匠 NPC，三种 NPC 的种类。
        //共有属性：npc 的名字，npc 的类型；
        //共有方法：都能和玩家交互(交谈)

        static void Main(string[] args)
        {
            Console.WriteLine("前面有几个人我们过去看看吧");
            Console.WriteLine("你想先看看谁？");
            Console.WriteLine("1.一个骑士 2.一位商人 3.一位大叔");
            string a = Console.ReadLine();
            ANV(a);
        }
        public static void ANV(string a)
        {
            switch (a)
            {
                case "1":
                 Task task = new Task("奇怪的骑士", "我有个朋友需要点特殊的东西，你做得出来吗", NpcType.任务);
                    task.speak();
                    break;
                case "2":
            Chant chant = new Chant("流浪商人", "听说这边有稀有食材，你能替我找来点吗", NpcType.商贩);
                    chant.speak();
                    break;
                case "3":
            Smith smith = new Smith("王叔叔", "去帮我找些矿石回来", NpcType.铁匠);
                    smith.speak();
                    break;
                default:
                    break;
            }
           


            
        }
    }
}
