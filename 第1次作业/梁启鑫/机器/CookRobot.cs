﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    enum Cook
    {
        麻婆豆腐 = 1,
        回锅肉,
        红烧肉,
        宫保鸡丁,
        酸菜鱼,
        鱼香肉丝,
        糖酥排骨
    }
    class CookRobot : Robot
    {
        public CookRobot(string name)
            : base(name)
        {

        }

        public override void wrok()
        {
            Console.WriteLine("欢迎使用，{0}竭诚为您服务", name);
            Console.WriteLine("请选择要吃的菜;");
            Console.WriteLine("麻婆豆腐、回锅肉、红烧肉、宫保鸡丁、酸菜鱼、鱼香肉丝、糖酥排骨");
            string cookName = Console.ReadLine();
            Cook cook = (Cook)Enum.Parse(typeof(Cook), cookName);
            switch (cook)
            {
                case Cook.麻婆豆腐:
                    Console.WriteLine("已完成");
                    break;
                case Cook.回锅肉:
                    Console.WriteLine("已完成");
                    break;
                case Cook.红烧肉:
                    Console.WriteLine("已完成");
                    break;
                case Cook.宫保鸡丁:
                    Console.WriteLine("已完成");
                    break;
                case Cook.酸菜鱼:
                    Console.WriteLine("已完成");
                    break;
                case Cook.鱼香肉丝:
                    Console.WriteLine("已完成");
                    break;
                case Cook.糖酥排骨:
                    Console.WriteLine("已完成");
                    break;
                default:
                    break;
            }
        }
    }
}
