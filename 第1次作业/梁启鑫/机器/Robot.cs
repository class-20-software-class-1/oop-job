﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    enum Robot1
    {
        炒菜 = 1,
        传菜
    }
    abstract class Robot
    {

        protected string name;

        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }
        public Robot(string name)
        {
            this.name = name;

        }
        public abstract void wrok();


    }
}
