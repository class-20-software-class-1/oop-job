﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo
{
    enum cai
    {
        /// <summary>
        /// 福建菜
        /// </summary>
        huer,
        /// <summary>
        /// 四川菜
        /// </summary>
        siher,
        /// <summary>
        /// 浙江菜
        /// </summary>
        zheer,
    }

    class CookRobot : Robot
    {
        public CookRobot(string name) : base(name)
        {
        }



        //可以在CookRobot中添加一个代表菜的类型的字段，用枚举类型；
        //在DeliveryRobot中添加一个代表连续工作时长的字段hours。
        //CookRobot和DeliveryRobot应实现具体的Working()方法。
        //public CookRobot(string name, int id) : base(name, id)
        //{

        //}
        public override void Working(string cai)
        {

            Console.WriteLine("你点的菜是{0}，请稍等", cai);
            Console.WriteLine();
            string b = cai;
            Console.WriteLine(b);
        }
    }
}
