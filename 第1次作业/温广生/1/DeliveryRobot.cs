﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo
{
    class DeliveryRobot : Robot
    {
        private int hours;

        public DeliveryRobot(string name) : base(name)
        {
        }

        public int Hours
        {
            get { return this.hours; }
            set { this.hours = value; }
        }
        //在DeliveryRobot中添加一个代表连续工作时长的字段hours。
        //CookRobot和DeliveryRobot应实现具体的Working()方法。

        //public DeliveryRobot(string name, int id) : base(name, id) { }
        public override void Working(string a)
        {
            Console.WriteLine("传菜机器人已经工作了{0}个小时。", a);
        }
    }
}
