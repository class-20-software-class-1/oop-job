﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo
{
    abstract class Robot
    {
        //Robot类应包含机器人姓名name字段、机器人工作的方法Working()，
        //该方法应该在子类中被实现，机器人工作的方式很多，所以Working()方法应该被定义为抽象方法。
        private string name;
       
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }
      
        public Robot(string name)
        {
            this.name = name;
     
        }
        public abstract void Working(string a);
    }
}
