﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{

     class 任务NPC : NPC父类
    {
        private string type;
        private string name;

        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }
        public string Type
        {
            get { return this.type; }
            set { this.type = value; }
        }
        public 任务NPC(string name, string type) {
            this.name = name;
            this.type = type;
        }
        //public abstract void xufangfa();
    } 
}

