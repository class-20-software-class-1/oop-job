﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo4
{
    class Program
    {
        static void Main(string[] args)
        {
            铁匠();
            商贩();
            任务();

        }
        public static void 铁匠()
        {
            铁匠 npc1 = new 铁匠("卢本伟","马飞飞",NPCType.铁匠);
            npc1.Speak();
        }
        public static void 商贩()
        {
            商贩 npc2 = new 商贩("卢本伟", "马飞飞", NPCType.商贩);
            npc2.Speak();
        }
        public static void 任务()
        {
            任务 npc3 = new 任务("卢本伟", "干翻马飞飞", NPCType.任务);
            npc3.Speak();
        }
    }
}
