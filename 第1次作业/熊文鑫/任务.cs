﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo4
{
    class 任务 : 父
    {
        private string three;


        public string Three { get => three; set => three = value; }
        public 任务(string name, string three, NPCType type) : base(name, type)
        {
            this.Three = three;
        }


        public override void Speak()
        {
            Console.WriteLine("我是{0}，任务是{1}", this.Name, this.three);
        }
    }
}
