﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo4
{
    class 商贩 : 父
    {
        private string two;

        public string Two { get => two; set => two = value; }

        public 商贩(string name, string two, NPCType type) : base(name, type)
        {
            this.Two = two;

        }
        public override void Speak()
        {
            Console.WriteLine("我是{0}，我卖{1}", this.Name, this.two);
        }
    }
}
