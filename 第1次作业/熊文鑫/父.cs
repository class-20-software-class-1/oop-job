﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo4
{
    enum NPCType
    {
        任务,
        商贩,
        铁匠
    }
    abstract class 父
    {
        private string name;
        private NPCType type;

        public string Name { get => name; set => name = value; }
        private NPCType Type { get => type; set => type = value; }

        public 父(string name,NPCType type)
        {
            this.Name = name;
            this.Type = type;
        }
        public abstract void Speak();
        
    }

}
