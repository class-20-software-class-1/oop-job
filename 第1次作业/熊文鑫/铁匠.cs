﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo4
{
    class 铁匠 : 父
    {

        private string one;

        public string One { get => one; set => one = value; }
        public 铁匠(string name,string one, NPCType type) : base(name,type)
        {
            this.one = one;
        }
        public override void Speak()
        {
            Console.WriteLine("我是{0},修理{1}", this.Name,this.One);
        }
    }
}
