﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P2
{
    class BuyNpc:NPC
    {
        public BuyNpc()
        { }
        public BuyNpc(npctype type, string name) : base(name, type)
        {

        }
        public override void talk()
        {
            Console.WriteLine("你好，我是神秘商人，请问要购买些什么？");
        }
    }
}
