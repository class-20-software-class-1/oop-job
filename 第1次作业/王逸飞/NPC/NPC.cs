﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P2
{
    
    enum npctype
    {
        铁匠,
        任务,
        商贩
    }
    abstract class NPC
    {
        private string name;
        private npctype type;
        public string Name
        { 
            get{ return this.name; }
            set { this.name = value; }  
        }
        public npctype Npctype
        {
            get { return this.type; }
            set { this.type = value; }
        }
        public NPC() { }
        public NPC(string name,npctype type)
        {
            this.name = name;
            this.type = type;
        }
        public abstract void talk();
    }
}
