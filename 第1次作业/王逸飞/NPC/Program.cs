﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P2
{
    class Program
    {
        
        static void Main(string[] args)
        {
            
            Console.WriteLine("请选择你所要对话的NPC：");
            string speak = Console.ReadLine();
            npctype nty = (npctype)Enum.Parse(typeof(npctype), speak);
            NPC npc;
            text(nty).talk();
        }
            public static NPC text(npctype n)
            {
                NPC npcc;
                switch (n)
                {
                    case npctype.任务:
                        npcc = new WorkNpc();
                        break;
                    case npctype.商贩:
                        npcc = new BuyNpc();
                        break;
                    case npctype.铁匠:
                        npcc = new TieNpc();
                        break;
                    default:
                        npcc = new WorkNpc();
                        break;
                }
                return npcc;
        }
    }
}
