﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P2
{
    enum ZBtype
    { 
        铠甲,
        头盔,
        佩剑,
        护腰
    }
    class TieNpc:NPC
    {
        public ZBtype zbtype;
        public ZBtype ZB
        {
            get { return this.zbtype; }
            set { this.zbtype = value; }
        }
        public TieNpc()
        { }
        public TieNpc(npctype type,string name, ZBtype zbtype):base(name,type)
        {
            this.zbtype = zbtype;
        }
        public override void talk()
        {
            Console.WriteLine("你好，我是铁匠，请问要打造些什么装备？");
        }
    }
}
