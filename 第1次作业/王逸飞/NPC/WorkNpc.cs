﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P2
{
    class WorkNpc:NPC
    {
        private string wk;
      public string Wkk
        {
            get { return this.wk; }
            set { this.wk = value; }
        }
        public WorkNpc()
        { }
        public WorkNpc(npctype type, string name,string wk) : base(name, type)
        {
            this.wk = wk;
        }
        public override void talk()
        {
            Console.WriteLine("你好，我是村长，请问是否要接取{0}的{1}任务？",base.Name,this.wk);
        }
    }
}
