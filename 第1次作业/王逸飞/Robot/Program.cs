﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    /
    class Program
    {
       
        static void Main(string[] args)
        {
            
            while (true)
            {
                Console.WriteLine("请输入你想要做什么：");
                string want = Console.ReadLine();
                
                take(want).Working();
            }
        }
        public static Robot take(string w)
        {
            Robot ro;
            switch (w)
            {
                case "炒菜":
                    ro = new CookRobot();
                    break;
                case "传菜":
                    ro = new DeliveryRobot();
                    break;
                default:
                    ro = new CookRobot();
                    break;
            }
            return ro;
           
        }
    }
}
