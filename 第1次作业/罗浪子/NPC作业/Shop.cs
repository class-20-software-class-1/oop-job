﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Shop : NPC
    {
        private string test;

        public string Test
        {
            get { return this.test; }
            set { this.test = value; }
        }

        public Shop(string name, string type, string test) : base(name, type)
        {
            this.test = test;
        }

        public override void ASD()
        {
            Console.WriteLine("NPC:{0},给钱:{1}", this.Name, this.Test);
        }


    }
}
