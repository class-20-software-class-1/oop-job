﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Shop : Npc
    {
        private string _huowu;

        public string Huowu { get => _huowu; set => _huowu = value; }

        public Shop() { }
        public Shop(string name, string type, string huowu)
        {
            this.Name = name;
            this.Type = type;
            this.Huowu = huowu;
        }
        public override void Function()
        {
            Console.WriteLine("我是{0}，瞧一瞧看一看咯，我这有上好的{1}",this.Name,this.Huowu);
        }
    }
}
