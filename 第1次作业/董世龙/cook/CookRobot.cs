﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class CookRobot : Robot
    {
        enum Cook
        {
            清蒸八宝猪=1,
            江米酿鸭子,
            烧子鹅
            
        }
        public CookRobot() { }
        public CookRobot(string name, string type)
        {
            this.Type = type;
            this.Name = name;
        }
        public override void Working()
        {
            Console.WriteLine("你好，我是{0}{1}",this.Type,this.Name);
            Console.WriteLine("我的菜单是：");
            Console.WriteLine("1.清蒸八宝猪");
            Console.WriteLine("2.江米酿鸭子");
            Console.WriteLine("3.烧子鹅");
            Console.WriteLine("请问您有什么需要？");
            string x = Console.ReadLine();
            
            Console.WriteLine("好的，您的"+x+"正在制作，请稍等");

        }
    }
}
