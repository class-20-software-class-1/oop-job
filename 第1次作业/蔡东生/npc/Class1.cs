﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{

    abstract class Class1
    {
        private string name;
        private string lei;

        public string Name { get => name; set => name = value; }
        public string Lei { get => lei; set => lei = value; }

        public Class1() 
        {
        }
        public Class1(string name,string lei)
        {
            this.lei = lei;
            this.name = name;
        }

        public abstract void shape();
    }
}
