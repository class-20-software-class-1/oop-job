﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Class2:Class1
    {
        private string farw;

        public string Farw { get => farw; set => farw = value; }

        public Class2(string name, string lei,string farw):base(name,lei)
        {
            this.farw = farw;
        }
        public override void shape()
        {
            Console.WriteLine("煞笔玩家给老子打工去,{0}{1}：{2}",base.Name,this.Lei,this.farw);
        }
    }
}
