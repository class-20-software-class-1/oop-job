﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Class4 : Class1
    {
        private string datie;
        public string Datie { get => datie; set => datie = value; }
        public Class4(string name, string lei, string datie) : base(name, lei)
        {
            this.datie = datie;
        }
        public override void shape()
        {
            Console.WriteLine("亲爱的儿子找你的野爹打什么装备,{0}{1}：{2}", base.Name, this.Lei, this.Datie);
        }
    }
}
