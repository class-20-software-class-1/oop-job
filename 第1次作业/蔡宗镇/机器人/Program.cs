﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("请输入需要的机器人：1、炒菜机器人 2、传菜机器人");
                string a = Console.ReadLine();
                Print(a);

            }
        }
        public static void Print(string a)
        {
            switch (a)
            {
                case "1":
                    CookRobot c = new CookRobot();
                    Console.WriteLine("请点菜：1、蛋炒饭 2、饭炒鸡蛋 3、皮蛋粥 4、瘦肉粥");
                    string b = Console.ReadLine();
                    Cai cai = (Cai)Enum.Parse(typeof(Cai), b);
                    switch (cai)
                    {
                        case (Cai)1:
                            c.Working("蛋炒饭");
                            break;
                        case (Cai)2:
                            c.Working("饭炒鸡蛋");
                            break;
                        case (Cai)3:
                            c.Working("皮蛋粥");
                            break;
                        case (Cai)4:
                            c.Working("瘦肉粥");
                            break;
                        default:
                            break;
                    }

                    break;
                case "2":
                    DeliveryRobot d = new DeliveryRobot();

                    Console.WriteLine("请输入机器人的工作时间：");
                    string l = Console.ReadLine();

                    d.Working(l);
                    break;
            }
        }
    }
}
