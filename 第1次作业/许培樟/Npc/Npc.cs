﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    abstract class Npc
    {
        private string name;
        private string type;
        private string task;

        public string Name { get => name; set => name = value; }
        public string Type { get => type; set => type = value; }
        public string Task { get => task; set => task = value; }

        public abstract void X();
        public Npc(string name,string type,string task)
        {
            this.name = name;
            this.type = type;
            this.task = task;
        }
    }
}
