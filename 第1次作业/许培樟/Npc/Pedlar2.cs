﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Pedlar2:Npc
    {
        public override void X()
        {
            Console.WriteLine("我的名字是{0}，我是{1}NPC，{2}", Name, Type, Task);
        }
        public Pedlar2(string name, string type, string task) : base(name, type, task)
        {

        }
    }
}
