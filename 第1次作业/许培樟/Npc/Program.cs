﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                print();

            }
        }
        public static void print()
        {
            Console.WriteLine("请选择NPC类型：1、任务 2、商人 3、铁匠");
            int key = int.Parse(Console.ReadLine());

            switch (key)
            {
                case 1:
                    Console.WriteLine("请输入您的选择：1、送信 2、打怪 3、采集");
                    int a = int.Parse(Console.ReadLine());
                    switch (a)
                    {
                        case 1:
                            Tasks tasks = new Tasks("张三","任务", "送信给李四");
                            tasks.X();
                            break;
                        case 2:
                            Tasks1 tasks1 = new Tasks1("张三1", "杀怪", "杀掉100只小白兔");
                            tasks1.X();
                            break;
                        case 3:
                            Tasks2 tasks2 = new Tasks2("张三2", "采集", "给李四采集蘑菇");
                            tasks2.X();
                            break;

                    }
                    break;
                case 2:
                    Console.WriteLine("请输入您的选择：1、武器 2、材料 3、食物");
                    int b = int.Parse(Console.ReadLine());
                    switch (b)
                    {
                        case 1:
                            Pedlar pedlar = new Pedlar("张三", "武器", "购买武器");
                            pedlar.X();
                            break;
                        case 2:
                            Pedlar1 pedlar1 = new Pedlar1("张三", "材料", "购买材料");
                            pedlar1.X();
                            break;
                        case 3:
                            Pedlar2 pedlar2 = new Pedlar2("张三", "食物", "购买食物");
                            pedlar2.X();
                            break;
                    }
                    break;
                case 3:
                    Console.WriteLine("请输入您的选择：1、修补 2、强化 3、打造");
                    int c = int.Parse(Console.ReadLine());
                    switch (c)
                    {
                        case 1:
                            Blacksmith blacksmith = new Blacksmith("张三", "修补", "修补武器");
                            blacksmith.X();
                            break;
                        case 2:
                            Blacksmith1 blacksmith1 = new Blacksmith1("张三", "强化", "强化道具");
                            blacksmith1.X();
                            break;
                        case 3:
                            Blacksmith2 blacksmith2 = new Blacksmith2("张三", "打造", "打造道具");
                            blacksmith2.X();
                            break;
                    }
                    break;
                default:
                    break;

            }
        }
    }
}
