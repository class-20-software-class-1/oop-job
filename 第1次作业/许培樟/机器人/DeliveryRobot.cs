﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class DeliveryRobot:Robot
    {
        //传菜机器人类
        private int hours;

        public int Hours { get => hours; set => hours = value; }
        public DeliveryRobot()
        {

        }
        public override void Working(string food)
        {
            Console.WriteLine("传菜机器人工作了{0}个小时。", food);
        }
    }
}
