﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        enum NPCtype { task,pedlar,blacksmith}
        static void Main(string[] args)
        {
            NPCtype nPCtype;
            Console.WriteLine("请选择NPC 0.任务 1.商贩 2.铁匠");
            int num = int.Parse(Console.ReadLine());
            switch (num)
            {
                case (int)NPCtype.task:
                    task a = new task();
                    a.Speak("任务型", "狗");
                    break;
                case (int)NPCtype.pedlar:
                    pedlar p = new pedlar();
                    p.Speak("商贩", "兔子");
                    break;
                case (int)NPCtype.blacksmith:
                    blacksmith b = new blacksmith();
                    b.Speak("铁匠", "猪");
                    break;
                default:
                    break;
            }
        }
    }
}
