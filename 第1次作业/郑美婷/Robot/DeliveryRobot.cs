﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class DeliveryRobot:Robot
    {
        private double hours;
        public double Hours { get => hours; set => hours = value; }
        public DeliveryRobot()
        { }
        public DeliveryRobot(double hours,string name,Work wk) : base(name,wk)
        {
            this.hours = hours;
        }
        public override void Working()
        {
            Console.WriteLine("你好，我是传菜机器人");
        }
    }
}
