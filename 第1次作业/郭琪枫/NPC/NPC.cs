﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    abstract class NPC
    {
        private string name1;
        private string name2;
        private string name3;
        private string name4;
        private string name5;
        private string name6;

        public NPC(string name1, string name2, string name3, string name4, string name5, string name6)
        {
            this.name1 = name1;
            this.name2 = name2;
            this.name3 = name3;
            this.name4 = name4;
            this.name5 = name5;
            this.name6 = name6;
        }

        public string Name1 { get => name1; set => name1 = value; }
        public string Name2 { get => name2; set => name2 = value; }
        public string Name3 { get => name3; set => name3 = value; }
        public string Name4 { get => name4; set => name4 = value; }
        public string Name5 { get => name5; set => name5 = value; }
        public string Name6 { get => name6; set => name6 = value; }
        public abstract void Speak();
    }
}
