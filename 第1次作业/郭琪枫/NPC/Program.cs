﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        public enum NPCType
        { 
            任务=1,
            商贩,
            铁匠
        }
        static void Main(string[] args)
        {
            Console.WriteLine("请输入你需要的NPC：1.任务型,2商贩型,3.铁匠型");
            string str =Console.ReadLine();

            NPCType nPCType = (NPCType)NPCType.Parse(typeof(NPCType), str);
            nPCAll(nPCType);

            }
        public static void nPCAll(NPCType nPCType)
        {
            switch (nPCType)
            {
                case NPCType.任务:
                    Task task = new Task("阿猫", "阿狗", "阿猪", "送信员", "勇者", "冒险者");
                    task.Speak();
                    break;
                case NPCType.商贩:
                    Shop shop = new Shop("康康", "迈克", "简", "武器店", "材料店", "饭馆");
                    shop.Speak();
                    break;
                case NPCType.铁匠:
                    Steel steel = new Steel("", "", "", "", "", "");
                    steel.Speak();
                    break;
                default:
                    break;
            }
        }
    }
}
