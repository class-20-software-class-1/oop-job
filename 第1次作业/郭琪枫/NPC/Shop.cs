﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Shop : NPC
    {
        public Shop(string name1, string name2, string name3, string name4, string name5, string name6)
    : base(name1, name2, name3, name4, name5, name6)
        { }
        public override void Speak()
        {
            Console.WriteLine("你好请问需要什么服务：1.武器,2.材料,3.食物");
            int num = int.Parse(Console.ReadLine());
            switch (num)
            {
                case 1:
                    Console.WriteLine("我是{0}，这里是{1}你需要武器吗?", this.Name1, this.Name4);
                    break;
                case 2:
                    Console.WriteLine("我是{0}，这里是{1}你需要材料吗?", this.Name2, this.Name5);
                    break;
                case 3:
                    Console.WriteLine("我是{0}，这里是{1}这是华莱士全家桶!", this.Name3, this.Name6);
                    break;
            }
        }
    }
}
