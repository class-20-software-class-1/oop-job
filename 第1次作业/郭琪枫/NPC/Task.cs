﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Task : NPC
    {
        public Task(string name1, string name2, string name3, string name4, string name5, string name6)
    : base(name1, name2, name3, name4, name5, name6)
        { }
        public override void Speak()
        {
            Console.WriteLine("你好请问需要什么服务：1.送信,2.杀怪,3.采集");
            int num = int.Parse(Console.ReadLine());
            switch (num)
            {
                case 1:
                    Console.WriteLine("我是{0}，是位{1}需要我帮你送信吗？", this.Name1,this.Name4);
                    break;
                case 2:
                    Console.WriteLine("我是{0}，是位{1}需要我帮你杀怪吗？", this.Name2,this.Name5);
                    break;
                case 3:
                    Console.WriteLine("我是{0}，是位{1}需要我帮你采点草药吗？", this.Name3,this.Name6);
                    break;
            }
        }
    }
}
