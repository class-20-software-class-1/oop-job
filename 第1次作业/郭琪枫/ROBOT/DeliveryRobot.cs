﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class DeliveryRobot : Robot
    {
        private int hours;
        public int Hours
        {
            get { return this.hours; }
            set { this.hours = value; }
        }
        public override void Working(string cai)
        {
            Console.WriteLine("传菜机器人连续工作时长：{0}分钟", cai);
        }
    }
}
