﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("请输入序号选择机器人为您服务：1.炒菜机器人 2.传菜机器人");
                int a = int.Parse(Console.ReadLine());
                Change(a);
            }
        }
        public static void Change(int a)
        {
            switch (a)
            {
                case 1:
                    CookRobot c = new CookRobot();
                    Console.WriteLine("请选择您的菜品：1.面线糊  2.牛肉羹  3.醋肉  4.烧肉粽");
                    string b = Console.ReadLine();
                    cai cai = (cai)Enum.Parse(typeof(cai), b);
                    switch (cai)
                    {
                        case (cai)1:
                            c.Working("面线糊");
                            break;
                        case (cai)2:
                            c.Working("牛肉羹");
                            break;
                        case (cai)3:
                            c.Working("醋肉");
                            break;
                        case (cai)4:
                            c.Working("烧肉粽");
                            break;
                        default:
                            break;
                    }
                    break;
                case 2:
                    DeliveryRobot d = new DeliveryRobot();
                    Console.WriteLine("请输入机器人传菜的时间(上菜时间)：");
                    string time = Console.ReadLine();
                    d.Working(time);
                    break;
            }
        }
    }
}
