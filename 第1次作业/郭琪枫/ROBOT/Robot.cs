﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    abstract class Robot
    {
        private string name;

        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }
        public abstract void Working(string cai);
    }
}

