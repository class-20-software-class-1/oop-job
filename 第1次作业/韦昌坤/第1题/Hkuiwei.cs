using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    enum Cook
    {
        白切鸡 = 1,
        糖醋排骨,
        红烧鱼,
        梅菜扣肉,
        鱼香肉丝,
        粉蒸肉,
        海鲜煲
    }
    class CookRobot : Robot
    {
        public CookRobot(string name)
            : base(name)
        {

        }

        public override void wrok()
        {
            Console.WriteLine("欢迎使用，{0}竭诚为您服务", name);
            Console.WriteLine("请选择要吃的菜;");
            Console.WriteLine("白切鸡、糖醋排骨、红烧鱼、梅菜扣肉、鱼香肉丝、 粉蒸肉、海鲜煲");
            string cookName = Console.ReadLine();
            Cook cook = (Cook)Enum.Parse(typeof(Cook), cookName);
            switch (cook)
            {
                case Cook.白切鸡:
                    Console.WriteLine("已完成");
                    break;
                case Cook.糖醋排骨:
                    Console.WriteLine("已完成");
                    break;
                case Cook.红烧鱼:
                    Console.WriteLine("已完成");
                    break;
                case Cook.梅菜扣肉:
                    Console.WriteLine("已完成");
                    break;
                case Cook.鱼香肉丝:
                    Console.WriteLine("已完成");
                    break;
                case Cook.粉蒸肉:
                    Console.WriteLine("已完成");
                    break;
                case Cook.海鲜煲:
                    Console.WriteLine("已完成");
                    break;
                default:
                    break;
            }
        }
    }
}