sing System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Smith : NPC
    {

        public Smith(string name, string taskInfo, NpcType npcType)
            : base(name, taskInfo, npcType)
        {

        }

        public override void speak()
        {
            Console.WriteLine(npctype + "：" + name);

            Console.WriteLine("疾风剑豪，赶紧开大招");
            Console.WriteLine("我没有风，开不了");
            Console.WriteLine("求你了，开大招吧。" + taskInfo);
        }
    }
}
