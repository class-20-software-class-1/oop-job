﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp2
{
    class Task : Npc
    {
        public override void Write()
        {
            Console.WriteLine("我的名字是{0}，我是{1}NPC，{2}",Name,Type,Info);
        }
        public Task(string name, string type,string info) : base(name, type,info)
        {

        }
    }
}
