﻿using System;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Console.WriteLine("请输入查看的机器人类型");
            string str = Console.ReadLine();
            
            Test(str).Write();
            
        }
        public static Robot Test(string str)
        {
            Robot r;
            switch (str)
            {
                case "传菜" :
                    r  = new DeliveryRobot();
                    break;
                case "炒菜":
                    r = new CookRobot();
                    break;
                default:
                    r = new CookRobot();
                    break;
            }
            return r;
        }
        static void Write(Robot r ) 
        {
            r.Write();
        }
    }
}
