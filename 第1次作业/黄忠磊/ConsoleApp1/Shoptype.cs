﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Shoptype : Npctype
    {


        public string wq { get; set; }
        public string cl { get; set; }
        public string sw { get; set; }
        public Shoptype() { }
        public Shoptype(string name, aa type, string wq, string cl, string sw) : base(name, type)
        {
            this.wq = wq;
            this.cl = cl;
            this.sw = sw;

        }
        public override void tell()
        {
            Console.WriteLine(" 姓名:{0} 类型:{1} 武器:{2} 材料:{3} 食物:{4}", this.name, this.type, this.wq, this.cl, this.sw);

        }
    }
}
