﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Smithytype : Npctype
    {
        public string xb { get; set; }
        public string qh { get; set; }
        public string dz { get; set; }
        public Smithytype() { }
        public Smithytype(string name, aa type, string xb, string qh, string dz) : base(name, type)
        {
            this.xb = xb;
            this.qh = qh;
            this.dz = dz;

        }
        public override void tell()
        {
            Console.WriteLine(" 姓名:{0} 类型:{1} 修补:{2} 强化:{3} 打造:{4}", this.name, this.type, this.xb, this.qh, this.dz);
        }
    }
}
