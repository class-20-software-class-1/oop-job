﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Tasktype : Npctype
    {
        public string sx { get; set; }
        public string sg { get; set; }
        public string cj { get; set; }
        public Tasktype() { }
        public Tasktype(string name, aa type, string sx, string sg, string cj) : base(name, type)
        {
            this.sx = sx;
            this.sg = sg;
            this.cj = cj;

        }
        public override void tell()
        {
            Console.WriteLine("姓名:{0} 类型:{1} 送信:{2}  杀怪:{3} 采集:{4}", this.name, this.type, this.sx, this.sg, this.cj);
        }
    }
}
