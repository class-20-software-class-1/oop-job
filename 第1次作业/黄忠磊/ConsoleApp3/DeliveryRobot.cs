﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class DeliveryRobot : Robot
    {
        public int hours { get; set; }

        public DeliveryRobot(int hours)
        {
            this.hours = hours;

        }
        public override void Working()
        {
            Console.WriteLine("工作时间{0}", this.hours);
        }
    }
}
