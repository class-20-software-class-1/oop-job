﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            string arr = "与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。";

            Test5();
        }
        static void Test1(string arr)
        {
            int num1 = 0;
            int num2 = 0;
            foreach (char item in arr)
            {
                if (item.Equals('类'))
                {
                    num1 += 1;
                }
                if (item.Equals('码'))
                {
                    num2 += 1;
                }
            }
            Console.WriteLine("码的个数是{0},类的个数是{1}", num1, num2);
            Console.ReadKey();
        }
        static void Test2(string arr)
        {
            string b = arr.Replace("类", "");
            int num1 = arr.Length - b.Length;
            string c = arr.Replace("码", "");
            int num2 = arr.Length - c.Length;
            Console.WriteLine("码的个数是{0},类的个数是{1}", num1, num2);
        }
        static void Test3(string a)
        {
            string[] arrr = { "类" };
            string[] arrr1 = a.Split(arrr, StringSplitOptions.RemoveEmptyEntries);
            int num1 = arrr.Length - 1;

            string[] brrr = { "码" };
            string[] brrr1 = a.Split(brrr, StringSplitOptions.RemoveEmptyEntries);
            int num2 = brrr.Length - 1;
            Console.WriteLine("码的个数是{0},类的个数是{1}", num1, num2);
        }
        static void Test5()
        {
            StringBuilder nam = new StringBuilder();

            string[] a = { };
            Console.WriteLine("输入你的姓名、年龄、家庭住址和兴趣爱好");
            Console.WriteLine("姓名");
            string Info = Console.ReadLine();
            nam.Append(Info);
            Console.WriteLine("年龄");
            Info = Console.ReadLine();
            nam.Append(Info);
            Console.WriteLine("家庭住址");
            Info = Console.ReadLine();
            nam.Append(Info);
            Console.WriteLine("兴趣爱好");
            Info = Console.ReadLine();

            Console.WriteLine(nam.Append(Info));
        }
    }
}
