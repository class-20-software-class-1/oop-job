﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Bird b = new Bird("小鸟");
            IFly fly1 = b;

            Superman s = new Superman("超人");
            IFly fly2 = s;

            Plane p = new Plane("飞机");
            IFly fly3 = p;

        }
    }
}
