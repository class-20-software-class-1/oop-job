﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。

//1、使用循环遍历的方法来实现。
//2、使用Replace方法来实现。
//3、使用Split()方法来实现。



namespace ConsoleApp5
{
    class Program
    {
            static void Main(string[] args)
                {
            //3、使用Split()方法来实现。
            string str = "与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。";
            string[] condition = { "类" };
            string[] condition1 = { "码" };
            string[] result = str.Split(condition, StringSplitOptions.None);
            string[] result1 = str.Split(condition1, StringSplitOptions.None);
            Console.WriteLine("字符串中含有类的个数为：" + (result.Length - 1));
            Console.WriteLine("字符串中含有码的个数为：" + (result1.Length - 1));

            Console.WriteLine("***************************************************");

            //2、使用Replace方法来实现。
            int lei = 0;
            int ma = 0;
            string a = str.Replace("码", "l");
            char[] arr = str.ToArray();
            for (int i = 0; i < arr.Length; i++)
            {
                if (a[i].Equals('类') == true)
                {
                    lei++;
                }
            }
            Console.WriteLine("类字的个数为：" + lei);

            string b = str.Replace("类", "m");
            char[] arr1 = str.ToArray();
            for (int i = 0; i < arr1.Length; i++)
            {
                if (b[i].Equals('码') == true)
                {
                    ma++;
                }
            }
            Console.WriteLine("码字的个数为" + ma);
            Console.WriteLine("***************************************************");
        

        //1、使用循环遍历的方法来实现。
            char[] arr2 = str.ToArray();
            int lei1 = 0;
            int ma1 = 0;
            for (int i = 0; i < arr1.Length; i++)
            {
                if (arr2[i].Equals('类') == true)
                {
                    lei1++;
                }
                if (arr2[i].Equals('码') == true)
                {
                    ma1++;
                }
            }
            Console.WriteLine("类字的个数为：" + lei1);
            Console.WriteLine("码字的个数为" + ma1);
         
        }
    }

}