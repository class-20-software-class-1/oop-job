﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


//二、
//C# (英 文名为 CSharp) 是 微  软开发的一种 面向对 象的 编程 语言。C# 语言具备了面向对象 语言 的特 征，  即封装、继承、多态，并且添加了  事件和委托，增强了 编程的灵 活性。C# 语 言是  一种安全的、稳定的、简 单 的、面向对象的编程 语言 ，其语 法与 C++ 类似，但在编程过  程中要比 C++ 简单；它不仅去掉了 C++ 和 Java 语  言中的一些复杂特性，还提  供了可视化 工具，能够高效地 编写程序。C# 是运行  在.NE  T平台之上的  编程 语言。

//去掉上面一段文字的所有空格，并统计空格数。

namespace ConsoleApp6
{
    class Program
    {
        static void Main(string[] args)
        {
            {

                string str = "C# (英 文名为 CSharp) 是 微  软开发的一种 面向对 象的 编程 语言。C# 语言具备了面向对象 语言 的特 征，  即封装、继承、多态，并且添加了  事件和委托，增强了 编程的灵 活性。C# 语 言是  一种安全的、稳定的、简 单 的、面向对象的编程 语言 ，其语 法与 C++ 类似，但在编程过  程中要比 C++ 简单；它不仅去掉了 C++ 和 Java 语  言中的一些复杂特性，还提  供了可视化 工具，能够高效地 编写程序。C# 是运行  在.NE  T平台之上的  编程 语言。";
                string[] condition = { " " };
                string[] result = str.Split(condition, StringSplitOptions.None);
                string a = str.Replace(" ", "");
                Console.WriteLine("去空格前：");
                Console.WriteLine(str);
                Console.WriteLine("去空格后：");
                Console.WriteLine(a);
                Console.WriteLine("字符串中含有空格的个数为：" + (result.Length - 1));
                Console.WriteLine();
            }
        }
    }
}
