﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Car
    {
        private string name;
        public string Name { get { return this.name; } set { this.name = value; } }
        public Car(string name)
        {
            this.name = name;
        }
        public void Go()
        {
            Console.WriteLine("{0}可以在陆地上行驶！",this.name);
        }
    }
 
}
