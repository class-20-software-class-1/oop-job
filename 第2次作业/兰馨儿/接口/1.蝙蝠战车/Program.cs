﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//1、蝙蝠战车的例子

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            Car car = new Car("宝骏");
            car.Go();

            Batmobile batmobile = new Batmobile("蝙蝠战车");
            batmobile.Go();
            batmobile.Fly();
        }
    }
}
