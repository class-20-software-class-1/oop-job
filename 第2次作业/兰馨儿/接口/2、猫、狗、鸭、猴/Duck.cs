﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Duck : Animals, ISwim
    {
        public void Swim()
        {
            Console.WriteLine("我会用翅膀游泳！");
        }
    }
}
