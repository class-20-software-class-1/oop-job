﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//2、猫、狗、鸭、猴，（吃、游泳、爬树）

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Cat cat = new Cat();
            cat.Eat();
            cat.Climbtree();

            Dog dog = new Dog();
            dog.Eat();
            dog.Swim();

            Monkey monkey = new Monkey();
            monkey.Eat();
            monkey.Climbtree();

            Duck duck = new Duck();
            duck.Eat();
            duck.Swim();

        }
    }
}
