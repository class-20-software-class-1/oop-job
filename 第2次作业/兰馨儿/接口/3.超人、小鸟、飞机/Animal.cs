﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Animal
    {
        private string brand;
        public string Brand { get { return this.brand; } set { this.brand = value; } }
        public Animal(string brand)
        {
            this.brand = brand;
        }
        public void Eat()
        {
            Console.WriteLine("{0}可以吃食物！",this.brand);
        }
    }
}
