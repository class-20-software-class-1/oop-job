﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Bird : Animal, IFlyable
    {
        public Bird(string brand) : base(brand)
        {
            
        }
        public void LayEggs()
        {
            Console.WriteLine("{0}可以下蛋！",this.Brand);
        }
        public void Fly()
        {
            Console.WriteLine("{0}可以在空中翱翔！", this.Brand);
        }

        public void Land()
        {
            Console.WriteLine("{0}可以在陆地上行走！", this.Brand);
        }

        public void TakeOff()
        {
            Console.WriteLine("{0}可以离开地面！", this.Brand);
        }
    }
}
