﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Plane : Vehicle, IFlyable
    {
        public Plane(string name) : base(name)
        {

        }
        public void CarryPassange()
        {
            Console.WriteLine("{0}可以在乘客！",this.Name);
        }
        public void Fly()
        {
            Console.WriteLine("{0}可以在空中行驶！",this.Name);
        }

        public void Land()
        {
            Console.WriteLine("{0}可以在陆地行驶！",this.Name);
        }

        public void TakeOff()
        {
            Console.WriteLine("{0}可以在离开地面！",this.Name);
        }
    }
}
