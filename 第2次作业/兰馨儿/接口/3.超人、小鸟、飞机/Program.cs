﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//4、超人、小鸟、飞机

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            Plane plane = new Plane("飞机");
            plane.TakeOff();
            plane.Fly();
            plane.Land();
            plane.CarryPassange();

            Console.WriteLine("*******************************");

            Bird bird = new Bird("老鹰");
            bird.TakeOff();
            bird.Fly();
            bird.Land();
            bird.LayEggs();
            bird.Eat();

            Console.WriteLine("*******************************");

            Superman superman = new Superman("蜘蛛侠");
            superman.Eat();
            superman.TakeOff();
            superman.Fly();
            superman.Land();
        }
    }
}
