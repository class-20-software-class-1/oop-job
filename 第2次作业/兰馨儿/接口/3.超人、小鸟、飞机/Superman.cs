﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Superman : Animal, IFlyable
    {
        public Superman(string brand) :base(brand)
        {

        }
        public void Fly()
        {
            Console.WriteLine("{0}可以在空中飞！",this.Brand);
        }

        public void Land()
        {
            Console.WriteLine("{0}可以在陆地上走！",this.Brand);
        }

        public void TakeOff()
        {
            Console.WriteLine("{0}可以离开地面！",this.Brand);
        }
    }
}
