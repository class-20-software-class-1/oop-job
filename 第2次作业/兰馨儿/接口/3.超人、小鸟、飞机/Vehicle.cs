﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Vehicle
    {
        private string name;
        public string Name { get { return this.name; } set { this.name = value; } }
        public Vehicle(string name)
        {
            this.name = name;
        }
    }
}
