﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class MobileHardDisk:Disk,IUSB
    {
        public MobileHardDisk(string brand):base(brand)
        {
            this.Brand = brand;
        }

        public void Read()
        {
            Console.WriteLine("移动硬盘{0}正在读取数据中",this.Brand);
        }

        public void Write()
        {
            Console.WriteLine("移动硬盘{0}正在写入数据中", this.Brand);
        }
    }
}
