﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            UDisk u1 = new UDisk("金士顿128G");
            MobileHardDisk m1 = new MobileHardDisk("三星");

            //u1.Read();
            //u1.Write();

            //m1.Read();
            //m1.Write();

            Computer computer = new Computer("联想");
            computer.Usb1 = u1;
            computer.Usb2 = m1;

            computer.Strat();
            computer.End();
            computer.ReadData();
            computer.WruteData();
        }
    }
}
