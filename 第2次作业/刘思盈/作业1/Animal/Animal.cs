﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{      //2、猫、狗、鸭、猴，（吃、游泳、爬树）
    class Animal
    {
        private string name;

        public string Name { get => name; set => name = value; }
        public Animal() { }
        public Animal(string name) { this.name = name; }
        public void Eat()
        {

            Console.WriteLine("{0}正在吃东西",this.name);
        }
    }
}
