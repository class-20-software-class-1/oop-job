﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Dog : Animal, Iswim
    {
        public Dog() { }
        public Dog(string name):base(name) { }
        

        public void swim()
        {
            Console.WriteLine("一只{0}正在游泳",this.Name);
            
        }
     
    }
}
