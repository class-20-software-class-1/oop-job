﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Dusk : Animal, Iswim
    {
        public Dusk() { }
        public Dusk(string name) : base(name) { }
        public void swim()
        {
            Console.WriteLine("一只{0}正在游泳", this.Name);
        }
    }
}
