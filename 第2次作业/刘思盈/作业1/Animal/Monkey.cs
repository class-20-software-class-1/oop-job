﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Monkey : Animal, Iclimbing

    {
        public Monkey() { }
        public Monkey(string name) : base(name) { }
        public void climbing()
        {
            Console.WriteLine("一只{0}正在爬树",this.Name);
        }
    }
}
