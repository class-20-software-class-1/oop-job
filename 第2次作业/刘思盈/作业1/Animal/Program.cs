﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        //2、猫、狗、鸭、猴，（吃、游泳、爬树）
        static void Main(string[] args)
        {
            Dog dog = new Dog("哈士奇");
            Monkey monkey = new Monkey("金丝猴");
            Dusk dusk = new Dusk("唐老鸭");
            Cat cat = new Cat("汤姆");
            cat.climbing();
            cat.Eat();
            dusk.swim();
            dusk.Eat();
            dog.swim();
            dog.Eat();
            monkey.climbing();
            monkey.Eat();
        }
    }
}
