﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Bird : Animal, IFlyable
    {
        public Bird(string name) : base(name) { }

        public override void Eat()
        {
            Console.WriteLine("{0}吃吃吃。。。", this.Name);
        }

        public void Fly()
        {
            Console.WriteLine("一只{0}正在飞。。。", this.Name);
        }

        public void Land()
        {
            Console.WriteLine("{0}在陆地上。。", this.Name);
        }

        public void TakeOff()
        {

            Console.WriteLine("{0}起飞中。。。", this.Name);
        }
        public void LayEggs()
        {

            Console.WriteLine("{0}，下蛋中...", this.Name);
        }
    }
}
