﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Plane : Vehicle, IFlyable
    {
        public Plane(string name) : base(name) { }
        public void Fly()
        {
            Console.WriteLine("{0}飞飞飞。。。", this.Name);
        }

        public void Land()
        {
            Console.WriteLine("{0}着陆了", this.Name);
        }

        public void TakeOff()
        {
            Console.WriteLine("{0}起飞中", this.Name);
        }
    }
}
