﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Bird bird = new Bird("麻雀");
            bird.Fly();
            bird.Eat();
            bird.Land();
            bird.TakeOff();
            bird.LayEggs();
            Console.WriteLine("---------------------------------");
            Superman su = new Superman("小超");
            su.Eat();
            su.Fly();
            su.Land();
            su.TakeOff();
            Console.WriteLine("-------------------------");
            Plane p = new Plane("壹号飞机");
            p.Fly();
            p.Land();
            p.TakeOff();


        }
    }
}
