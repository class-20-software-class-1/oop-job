﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Superman : Animal, IFlyable
    {
        public Superman(string name) : base(name) { }

        public override void Eat()
        {
            Console.WriteLine("{0}吃吃吃。。。", this.Name);

        }

        public void Fly()
        {
            Console.WriteLine("{0}正在天上飞。。。。。", this.Name);
        }

        public void Land()
        {
            Console.WriteLine("{0}在陆地上。。", this.Name);
        }

        public void TakeOff()
        {
            Console.WriteLine("{0}起飞中。。。", this.Name);
        }
    }
}
