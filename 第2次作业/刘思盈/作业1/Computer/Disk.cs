﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp12
{
    class Disk
    {
        private string brand;

        public string Brand { get => brand; set => brand = value; }
        public Disk() { }
        public Disk(string brand) { this.brand = brand; }
    }
}
