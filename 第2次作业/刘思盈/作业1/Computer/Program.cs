﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp12
{
    class Program
    {
        static void Main(string[] args)
        {
            UDisk u1 = new UDisk("金士顿64G");
            MobileHardDisk m1 = new MobileHardDisk("三星500G");
            u1.Write();
            u1.Read();
            Console.WriteLine("------------------------------------------------");
            m1.Read();
            m1.Write();
            Console.WriteLine("-----------------------------------------------");

            Computer com = new Computer("华为");
            com.Usb1 = u1;
            com.Usb2 = m1;
            com.Start();
            com.WriteData();
            com.ReadData();
            com.End();
           
        }
    }
}
