﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp13
{
    class Animal
    {
        private string name;

        public string Name { get => name; set => name = value; }
        public Animal(string name) { this.name = name; }
        public void Eat() { Console.WriteLine("一只{0}正在吃。。。",this.name); }
    }
}
