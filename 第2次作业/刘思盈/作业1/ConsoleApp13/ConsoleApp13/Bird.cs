﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp13
{
    class Bird : Animal, IFlyable
    {
        public Bird(string name) : base(name) { }
        
        
        public void Fly()
        {
            Console.WriteLine("一只{0}正在飞。。。",this.Name);   
        }
    }
}
