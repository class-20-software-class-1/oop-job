﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            //一、统计下面一段文字中“类”字和“码”的个数。
            string a = ("与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。" );
            //1、使用循环遍历的方法来实现。
            char[] arr = a.ToArray();
            int l = 0;
            int m = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (a[i].Equals('类') == true)
                {
                    l++;

                }
                if(a[i].Equals('码') == true)
                {
                    m++;
                }
              
            }
            Console.WriteLine("文字中'类'的个数：" + l);
            Console.WriteLine("文字中'码'的个数：" + m);
            Console.WriteLine("2、使用Replace方法来实现。------------------------------");
            int lll = 0;
            int mmm = 0;
            string b = a.Replace("类", "类");
            char[] arr1 = b.ToArray();
            for (int i = 0; i < arr1.Length; i++)
            {
                if (b[i].Equals('类') == true)
                {
                    lll++;
                }

            }
            Console.WriteLine("文字中'类'的个数：" + l);
            string c = a.Replace("码", "码");
            char[] arr2 = c.ToArray();
            for (int i = 0; i < arr2.Length; i++)
            {
                if (b[i].Equals('码') == true)
                {
                   mmm++;
                }
            }
            Console.WriteLine("文字中'码'的个数：" + mmm);
            Console.WriteLine("3、使用Split()方法来实现------------------------------------------------------");
            string[] d = {"类"};
            string[] D = a.Split(d, StringSplitOptions.RemoveEmptyEntries);
            Console.WriteLine("文字中'类'的个数：" +(D.Length-1));
            string[] f = {"码"};
            string[] F = a.Split(f, StringSplitOptions.RemoveEmptyEntries);
            Console.WriteLine("文字中'码'的个数：" + (F.Length));
            Console.WriteLine("--------------------------------------------------");
            //            二、C# (英 文名为 CSharp) 是 微  软开发的一种 面向对 象的 编程 语言。C# 语言具备了面向对象 语言 的特 征，  即封装、继承、多态，并且添加了  事件和委托，增强了 编程的灵 活性。C# 语 言是  一种安全的、稳定的、简 单 的、面向对象的编程 语言 ，其语 法与 C++ 类似，但在编程过  程中要比 C++ 简单；它不仅去掉了 C++ 和 Java 语  言中的一些复杂特性，还提  供了可视化 工具，能够高效地 编写程序。C# 是运行  在.NE  T平台之上的  编程 语言。

            //去掉上面一段文字的所有空格，并统计空格数。
            string g = "C# (英 文名为 CSharp) 是 微  软开发的一种 面向对 象的 编程 语言。C# 语言具备了面向对象 语言 的特 征，  即封装、继承、多态，并且添加了  事件和委托，增强了 编程的灵 活性。C# 语 言是  一种安全的、稳定的、简 单 的、面向对象的编程 语言 ，其语 法与 C++ 类似，但在编程过  程中要比 C++ 简单；它不仅去掉了 C++ 和 Java 语  言中的一些复杂特性，还提  供了可视化 工具，能够高效地 编写程序。C# 是运行  在.NE  T平台之上的  编程 语言。";
            string[] h = { "" };
            string[] H = g.Split(h, StringSplitOptions.RemoveEmptyEntries);
            string z = g.Replace(" ", "");
            Console.WriteLine("去空格后："+z);
            Console.WriteLine("空格数："+(H.Length-1));
            Console.WriteLine("--------------------------------------------------------------------------------");
            StringBuilder sb = new StringBuilder();
            sb.ToString();

        }
    }
} 
