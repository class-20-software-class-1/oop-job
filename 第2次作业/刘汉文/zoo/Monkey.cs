﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Monkey : Eat , Tree
    {
        private string name;
        public string Name { get => name; set => name = value; }

        public Monkey(string name)
        {
            Eat(name);
            Tree(name);
        }
        public void Eat(string name)
        {
            Console.WriteLine("{0}吃饭", name);
        }
        public void Tree(string name)
        {
            Console.WriteLine("{0}会爬树", name);
        }
    }
}
