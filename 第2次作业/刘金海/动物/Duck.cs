﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Duck : Eat, Swim
    {
        private string name;
        public string Name { get => name; set => name = value; }

        public Duck(string name)
        {
            Eat(name);
            Swim(name);
        }
        public void Eat(string name)
        {
            Console.WriteLine("{0}吃饭", name);
        }
        public void Swim(string name)
        {
            Console.WriteLine("{0}会游泳", name);
        }
    }
}
