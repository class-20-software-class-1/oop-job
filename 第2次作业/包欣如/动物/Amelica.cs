﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    abstract class Amelica
    {
        public Amelica()
        {
           
        }
        public Amelica(string eat)
        {
            this.eat = eat;
        }
        public string eat { get; set; }
        public abstract void tell();

    }
}
