﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
  abstract  class father
    {

        public string name { get; set; }

        protected father()
        {
        }
            protected father(string name)
        {
            this.name = name;
        }

        public abstract void tell();
    }
}
