﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Animal
    {
        private string animalName;

        public string AnimalName
        {
            get { return this.animalName; }
            set { this.animalName = value; }
        }

        public Animal(string animalname)
        {
            this.animalName = animalname;
        }

        public void Eat()
        {
            Console.WriteLine("{0}在吃饭", this.animalName);
        }

    }
}
