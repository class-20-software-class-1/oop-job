﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Monkey : Animal, IClimbTree,ISwim
    {
        public Monkey(string animalName) : base(animalName)
        {

        }

        public void Climb()
        {
            Console.WriteLine("{0}在爬树", this.AnimalName);
        }

        public void Swim()
        {
            Console.WriteLine("{0}在游泳", this.AnimalName);
        }
    }
}
