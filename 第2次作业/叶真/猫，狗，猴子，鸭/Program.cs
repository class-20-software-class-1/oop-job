﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        //猫、狗、鸭、猴，（吃、游泳、爬树）
        static void Main(string[] args)
        {
            Cat cat = new Cat("猫");
            Dog dog = new Dog("狗");
            Monkey monkey = new Monkey("猴子");
            Duck duck = new Duck("鸭");

            Eat(cat);
            Swim(cat);
            Climb(cat);

            Console.WriteLine("------------------------------------");

            Eat(dog);
            Swim(dog);

            Console.WriteLine("------------------------------------");

            Eat(monkey);
            Swim(monkey);
            Climb(monkey);

            Console.WriteLine("------------------------------------");

            Eat(duck);
            Swim(duck);
        }

        public static void Climb(IClimbTree climbTree)
        {
            climbTree.Climb();
        }

        public static void Swim(ISwim swim)
        {
            swim.Swim();
        }

        public static void Eat(Animal animal)
        {
            animal.Eat();
        }
        
    }
}
