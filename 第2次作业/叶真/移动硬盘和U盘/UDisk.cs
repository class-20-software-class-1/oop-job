﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class UDisk:MobileStorage,IUSB
    {
        public UDisk(string name,string capacity) : base(name,capacity)
        {

        }

        public void Read()
        {
            Console.WriteLine("{0}正在读取数据", this.DiskName);
        }

        public void Write()
        {
            Console.WriteLine("{0}正在写入数据", this.DiskName);
        }
    }
}
