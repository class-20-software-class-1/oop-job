﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Batmobile:Car,IFly
    {
        public Batmobile(string carName) : base(carName)
        {

        }

        public void Fly()
        {
            Console.WriteLine("{0}正在飞",this.CarName);
        }
    }
}
