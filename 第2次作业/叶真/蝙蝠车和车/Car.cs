﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Car
    {
        private string carName;

        public string CarName 
        {
            get { return this.carName; }
            set { this.carName = value; }
        }

        public Car() { }

        public Car(string carname)
        {
            this.carName = carname;
        }

        public void Run()
        {
            Console.WriteLine("{0}在奔跑中",this.carName);
        }
    }
}
