﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Animal
    {
        private string name;

        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        public Animal(string name)
        {
            this.name = name;
        }

        public void Eat()
        {
            Console.WriteLine("{0}在吃东西", this.name);
        }
    }
}
