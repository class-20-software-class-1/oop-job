﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class BatCar:Car,IFly

    {
        public BatCar(string brand) :base(brand)
        {
        }

        public void Fly()
        {
            Console.WriteLine("{0},正在路上跑", brand);
        }
    }
}
