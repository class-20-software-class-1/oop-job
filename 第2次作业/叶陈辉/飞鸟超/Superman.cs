﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Superman : Animal, IFlyable
    {
        public Superman(string eat, string name) : base(eat, name)
        {
        }
        public void Fly()
        {
            Console.WriteLine("是奥特曼！！！");
        }

        public void Land()
        {
            Console.WriteLine("歘的一下就飞走了");
        }

        public void TakeOff()
        {
            Console.WriteLine("就像阳光----");
        }
    }
}
