﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Cat:Animal,IClimbTree
    {
        public void ClimbTree()
        {
            Console.WriteLine("俺{0},还能爬树", this.Name);
        }

        public override void Eat()
        {
            //this.Name = "猫";
            Console.WriteLine("我是{0}，我能吃",this.Name);
        }
        public Cat(string name) : base(name)
        {

        }
    }
}
