﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    interface IFlyable
    {
        void Takeoff();

        void Fly();

        void Land();

    }
}
