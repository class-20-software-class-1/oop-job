﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Superman superman = new Superman("不内裤外穿的超人");

            Eat(superman);
            Fly(superman);

            Console.WriteLine("-------------分割线----------------");

            Bird bird = new Bird("抓小鸡的老鹰");

            Eat(bird);
            Fly(bird);

            Console.WriteLine("-------------分割线----------------");

            Plane plane = new Plane("不是飞机的飞鸭");

            Fly(plane);
            Tool(plane);
        }
        static void Tool(Vehicle vehicle ) 
        {
            vehicle.CarryPassange();
        }
        static void Eat(Animal animal) 
        {
            animal.Eat();
        }

        static void Fly(IFlyable flyable) 
        {
            flyable.Fly();
            flyable.Takeoff();
            flyable.Land();
        }
        
    }
    
}
