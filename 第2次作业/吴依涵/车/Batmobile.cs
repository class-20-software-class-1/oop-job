﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Batmobile:Car,IFly
    {
        public Batmobile(string stand ):base(stand)
        { 
           
        }

        public void Fly()
        {
            Console.WriteLine("一辆{0}车正在天上飞",this.Brand);
        }
    }
}
