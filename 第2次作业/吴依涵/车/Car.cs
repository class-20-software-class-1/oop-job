﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Car
    {
        private string brand;

        public string Brand
        {
            get { return this.brand; }
            set { this.brand = value; }
        }

        public Car() { }

        public Car(string brand)
        {
            this.brand = brand;
        }

        public void Run() 
        {
            Console.WriteLine("一辆{0}在大马路上跑",this.brand);
        }

        public void Fly()
        {
            Console.WriteLine("一辆{0}在飞...", this.brand);
        }
    }
}
