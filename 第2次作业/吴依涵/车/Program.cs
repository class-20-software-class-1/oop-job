﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            LanBoJiNi lanbojini = new LanBoJiNi("兰博基尼");
            lanbojini.Run();

            LaiKen laiken = new LaiKen("莱肯");
            laiken.Run();

            Batmobile batmobile = new Batmobile("蝙蝠战车");
            batmobile.Run();
            batmobile.Fly();
        }
    }
}
       