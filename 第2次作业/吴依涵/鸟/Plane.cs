﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Plane: Vehicle,IFlyable
    {

        public void TakeOff()
        {
            Console.WriteLine("我是飞机，我在起飞中。");
        }

        public void Fly()
        {
            Console.WriteLine("我是飞机，我现在在天上飞。");
        }
        public void Land()
        {
            Console.WriteLine("我是飞机，我会在陆地跑。");
        }

        public void CarryPassange()
        {
            Console.WriteLine("我是飞机，我可以运载乘客。");
        }
    }
}
