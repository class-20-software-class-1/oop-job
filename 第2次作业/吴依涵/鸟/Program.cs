﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            Plane plane = new Plane();
            Task(plane);
            plane.CarryPassange();
            Console.WriteLine("---------------------------");

            Bird bird = new Bird();
            Task2(bird);
            bird.LayEggs();
            bird.Eat();
            Console.WriteLine("---------------------------");

            Superman superman = new Superman();
            Task3(superman);
            superman.Eat();
            Console.WriteLine("---------------------------");

        }

        public static void Task(IFlyable plane)
        {
            plane.TakeOff();
            plane.Fly();
            plane.Land();
        }
        public static void Task2(IFlyable bird)
        {
            bird.TakeOff();
            bird.Fly();
            bird.Land();
        }
        public static void Task3(Superman superman)
        {
            superman.TakeOff();
            superman.Fly();
            superman.Land();
        }
    }
}
