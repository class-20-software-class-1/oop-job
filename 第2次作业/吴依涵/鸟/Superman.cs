﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Superman:Animal
    {
        public void Eat()
        {
            Console.WriteLine("我是超人，我会吃。");
        }

        public void TakeOff()
        {
            Console.WriteLine("我是超人，我在起飞中。");
        }

        public void Fly()
        {
            Console.WriteLine("我是超人，我现在在天上飞。");
        }
        public void Land()
        {
            Console.WriteLine("我是超人，我会在陆地跑。");
        }
    }
}
