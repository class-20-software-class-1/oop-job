﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Dog : Animal,ISwim
    {
        public Dog(string animalName) : base(animalName)
        {

        }

        public void Swim()
        {
            Console.WriteLine("{0}在游泳", this.AnimalName);
        }
    }
}
