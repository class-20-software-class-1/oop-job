﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Computer
    {
        private string computerName;

        public string ComputerName 
        {
            get { return this.computerName; }
            set { this.computerName = value; }
        }

        public Computer(string name)
        {
            this.computerName = name;
        }

        public void Start()
        {
            Console.WriteLine("{0}正在开机",this.computerName);
        }

        //public void Read()
        //{
        //    Console.WriteLine("{0}正在读取数据",this.computerName);
        //}

        //public void Write()
        //{
        //    Console.WriteLine("{0}正在写入数据",this.computerName);
        //}

        public void End()
        {
            Console.WriteLine("{0}正在关机",this.computerName);
        }
    }
}
