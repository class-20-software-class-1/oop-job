﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class MobileStorage
    {
        private string diskName;

        private string capacity;//容量

        public string DiskName 
        {
            get { return this.diskName; }
            set { this.diskName = value; } 
        }

        public string Capacity
        {
            get { return this.capacity; }
            set { this.capacity = value; }
        }

        public MobileStorage(string DiskName,string Capacity)
        {
            this.diskName = DiskName;
            this.capacity = Capacity;
        }


    }
}
