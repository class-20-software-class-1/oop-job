﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            Computer c = new Computer("华为");

            c.Start();

            UDisk usb = new UDisk("U盘","10G");

            usb.Write();
            usb.Read();

            Console.WriteLine();

            MoveDisk disk = new MoveDisk("移动硬盘","500G");

            disk.Write();
            disk.Read();

            Console.WriteLine();

            c.End();
        }
    }
}
