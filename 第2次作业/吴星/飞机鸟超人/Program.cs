﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        //飞机小鸟超人
        static void Main(string[] args)
        {
            Bird bird = new Bird("老鹰");

            bird.Eat();
            bird.LayEggs();
            bird.TakeOff();
            bird.Fly();
            bird.Land();

            Superman superman = new Superman("克拉克·肯特");

            superman.Eat();
            superman.TakeOff();
            superman.Fly();
            superman.Land();

            Plane plane = new Plane("方荣星一号");

            plane.TakeOff();
            plane.Fly();
            plane.Land();
        }
    }
}
