﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Car
    {
        private string brand;

        public string Brand { get => brand; set => brand = value; }

        public Car()
        { 
        }

        public Car(string brand)
        {
            this.brand = brand;
        }

        public void Run()
        {
            Console.WriteLine("一辆{0}正在跑");
        }
    }
}
