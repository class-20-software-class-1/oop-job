﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            BmCar bmCar = new BmCar("宝马m3");
            bmCar.Run();

            BatCar batCar = new BatCar("蝙蝠战车");
            batCar.Fly();

        }
    }
}
