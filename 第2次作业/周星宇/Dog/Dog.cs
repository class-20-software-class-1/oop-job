﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Dog: Animal
    {
        public Dog()
        {
        }

        public Dog(string species) : base(species)
        {
        }
        public void swim()
        {
            Console.WriteLine("一只{0}正在游泳");
        }
    }
}
