﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Monkey: Animal
    {
        public Monkey()
        {
        }

        public Monkey(string species) : base(species) 
        {
        }
        public void climb_trees() 
        {
            Console.WriteLine("一只{0}正在爬树");
        }
    }
}
