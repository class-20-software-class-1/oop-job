﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Cat car = new Cat("小猫");
            car.eat();

            Dog dog = new Dog("伊奇");
            dog.eat();
            dog.swim();

            Duck duck = new Duck("谈老鸭");
            duck.eat();
            duck.swim();

            Monkey monkey = new Monkey("金丝猴");
            monkey.eat();
            monkey.climb_trees();
        }
    }
}
