﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class IFly
    {
        private string fly;

        public string Fly { get => fly; set => fly = value; }

        public IFly() 
        {
        }
        public IFly(string fly) 
        {
            this.fly = fly;
        }
        public void ifly() 
        {
            Console.WriteLine("{0}正在飞"); 
        }
    }
}
