﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            SuperMan superMan = new SuperMan("克拉克肯特");
            superMan.ifly();
            superMan.Write();

            Bird bird = new Bird("鸟人");
            bird.ifly();

            Plane plane = new Plane("歼20");
            plane.ifly();

        }
    }
}
