﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class SuperMan : IFly
    {
        public SuperMan(string fly): base(fly)
        {
        }
        public void Write() 
        {
            Console.WriteLine("{0}正在写字", this.Fly);
        }
    }
}
