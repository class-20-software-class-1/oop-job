﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Animal
    {
        private string name;

        public string Name 
        {
            get { return this.name;  }
            set { this.name = value; }
        }

        public virtual void Eat() 
        {
        }
        public Animal(string name) 
        {
            this.name = name;
        }
    }
}
