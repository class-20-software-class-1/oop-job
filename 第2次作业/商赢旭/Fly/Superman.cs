﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class Superman:Animal,IFlyable
    {
        public Superman(string name):base(name) { }
        public void Fly() 
        {
            Console.WriteLine("俺{0}能飞",this.Name);
        }
        public void Takeoff() 
        {
            Console.WriteLine("俺{0}能起飞", this.Name);
        }
        public void Land() 
        {
            Console.WriteLine("俺{0}着陆", this.Name);
        }
        public override void Eat()
        {
            Console.WriteLine("俺{0}能吃", this.Name);
        }
    }
}
