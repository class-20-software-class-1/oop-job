﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Demo1
{
    class Car
    {
        private string brack;

        public string Brack
        {
            get { return this.brack; }
            set { this.brack = value; }
        }
        public Car() { }
        public Car(string brack)
        {
            this.brack = brack;
        }

        public void Run()
        {
            Console.WriteLine("一辆{0}正在行驶中...",this.brack);
        }
    }
}
