﻿using System;

namespace Demo1
{
    class Program
    {
        static void Main(string[] args)
        {
            BJCar car1 = new BJCar("宝骏730");
            car1.Run();

            Console.WriteLine();

            BJCar car2 = new BJCar("宝骏510");
            car2.Run();

            Console.WriteLine();

            BianfuCar car3 = new BianfuCar("蝙蝠战车");
            car3.Run();
            car3.Fly();

            Console.WriteLine();

            ChaoR r1 = new ChaoR("开心超人");
            r1.Eat();
            r1.Fly();
        }
    }
}
