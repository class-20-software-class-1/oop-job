﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Demo1
{
    class Proson
    {
        private string name;

        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }
        public Proson(string name)
        {
            this.name = name;
        }
        public void Eat()
        {
            Console.WriteLine("{0}正在吃东西呢！",this.name);
        }
    }
}
