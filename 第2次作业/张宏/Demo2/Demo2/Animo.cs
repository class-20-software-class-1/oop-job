﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Demo2
{
    class Animo
    {
        // 2、猫、狗、鸭、猴，（吃、游泳、爬树）
        private string dw;

        public string Dw
        {
            get { return this.dw; }
            set { this.dw = value; }
        }

        public Animo() { }
        public Animo(string dw)
        {
            this.dw = dw;
        }
        public void Eat()
        {
            Console.WriteLine("一只{0}正在吃东西",this.dw);
        }

    }
}
