﻿using System;

namespace Demo2
{
    class Program
    {
        static void Main(string[] args)
        {
            // 2、猫、狗、鸭、猴，（吃、游泳、爬树） 
            Cat an1 = new Cat("猫");
            an1.Eat();

            Console.WriteLine();

            Duk an2 = new Duk("鸭子");
            an2.Eat();
            an2.Swing();

            Console.WriteLine();

            Dog an3 = new Dog("狗");
            an3.Eat();

            Console.WriteLine();

            Menkey an4 = new Menkey("猴子");
            an4.Eat();
            an4.Ps();
        }
    }
}
