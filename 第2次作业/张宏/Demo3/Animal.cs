﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Demo3
{
    class Animal
    {
        private string shui;

        public string Shui
        {
            get { return this.shui; }
            set { this.shui = value; }
        }
        public Animal()
        {

        }
        public Animal(string shui)
        {
            this.shui = shui;
        }
        public void Eat()
        {
            Console.WriteLine("{0}正在吃粑粑",this.shui);
        }
    }
}
