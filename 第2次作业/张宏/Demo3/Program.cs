﻿using System;

namespace Demo3
{
    class Program
    {
        static void Main(string[] args)
        {
            Plane f1 = new Plane("飞机");
            f1.Fly();

            Console.WriteLine();

            Chaoren f2 = new Chaoren("超人");
            f2.Fly();
            f2.Eat();

            Console.WriteLine();

            Bird f3 = new Bird("鸟");
            f3.Eat();
            f3.Fly();

            Console.WriteLine();
            

        }
    }
}
