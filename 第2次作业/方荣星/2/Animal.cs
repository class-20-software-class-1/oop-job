﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Animal
    {
        private string name;

        public string Name { get => name; set => name = value; }

        public Animal(string name)
        {
            Name = name;
        }
      
    }
}
