﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Dog:Animal,IEat
    {
        public Dog(string name) : base(name)
        {
            Console.WriteLine("我的名字是{0}",this.Name);
        }

        public void Eat()
        {
            Console.WriteLine("我能吃");
        }
    }
}
