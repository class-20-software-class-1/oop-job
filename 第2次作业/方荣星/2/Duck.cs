﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Duck : Animal, ISwin
    {
        public Duck(string name) : base(name)
        {
            Console.WriteLine("我的名字是{0}", this.Name);
        }
        public void Swin()
        {
            throw new NotImplementedException();
        }
    }
}
