﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Aircraft : Vehicle, Ifly
    {
        public Aircraft(string name) : base(name) { }
        public void fly()
        {
            Console.WriteLine("我飞了");
        }
    }
}
