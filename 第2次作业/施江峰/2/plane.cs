﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class plane: father,Fly
    {
        public plane(string name)
        {
            this.name = name;
        }

        public void flly()
        {
            Console.WriteLine("我是{0}我会飞", this.name);
        }

        public override void tell()
        {
            Console.WriteLine("一个{0}", this.name);
        }
    }
}
