﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Monkey monkey = new Monkey("猴子");
            //monkey.Eat();
            //monkey.ClimbTree();
            Eat(monkey);
            Climb(monkey);


            Console.WriteLine("---------------------");

            Duck duck = new Duck("巨力鸭");
            //duck.Eat();
            //duck.Swim();
            Eat(duck);
            Swim(duck);

            Console.WriteLine("---------------------");

            Cat cat = new Cat("加菲猫");
            //cat.Eat();
            //cat.ClimbTree();
            Eat(cat);
            Climb(cat);

            Console.WriteLine("---------------------");

            Dog dog = new Dog("哮天犬");
            //dog.Eat();
            //dog.Swim();
            Eat(dog);
            Swim(dog);
        }
        public static void Climb(IClimbTree climbTree ) 
        {
            climbTree.ClimbTree();
        }

        public static void Swim(ISwim swim ) 
        {
            swim.Swim();
        }

        public static void Eat(Animal animal) 
        {
            animal.Eat();
        }
    }
}
