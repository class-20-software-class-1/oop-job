﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp3
{
    class Computer
    {
        private string name;

        private IUSB usb1;
        private IUSB usb2;

        public string Name 
        {
            get { return this.name; }
            set { this.name = value; }
        }
        //public IUSB Usb1
        //{
        //    get { return this.usb1; }
        //    set { this.usb1 = value; }
        //}

        //public IUSB Usb2
        //{
        //    get { return this.usb2; }
        //    set { this.usb2 = value; }
        //}

        internal IUSB Usb1 { get => usb1; set => usb1 = value; }
        internal IUSB Usb2 { get => usb2; set => usb2 = value; }

        public Computer(string name) 
        {
            this.name = name;
        }
        public void Star() 
        {
            Console.WriteLine("{0}正在开机",name);
        }
        public void Writedate() 
        {
            
            this.usb1.Write();
            this.usb2.Write();
        }

        public void Readdate() 
        {
            this.usb1.Read();
            this.usb2.Read();
        }

        public void End() 
        {
            Console.WriteLine("{0}正在关机",name); 
        }
    }
}
