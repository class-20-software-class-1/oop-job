﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp3
{
    class MoveDisk:Diskfather,IUSB
    {

        public MoveDisk(string name):base(name) { }
        public  void Read()
        {
            Console.WriteLine("我是移动硬盘{0}，我在读取数据"+this.Name);
        }

        public  void Write()
        {
            Console.WriteLine("我是移动硬盘{0}，我在写入数据" + this.Name);
        }

        
    }
}
