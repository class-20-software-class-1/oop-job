﻿using System;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            Computer computer = new Computer("漏电的电脑");
            computer.Star();
            computer.End();

            Console.WriteLine("--------------------------------------------");
            Udisk u1 = new Udisk("一点也装不下了的1024T");
            Udisk u2 = new Udisk("我是真的真的一点也装不下的1G");
            computer.Usb1 = u1;
            computer.Usb2 = u2;
            computer.Writedate();



        }
    }
}
