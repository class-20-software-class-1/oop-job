﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp3
{
    class Udisk : Diskfather, IUSB
    {

        public Udisk(string name):base(name) 
        {
            this.Name = name;
        }
       
        public void Read()
        {
            Console.WriteLine("我是U盘{0}，我在努力读取数据",this.Name);
        }

        public void Write()
        {
            Console.WriteLine("我是U盘{0}，我在努力写入数据",this.Name);
        }
    }
}
