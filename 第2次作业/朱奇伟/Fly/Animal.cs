﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class Animal
    {
        private string name;

        public string Name 
        {
            get { return this.name;  }
            set { this.name = value; }
        }

        public Animal(string name ) 
        {
            this.name = name;
        }
        public virtual void Eat() { }
    }
}
