﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class Plane:Vehicle,IFlyable
    {
        public Plane(string name):base(name) { }
        public void Takeoff()
        {
            Console.WriteLine("俺{0}能起飞",this.Name);
        }

        public void Fly()
        {
            Console.WriteLine("俺{0}能飞", this.Name);
        }

        public void Land()
        {
            Console.WriteLine("俺{0}能着陆", this.Name);
        }
        public override void CarryPassange()
        {
            Console.WriteLine("俺{0}跟他们不同,我能载人",this.Name);
        }

    }
}
