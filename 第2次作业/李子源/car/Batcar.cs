﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Batcar:Car,IFly
    {
        public Batcar(string name) : base(name)
        {
            this.Name = name;
        }
        public void Fly()
        {
            Console.WriteLine("我是{0}，我能上天",this.Name );
        }
        public void Write()
        {
            Console.WriteLine("我是{0}，我开始飙车了",this.Name );
        }
    }
}
