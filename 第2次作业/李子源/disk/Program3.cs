﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            Comparer computer = new Computer("死机的电脑")；
            computer.Star();
            computer.End();

            Console.WriteLine("----------------------------");
            Udisk a = new Udisk("啥也不是的1024T");
            Udisk b = new Udisk("我真的啥也不是");
            computer.Usb1 = a;
            computer.Usb2 = b;
            Console.WriteLine();
        }
    }
}
