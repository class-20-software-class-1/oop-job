﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp4
{
    class Plane : Vehicle, IFlyable
    {
        public Plane(string name) : base(name) { }
        public void Takeoff()
        {
            Console.WriteLine("我是{0}，我能起飞", this.Name);
        }
        public void Fly()
        {
            Console.WriteLine("我是{0}，我能飞", this.Name);
        }
        public void Land()
        {
            Console.WriteLine("我是{0}，我能着陆", this.Name);
        }
        public override void CarryPassange()
        {
            Console.WriteLine("我{0}跟他们不同,我能载人", this.Name);
        }
    }
}
