﻿using System;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            Superman superman = new Superman("不穿内裤的超人");
            Eat(superman);
            Fly(superman);

            Console.WriteLine("-------------分割线----------------");

            Bird bird = new Bird("抓老鹰的小鸡");
            Eat(bird);
            Fly(bird);

            Console.WriteLine("-------------分割线----------------");

            Plane plane = new Plane("不是飞鸡的飞鸭");
            Fly(plane);
            Tool(plane);
        }
        static void Tool(Vehicle vehicle)
        {
            vehicle.CarryPassange();
        }
        static void Eat(Animal animal)
        {
            animal.Eat();
        }
        static void Fly(IFlyable flyable)
        {
            flyable.Fly();
            flyable.Takeoff();
            flyable.Land();
        }
    }
}
