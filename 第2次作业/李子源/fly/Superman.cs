﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp4
{
    class Superman : Animal, IFlyable
    {
        public Superman(string name) : base(name) { }
        public void Fly()
        {
            Console.WriteLine("我是{0}，我能飞", this.Name);
        }
        public void Takeoff()
        {
            Console.WriteLine("我是{0}，我能起飞", this.Name);
        }
        public void Land()
        {
            Console.WriteLine("我是{0}，我能着陆", this.Name);
        }
        public override void Eat()
        {
            Console.WriteLine("我是{0}，我能吃", this.Name);
        }
    }
}
