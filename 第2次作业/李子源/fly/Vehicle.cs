﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp4
{
    class Vehicle
    {
        private string name;
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }
        public Vehicle(string name)
        {
            this.name = name;
        }
        public virtual void CarryPassange() { }
    }
}
