﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Disk:Father,IOperation
    {
        public Disk(string name, string size) : base(name, size)
        {
        }
        public override void operation()
        {
            Console.WriteLine("正在读取{0}中...", name);
            Console.WriteLine("剩余空间{0}，请操作：", size);
        }

        public void store()
        {
            
        }

        public void take()
        {
            
        }
    }
}
