﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    abstract class Father
    {
        protected string name;
        protected string size;

        public  string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }

        public  string Size
        {
            get
            {
                return size;
            }

            set
            {
                size = value;
            }
        }

        public Father(string name , string size)
        {
            this.name = name;
            this.size = size;
        }
        public abstract void operation();
    }
}
