﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Eat
    {
        protected string name;
        protected string food;

        protected string Food
        {
            get { return this.food; }
            set { this.food = value; }
        }
        protected string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        protected Eat(string food,string name)
        {
            this.food = food;
            this.name = name;
        }

        public void eatFood()
        {
            Console.WriteLine("{0}正在吃{1}",name,food);
        }
    }
}
