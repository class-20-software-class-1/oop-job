﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Animal
    {
        protected string eat;
        protected string name;

        public Animal(string eat, string name)
        {
            this.eat = eat;
            this.name = name;
        }

        public string Eat
        {
            get { return this.eat; }
            set { this.eat = value; }
        }

        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }
    }
}
