﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Computer
    {
        private string cName;
        private string usb1;
        private string usb2;

        public string CName { get => cName; set => cName = value; }
        public string Usb1 { get => usb1; set => usb1 = value; }
        public string Usb2 { get => usb2; set => usb2 = value; }

        public Computer(string cName, string usb1, string usb2)
        {
            this.CName = cName;
            this.Usb1 = usb1;
            this.Usb2 = usb2;
        }

        public void star()
        {
            Console.WriteLine("{0}电脑启动中...",this.CName);
            Console.WriteLine("启动完毕，读取{0}{1}中...",this.Usb1,this.Usb2);
        }
        public void end()
        {
            Console.WriteLine("电脑关机中...",this.cName);
        }
    }
}
