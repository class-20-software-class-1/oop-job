﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Disk : Computer, IOperation
    {
        public Disk(string usb1, string usb2, string cName) : base(usb1, usb2, cName)
        {

        }
        public void duqu()
        {
            Console.WriteLine("{0}正在读取中", this.Usb2);
        }

        public void xieru()
        {
            Console.WriteLine("{0}正在写入中", this.Usb2);
        }
    }
}
