﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class animal
    {
        private string name;

        public animal(string name)
        {
            this.Name = name;
        }

        public string Name { get => name; set => name = value; }

        public void Eat()
        {
            Console.WriteLine("{0}可以吃饭",this.name);
        }
    }
}
