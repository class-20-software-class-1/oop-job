﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            string str = "与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。";
            char[] arr = str.ToArray();
            int sum1 = 0;
            int sum2 = 0;

            for (int i = 0; i < arr.Length; i++)
            {
                if (str[i].Equals('码') == true)
                {
                    sum1++;
                }
                if (str[i].Equals('类') == true)
                {
                    sum2++;
                }
            }
            Console.WriteLine("码字的个数:" + sum1);
            Console.WriteLine("类字的个数:" + sum2);

            Console.WriteLine("-----------------------------------");

            int sum3 = 0;
            int sum4 = 0;
            string b = str.Replace("码", "1");
            char[] arr1 = b.ToArray();
            for (int i = 0; i < arr1.Length; i++)
            {
                if (b[i].Equals('1') == true)
                {
                    sum3++;
                }
            }
            Console.WriteLine("码字的个数:" + sum3);

            string c = str.Replace("类", "2");
            char[] arr2 = c.ToArray();
            for (int i = 0; i < arr2.Length; i++)
            {
                if (c[i].Equals('2') == true)
                {
                    sum4++;
                }
            }
            Console.WriteLine("类字的个数:" + sum4);
            Console.WriteLine("-----------------------------------");
            string[] a = { "码" };
            string[] A = str.Split(a, StringSplitOptions.RemoveEmptyEntries);
            Console.WriteLine("码字的个数:" + (A.Length - 1));
            string[] jkl = { "类" };
            string[] JKL = str.Split(jkl, StringSplitOptions.RemoveEmptyEntries);
            Console.WriteLine("类字的个数:" + (JKL.Length - 1));
            Console.WriteLine("------------------------------------");

            string str1 = "C# (英 文名为 CSharp) 是 微  软开发的一种 面向对 象的 编程 语言。C# 语言具备了面向对象 语言 的特 征，  即封装、继承、多态，并且添加了  事件和委托，增强了 编程的灵 活性。C# 语 言是  一种安全的、稳定的、简 单 的、面向对象的编程 语言 ，其语 法与 C++ 类似，但在编程过  程中要比 C++ 简单；它不仅去掉了 C++ 和 Java 语  言中的一些复杂特性，还提  供了可视化 工具，能够高效地 编写程序。C# 是运行  在.NE  T平台之上的  编程 语言。";
            string[] asd = { " " };

            string[] ASD = str1.Split(asd, StringSplitOptions.None);

            string p = str1.Replace(" ", "");
            Console.WriteLine("去空格前：");
            Console.WriteLine(str1);

            Console.WriteLine("去空格后：");
            Console.WriteLine(p);

            Console.WriteLine("文字中空格数的个数:" + (ASD.Length));

            Console.WriteLine();
        }
    }
        
    }
