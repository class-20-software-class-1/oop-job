﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Car
    {
        private string brand;

        public string Brand
        {
            get { return this.brand; }
            set { this.brand = value; }
        }

        public Car(string brand)
        {
            this.Brand = brand;
        }

        public Car() { }

        public void Run()
        {
            Console.WriteLine("一辆{0}在高速公路上飞驰",this.Brand);
        }
    }
}
