﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Anmal
    {
        private string name;

        public Anmal(string name)
        {
            this.Name = name;
        }

        public string Name { get => name; set => name = value; }

        public void Eat()
        {
            Console.WriteLine("{0}可以吃东西",this.name);
        }
    }
}
