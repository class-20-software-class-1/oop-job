﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Plane:Vehicle,Fly,TakeOff,Land
    {
        public Plane(string name) : base(name)
        {

        }
        public void fly()
        {
            Console.WriteLine("{0}可以飞行", this.Name);
        }

        public void land()
        {
            Console.WriteLine("{0}可以降落", this.Name);
        }

        public void takeOff()
        {
            Console.WriteLine("{0}可以起飞", this.Name);
        }
    }
}
