﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            Bird bird = new Bird("小鸟");
            bird.fly();
            bird.Eat();
            bird.land();
            bird.takeOff();
            Console.WriteLine("------------------------");
            Plane plane = new Plane("战斗机");
            plane.vehicle();
            plane.fly();
            plane.land();
            plane.takeOff();
            Console.WriteLine("-------------------------");
            Superman superman = new Superman("超人");
            superman.Eat();
            superman.fly();
            superman.land();
            superman.takeOff();
        }
    }
}
