﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Vehicle
    {
        private string name;

        public Vehicle(string name)
        {
            this.Name = name;
        }

        public string Name { get => name; set => name = value; }

        public void vehicle()
        {
            Console.WriteLine("{0}是交通工具",this.name);
        }
    }
}
