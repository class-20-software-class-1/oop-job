﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Cat : Eat, Swim, Tree
    {
        private string name;
        public string Name { get => name; set => name = value; }

        public Cat(string name) 
        {
            Eat(name);
            Swim(name);
            Tree(name);
        }
        public void Swim(string name)
        {
            Console.WriteLine("{0}会游泳", name);
        }

        public void Tree(string name)
        {
            Console.WriteLine("{0}会爬树", name);
        }

        public void Eat(string name)
        {
            Console.WriteLine("{0}吃饭", name);
        }
    }
}
