﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class USB : Mobile, USBClass
    {
        public USB(string name) 
        {
            Start(name);
            End(name);
        }
        public void Start(string name)
        {
            Console.WriteLine("{0}正在读取数据。", name);
        }

        public void End(string Name)
        {
            Console.WriteLine("{0}正在写入数据。", Name);
        }

        public void Usb()
        {
            Console.WriteLine();
        }
    }
}
