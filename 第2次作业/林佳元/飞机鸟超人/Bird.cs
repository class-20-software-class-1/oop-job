﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Bird : Animal, IFlyable
    {
        public Bird(string name) : base(name)
        {

        }

        public void Fly()
        {
            Console.WriteLine("{0}在飞", this.Name);
        }

        public void Land()
        {
            Console.WriteLine("{0}在降落", this.Name);
        }


        public void TakeOff()
        {
            Console.WriteLine("{0}在起飞", this.Name);
        }

        public void LayEggs()
        {
            Console.WriteLine("{0}在下蛋", this.Name);
        }
    }
}
