﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            BatCar batCar = new BatCar("蝙蝠车");
            batCar.Run();
            batCar.Fly();
            PCar pCar = new PCar("什么车");
            pCar.Run();
        }
    }
}
