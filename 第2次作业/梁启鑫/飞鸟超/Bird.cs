﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Bird : Animal, IFlyable
    {
        public Bird(string eat, string name) : base(eat, name)
        {
        }
        public void Fly()
        {
            Console.WriteLine("捉虫子去了");
        }

        public void Land()
        {
            Console.WriteLine("被吃的--竟然是我！！（{0}）",eat);
           
        }

        public void TakeOff()
        {
            Console.WriteLine("{0}:早起的鸟儿哟~~~~",name);
        }
    }
}
