﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class car
    {
        private string name;
        private string brak;

        public car(string name, string brak)
        {
            this.name = name;
            this.brak = brak;
        }
        public car()
        {
            
        }

        public void run() {

            Console.WriteLine("我叫{0},我{1}正在奔跑中...", name, brak);
        }

        public string Name { get => name; set => name = value; }
        public string Brak { get => brak; set => brak = value; }

    }
}
