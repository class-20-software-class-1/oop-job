﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Animal
    {
        private string brak;

        public Animal(string brak)
        {
            this.brak = brak;
        }

        public string Brak { get => brak; set => brak = value; }

        public void eat() {
            Console.WriteLine("我在吃饭");
        }
    }
}
