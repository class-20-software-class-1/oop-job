﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            //4、超人、小鸟、飞机
            Superman S = new Superman("一拳超人");
            S.land();
            S.takeoff();
            S.fly();
            Console.WriteLine("--------");
            Bird B = new Bird("8");
            B.fly();
            B.eat();
            B.land();
            B.layEggs();
            B.takeoff();
            Console.WriteLine("--------");
            Plane P = new Plane();
            P.ees();
            P.land();
            P.takeoff();
            P.fly();
            P.CarryPassange();
        }
    }
}
