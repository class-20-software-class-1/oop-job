﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Superman:iflyable
    {
        private string name;

        public Superman(string name)
        {
            this.name = name;
        }

        public string Name { get => name; set => name = value; }

        public void fly()
        {
            Console.WriteLine("{0}正在飞",name);
        }

        public void land()
        {
            Console.WriteLine("{0}正在奔跑",name);
        }

        public void takeoff()
        {
            Console.WriteLine("{0}正在准备起飞",name);
        }
    }
}
