﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            //            一、统计下面一段文字中“类”字和“码”的个数。

            //与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。

            //1、使用循环遍历的方法来实现。
            //2、使用Replace方法来实现。
            //3、使用Split()方法来实现。
            text1();
            text2();
        }
        public static void text1() {
            string a = "与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。";
            char  [] arr = a.ToArray();
            int sum1 = 0;
            int sum2 = 0;
            for (int i = 0; i < arr.Length; i++)
            {
               
                if (a[i].Equals('类')==true)
                {
                    sum1++;

                }
                if (a[i].Equals('码') == true)
                {
                    sum2++;
                }
            }
            Console.WriteLine("结果是：类有{0}个，码有{1}个",sum1,sum2);


            Console.WriteLine("----------------");
            string[] uio = { "码" };
            string[] UIO = a.Split(uio, StringSplitOptions.RemoveEmptyEntries);
            Console.WriteLine("文字中“码”字的个数:" + (UIO.Length - 1));
            string[] jkl = { "类" };
            string[] JKL = a.Split(jkl, StringSplitOptions.RemoveEmptyEntries);
            Console.WriteLine("文字中“类”字的个数:" + (JKL.Length - 1));
            Console.WriteLine();


            Console.WriteLine("----------------");
            int sum3 = 0;
            int sum4 = 0;
            string b = a.Replace("码","1");
            char []arr1 = b.ToArray();
            for (int i = 0; i < arr1.Length; i++)
            {
                if (b[i].Equals('1') == true)
                {
                    sum3++;
                }
            }
            Console.WriteLine("“码”字的个数:{0}",sum3);
            string d = a.Replace("类", "2");
            char[] arr2 = b.ToArray();
            for (int i = 0; i < arr1.Length; i++)
            {
                if (d[i].Equals('2') == true)
                {
                    sum4++;
                }
            }
            Console.WriteLine("“类”字的个数:{0}", sum4);
        }
        //二、
        //        C# (英 文名为 CSharp) 是 微  软开发的一种 面向对 象的 编程 语言。C# 语言具备了面向对象 语言 的特 征，  即封装、继承、多态，并且添加了  事件和委托，增强了 编程的灵 活性。C# 语 言是  一种安全的、稳定的、简 单 的、面向对象的编程 语言 ，其语 法与 C++ 类似，但在编程过  程中要比 C++ 简单；它不仅去掉了 C++ 和 Java 语  言中的一些复杂特性，还提  供了可视化 工具，能够高效地 编写程序。C# 是运行  在.NE  T平台之上的  编程 语言。

        //去掉上面一段文字的所有空格，并统计空格数。
        public static void text2() {
            Console.WriteLine("------------------");
            string a = "C# (英 文名为 CSharp) 是 微  软开发的一种 面向对 象的 编程 语言。C# 语言具备了面向对象 语言 的特 征，  即封装、继承、多态，并且添加了  事件和委托，增强了 编程的灵 活性。C# 语 言是  一种安全的、稳定的、简 单 的、面向对象的编程 语言 ，其语 法与 C++ 类似，但在编程过  程中要比 C++ 简单；它不仅去掉了 C++ 和 Java 语  言中的一些复杂特性，还提  供了可视化 工具，能够高效地 编写程序。C# 是运行  在.NE  T平台之上的  编程 语言。";
            char[] arr = a.ToArray();
            string [] add = { " " };
            string[] bdd = a.Split(add,StringSplitOptions.None);
            Console.WriteLine("文字中“ ”字的个数:",(bdd.Length));
            string p = a.Replace(" ","");
            Console.WriteLine("清楚空格前::"+a);
            Console.WriteLine();
            Console.WriteLine("清楚空格前::"+p);
            
        }
    }
}
