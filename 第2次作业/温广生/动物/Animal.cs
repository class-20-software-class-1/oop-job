﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Animal
    {

        //2、猫、狗、鸭、猴，（吃、游泳、爬树）
        private string name;
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        public Animal(string name)
        {
            this.name = name;
        }

        public Animal() { }

        public void Eat()
        {
            Console.WriteLine("我是一只{0},我会吃", this.Name);
        }
        
    }
}
