﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Gou:Animal,IYOUYONG
    {
        public Gou(string name) : base(name)
        {
            this.Name = name;
        }
        public void Eat()
        {
            Console.WriteLine("我是一只{0},我会吃", this.Name);
        }
        public void YouYong()
        {
            Console.WriteLine("我是一只{0},我会游泳", this.Name);
        }
    }
}
