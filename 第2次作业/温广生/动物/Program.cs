﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Cat cat = new Cat("猫");
            cat.Eat();
            Test4(cat);
            Console.WriteLine("---------------------------");

            Hou hou = new Hou("猴");
            hou.Eat();
            Test3(hou);
            Console.WriteLine("---------------------------");

            Gou gou = new Gou("狗");
            gou.Eat();
            Test2(gou);
            Console.WriteLine("---------------------------");

            Ya ya = new Ya("鸭");
            ya.Eat();           
            Test(ya);
            Console.WriteLine("---------------------------");
        }

       public static void Test(IYOUYONG ya)
        {
            ya.YouYong();
        }

        public static void Test2(IYOUYONG gou)
        {
            gou.YouYong();
        }

        public static void Test3(IPASHU hou)
        {
            hou.PaShu();
        }

        public static void Test4(IPASHU cat)
        {
            cat.PaShu();
        }
    }
}
