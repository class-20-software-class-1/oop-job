﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Plane : IFly
    {
        public Plane(string name)
        {
             Fly(name);
        }
        public void Fly(string name)
        {
            Console.WriteLine("{0}会飞",name);
        }
    }
}
