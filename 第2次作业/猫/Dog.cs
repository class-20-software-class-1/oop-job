﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Dog:Animal,ISwim
    {
        public Dog(string kind) : base(kind) { }

        public void Swim()
        {
            Console.WriteLine("{0}会游",this.Kind);
        }
    }
}
