﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Monkey:Animal,IClimb,ISwim
    {
        public Monkey(string kind) : base(kind) { }

        public void Climb()
        {
            Console.WriteLine("{0}会爬树", this.Kind);
        }
        public void Swim()
        {
            Console.WriteLine("{0}会游泳", this.Kind);
        }
    }
}
