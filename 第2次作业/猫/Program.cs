﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Cat c = new Cat("猫");
            c.Eat();
            c.Swim();
            c.Climb();
            Dog d = new Dog("狗");
            d.Eat();
            d.Swim();
            Duck ck = new Duck("鸭子");
            ck.Eat();
            ck.Swim();
            Monkey m = new Monkey("猴子");
            m.Eat();
            m.Swim();
            m.Climb();
        }
    }
}
