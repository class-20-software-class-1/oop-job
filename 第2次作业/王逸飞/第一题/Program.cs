﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5._27
{
    class Program
    {
        static void Main(string[] args)
        {
            string num = "与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。";
            string[] var = new string[num.Length];
            int a = 0;
            int b = 0;
            for (int i = 0; i < var.Length; i++)
            {
                var[i] = num.Substring(i, 1);


                if (var[i] == "类")
                {
                    a++;
                }
                else if (var[i] == "码")
                {
                    b++;
                }
            }
            Console.WriteLine("类有" + a + "个");
            Console.WriteLine("类有" + b + "个");
            Console.WriteLine("-----------------------------------------------");
            //----------------------------------------------------------------------------------------------------


            string[] arr = {"类" };
            string[] ASD = num.Split(arr,StringSplitOptions.RemoveEmptyEntries);
            Console.WriteLine(ASD.Length-1);



        }
    }
}
