﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Car : Trumpet,Drive
    {
        private string name;

        public string Name { get => name; set => name = value; }

        public Car (string name)
        {
            Drive(name);
            Trumpet(name);
        }
        public void Drive(string name)
        {
            Console.WriteLine("{0}有轮子",name);
        }

        public void Trumpet(string name)
        {
            Console.WriteLine("{0}能跑",name);
        }
    }
}
