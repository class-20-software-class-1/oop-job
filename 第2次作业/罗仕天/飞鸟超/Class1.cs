﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Class1
    {
        private IFlyable fly1;
        private IFlyable fly2;
        private IFlyable fly3;

        public IFlyable Fly1 { get => fly1; set => fly1 = value; }
        public IFlyable Fly2 { get => fly2; set => fly2 = value; }
        public IFlyable Fly3 { get => fly3; set => fly3 = value; }

        public void 芜湖()
        {
            fly1.TakeOff();
            fly2.TakeOff();
            fly3.TakeOff();
        }

        public void 奥特曼()
        {
            fly1.Fly();
            fly2.Fly();
            fly3.Fly();
        }

        public void 谢谢你()
        {
            fly1.Land();
            fly2.Land();
            fly3.Land();
        }
    }
}
