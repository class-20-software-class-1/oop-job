﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class BatCar : Drive, Trumpet
    {
        private string name;

        public string Name { get => name; set => name = value; }

        public BatCar (string name)
        {
            Drive(name);
            Trumpet(name);
        }

        public void Trumpet(string name)
        {
            Console.WriteLine("{0}能按喇叭", name);
        }

        public void Drive(string name)
        {
            Console.WriteLine("{0}能开", name);
        }
    }
}
