﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        
        static void Main(string[] args)
        {
            BatCar b = new BatCar("蝙蝠车");
            Drive drive = b;
            Trumpet trumpet = b;

            Console.WriteLine();

            Car c = new Car("车");
            Drive drive = c;
            Trumpet trumpet = c;

        }
    }
}
