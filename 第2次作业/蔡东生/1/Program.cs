﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Class2 c2 = new Class2("蝙蝠战车");
            c2.fei();
            Class3 C3 = new Class3("F4方程赛车");
            C3.kai();

            Class1 c1 = new Class1();
            Test(c1);
            Test2(c1);
            Test3(c1);
        }
        static void Test(Class2 class2)
        {
            class2.fei();
        }
        static void Test2(Class3 class3)
        {
            class3.kai();
        }
    }
}
