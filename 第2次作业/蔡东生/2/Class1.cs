﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Class1
    {
        private string name;
        private string chi;

        public string Name { get => name; set => name = value; }
        public string Chi { get => chi; set => chi = value; }

        public Class1(string name, string chi)
        {
            this.name = name;
            this.chi = chi;
        }
        public  void chihe() {
            Console.WriteLine("{0}都会吃饭{1}",this.name,this.chi);
        }

    }
}
