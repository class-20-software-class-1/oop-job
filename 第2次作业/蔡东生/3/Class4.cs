﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Class4 : Class1,Class2
    {
        public Class4(string name) : base(name)
        {
        }

        public void dongwu()
        {
            Console.WriteLine("{0}是一个碳基物体", this.Name);
        }

        public void fei()
        {
            Console.WriteLine("{0}都会飞", this.Name);
        }
    }
}
