﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Computer
    {
        private string brand;

        private IUSB usb1;
        private IUSB usb2;

        public string Brand { get => brand; set => brand = value; }
        internal IUSB Usb1 { get => usb1; set => usb1 = value; }
        internal IUSB Usb2 { get => usb2; set => usb2 = value; }

        public Computer(string brand)
        {
            this.brand = brand;
        }

        public void Start()
        {
            Console.WriteLine("{0}电脑正在开机中...",this.brand);
        }

        public void ReadData()
        {
            this.usb1.Read();
            this.usb2.Read();
        }
        public void WriteData()
        {
            this.usb1.Write();
            this.usb2.Write();
        }

        public void End()
        {
            Console.WriteLine("{0}电脑正在关机中...", this.brand);
        }
    }
}
