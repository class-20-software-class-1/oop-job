﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Disk
    {
        private string brand;
        public string Brand { get => brand; set => brand = value; }

        public Disk(string brand)
        {
            this.brand = brand;
        }

        public Disk()
        {
        }


    }
}
