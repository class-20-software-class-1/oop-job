﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            UDisk u = new UDisk("金士顿64G");
            MobileHardDisk m = new MobileHardDisk("三星500G");

            u.Write();
            u.Read();
            m.Write();
            m.Read();
            Computer computer = new Computer("华为");
            computer.Usb1 = u;
            computer.Usb2 = m;

            computer.Start();
            computer.WriteData();
            computer.ReadData();
            computer.End();
            
        }
    }
}
