﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class UDisk : Disk, IUSB
    {
        public UDisk(string brand) : base(brand)
        { 
        }

        public void Read()
        {
            Console.WriteLine("U盘{0}正在读取数据中...",this.Brand);
        }

        public void Write()
        {
            Console.WriteLine("U盘{0}正在写入数据中...", this.Brand);
        }
    }
}
