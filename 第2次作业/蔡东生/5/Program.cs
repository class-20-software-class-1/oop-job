﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {

            test4();
        }

            public static void test() {
            string a = "与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。";

            char[] asd = a.ToArray();
            char s = '类';
            char b = '码';
            int sum = 0;
            int kkk = 0;
            foreach (char item in asd)
            {
                if (item.Equals(s))
                {
                    sum += 1;
                }
                if (item.Equals(b))
                {
                    kkk += 1;
                }


            }
            Console.WriteLine(sum);
            Console.WriteLine(kkk);
        }
        public static void test1() {
            string a = "与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。";
            string[] condition = { "类"};
            string[] condition1 = {  "码" };
            string[] result = a.Split(condition, StringSplitOptions.None);
            string[] result1 = a.Split(condition1, StringSplitOptions.None);
            Console.WriteLine("字符串中含有类的个数为：" + (result.Length - 1));
            Console.WriteLine("字符串中含有码的个数为：" + (result1.Length - 1));
        }
        public static void test2() {
            string a = "与其他面向对象语言一样，C#语言也具有面向对象语言的基本特征，即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。";       
            a=a.Replace("类","无");
            a=a.Replace("码","聊");
            char[] b = a.ToArray();
            char s = '无';
            char k = '聊';
            int c = 0;
            int d = 0;
            foreach (char item in b)
            {
                if (item.Equals(s))
                {
                    c += 1;
                }
                if (item.Equals(k))
                {
                    d += 1;
                }


            }
            Console.WriteLine("类 = " +c);
            Console.WriteLine("码 = "+d );
        }
        public static void test3() {
            string a = "C# (英 文名为 CSharp) 是 微  软开发的一种 面向对 象的 编程 语言。C# 语言具备了面向对象 语言 的特 征，  即封装、继承、多态，并且添加了  事件和委托，增强了 编程的灵 活性。C# 语 言是  一种安全的、稳定的、简 单 的、面向对象的编程 语言 ，其语 法与 C++ 类似，但在编程过  程中要比 C++ 简单；它不仅去掉了 C++ 和 Java 语  言中的一些复杂特性，还提  供了可视化 工具，能够高效地 编写程序。C# 是运行  在.NE  T平台之上的  编程 语言。";
            char[] b = a.ToArray();
            int s = 0;
            foreach (char item in b)
            {
                if (item.Equals(' '))
                {
                    s+= 1;
                }
            }
            a = a.Replace(" ", "");
            Console.WriteLine(a);
            Console.WriteLine(s);
        }
        public static void test4() {
            
            StringBuilder sb = new StringBuilder();
            string a = Console.ReadLine();
            string b = Console.ReadLine();
            string c = Console.ReadLine();
            string d = Console.ReadLine();
            sb.Append(a);
            sb.Append(b);
            sb.Append(c);
            sb.Append(d);

            Console.WriteLine(sb);
        }
    }
}


