﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chp
{
    //蝙蝠战车
    class Batmobile:IRun,IFly
    {
        private string name;
        public Batmobile(string name) { this.name = name; }
        public void fly()
        {
            Console.WriteLine("{0}可以飞",this.name); 
        }

        public void run()
        {
            Console.WriteLine("{0}可以飞快地跑", this.name);
        }
    }
}
