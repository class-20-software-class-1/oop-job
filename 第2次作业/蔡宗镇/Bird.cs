﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chp
{
    class Bird : Flyable ,IEat ,ILayegg
    {  //鸟类
        public Bird(string name) : base(name){ }
        public void Eat() 
        {
            Console.WriteLine("{0}正在吃东西",Name);
        }

        public void layegg()
        {
            Console.WriteLine("{0}正在下蛋", Name);
        }
    }
}
