﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chp
{
    //普通车
    class Car : IRun
    {
        private string name;
        public Car(string name) { this.name = name; }
        public void run() 
        {
            Console.WriteLine("{0}车可以飞快的跑",this.name);
        }
    }
}
