﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chp02
{
    class Class1
    {
        private string name;
        private int age;
        private string hobby;

        public string Name { get => name; set => name = value; }
        public int Age { get => age; set => age = value; }
        public string Hobby { get => hobby; set => hobby = value; }

        public void zfc() 
        {
            string st = string.Format("我的姓名：{0},今年{1},兴趣爱好：{2}",name,Age,hobby);
            Console.WriteLine(st);
        }
    }
}
