﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chp
{
    class Computer
    {
        private IUSB usb1;
        private IUSB usb2;
        private string name;
        public string Name{get{ return this.name; } set { this.name = value; } }

        internal IUSB Usb1 { get => usb1; set => usb1 = value; }
        internal IUSB Usb2 { get => usb2; set => usb2 = value; }

        public Computer(string name)
        {
            this.name = name;
        }

        public void Read() 
        {
            this.usb1.Read();
            this.usb2.Read();
        }
        public void Write()
        {
            this.usb1.Write();
            this.usb2.Write();
        }
    }
}
