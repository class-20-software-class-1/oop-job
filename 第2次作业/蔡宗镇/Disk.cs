﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chp
{
    class Disk
    {
        //移动储存
        private string brand;

        public string Brand { get => brand; set => brand = value; }

        public Disk(string brand) { this.brand = brand; }
    }
}
