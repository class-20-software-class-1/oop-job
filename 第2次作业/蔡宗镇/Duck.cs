﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chp
{
    class Duck : Eat, ISwim
    {
        //鸭子类
        public void swim()
        {
            Console.WriteLine("{0}可以游泳",this.Name);
        }
        public Duck(string name):base(name) { }
    }
}
