﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chp
{
    class Eat
    {
        //父类 吃
        private string name;
        public string Name { get { return this.name; } }
        public Eat(string name) { this.name = name;}
        public void eat() 
        {
            Console.WriteLine("{0}可以吃东西", this.name);
        }
    }
}
