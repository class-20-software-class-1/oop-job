﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chp
{
    class Flyable
    {   //飞行类
        public string Name { get; set; }
        public Flyable(string name) { this.Name = name; }
        public void Takeoff() 
        {
            Console.WriteLine("{0}正在起飞",this.Name);
            Console.WriteLine("{0}正在飞翔", this.Name);
            Console.WriteLine("{0}正在着陆", this.Name);
        }
    }
}
