﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chp
{
    class Mobialdisk : Disk, IUSB
    {
        public Mobialdisk(string brand):base(brand) { }
        public void Read()
        {
            Console.WriteLine("移动硬盘{0}正在读取数据中............", this.Brand);
        }

        public void Write()
        {
            Console.WriteLine("移动硬盘{0}正在读取数据中............", this.Brand);
        }
    }
}
