﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chp
{
    class UDisk :Disk,IUSB
    {
        //U盘类

        public UDisk(string brand):base (brand) { }

        public void Read()
        {
            Console.WriteLine("{0}正在读取数据中............",this.Brand);
        }

        public void Write()
        {
            Console.WriteLine("{0}正在写入数据中............",this.Brand);
        }
    }
}
