﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Action
    {
        private string eat;
        private string name;
        public Action()
        {

        }

        

        public string Eat { get => eat; set => eat = value; }
        public string Name { get => name; set => name = value; }


        public Action(string name, string eat)
        {
            this.eat = eat;
            this.name = name;
        }

        public virtual void eatting(string name,string eat)
        {
            Console.WriteLine("{0}在吃{1}", this.name, this.eat);
        }
    }
}
