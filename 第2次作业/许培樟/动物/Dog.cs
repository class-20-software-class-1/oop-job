﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Dog : Action, ISwim, IClimbtree
    {
        public void ISwim()
        {
            Console.WriteLine("{0}会游泳", this.Name);
        }
       public void IClimbtree()
        {
            Console.WriteLine("{0}会爬树",this.Name);
        }
        public Dog(string name, string eat) : base(name, eat)
        {

        }
    }
}
