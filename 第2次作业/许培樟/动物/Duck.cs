﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Duck: Action, ISwim
    {
        public void ISwim()
        {
            Console.WriteLine("{0}会游泳",this.Name);
        }
        public Duck(string name, string eat) : base(name, eat)
        {

        }
    }
}
