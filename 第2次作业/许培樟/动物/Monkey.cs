﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Monkey : Action, IClimbtree
    {
        public void IClimbtree()
        {
            Console.WriteLine("{0}会爬树", this.Name);
        }
        public Monkey(string name, string eat) : base(name, eat)
        {

        }
    }
}
