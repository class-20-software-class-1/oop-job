﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            //2、猫、狗、鸭、猴，（吃、游泳、爬树）
            Cat cat1 = new Cat("猫", "鱼");
            cat1.eatting("猫", "鱼");
            cat1.IClimbtree();
            Climb(cat1);

            Console.WriteLine("------------");

            Monkey monkey= new Monkey("猴子", "香蕉");
            monkey.eatting("猴子", "香蕉");
            monkey.IClimbtree();
            Climb(monkey);

            Console.WriteLine("------------");

            Duck duck = new Duck("鸭子","饲料");
            duck.eatting("鸭子", "饲料");
            duck.ISwim();
            Swim(duck);

            Console.WriteLine("------------");

            Dog dog = new Dog("狗", "骨头");
            dog.eatting("狗", "骨头");
            dog.IClimbtree();
            dog.ISwim();
            Climb(dog);
            Swim(dog);
        }
        public static void Climb(IClimbtree climbTree)
        {
            climbTree.IClimbtree();
        }

        public static void Swim(ISwim swim)
        {
            swim.ISwim();
        }
    }
}
