﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Car
    {
        private string drive;

        public string Drive { get => drive; set => drive = value; }
        public Car()
        {

        }

        public Car(string drive)
        {
            this.drive = drive;
        }
        public void run()
        {
            Console.WriteLine("一辆{0}汽车正在行驶",this.drive);
        }
    }
}
