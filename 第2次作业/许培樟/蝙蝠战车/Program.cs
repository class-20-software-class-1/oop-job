﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            BMW mW = new BMW("宝马");
            mW.run();

            Console.WriteLine("-----------");

            BatCar bat = new BatCar("蝙蝠战车");
            bat.run();
            bat.Fly();

        }
    }
}
