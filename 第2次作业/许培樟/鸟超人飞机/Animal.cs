﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Animal
    {
        private string eat;
        private string name;

        public Animal(string name, string eat)
        {
            this.eat = eat;
            this.name = name;
        }

        public string Eat { get => eat; set => eat = value; }
        public string Name { get => name; set => name = value; }

        public virtual void eatting(string name, string eat)
        {
            Console.WriteLine("我是{0}，吃{1}",this.Name,this.Eat);
        }
    }
}
