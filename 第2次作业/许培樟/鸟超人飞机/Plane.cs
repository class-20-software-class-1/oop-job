﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Plane : Vehicle, IFlyable
    {
        //飞机
        void IFlyable.Fly()
        {
            Console.WriteLine("飞机能飞");
        }

        void IFlyable.Land()
        {
            Console.WriteLine("飞机着陆了");
        }

        void IFlyable.TakeOff()
        {
            Console.WriteLine("飞机飞起来了");
        }
    }
}
