﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            //超人、小鸟、飞机
            Superman superman = new Superman("超人", "SuperFood");
            superman.eatting("超人", "SuperFood");
            Fly(superman);

            Console.WriteLine("--------");

            Bird bird = new Bird("小鸟", "虫子");
            bird.eatting("小鸟", "虫子");
            Fly(bird);

            Console.WriteLine("-----------");

            Plane plane = new Plane();
            Fly(plane);
        }
       
       
        static void Fly(IFlyable flyable)
        {
            flyable.Fly();
            flyable.TakeOff();
            flyable.Land();
        }
    }
}
