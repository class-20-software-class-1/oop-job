﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Superman : Animal, IFlyable
    {
        //超人
        public Superman(string name, string eat) : base(name, eat)
        {

        }
        void IFlyable.Fly()
        {
            Console.WriteLine("{0}能飞", this.Name);
        }

        void IFlyable.Land()
        {
            Console.WriteLine("{0}着陆了", this.Name);
        }

        void IFlyable.TakeOff()
        {
            Console.WriteLine("{0}飞起来了", this.Name);
        }
        
    }
}
