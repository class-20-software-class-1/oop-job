using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Test1();
            Test2();
            Test3();
        }
        public static void Test1()
        {
            Console.WriteLine();
            string a = "与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。";
            char[] arr = a.ToArray();
            int sum = 0;
            int asd = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (a[i].Equals('码') == true)
                {
                    sum++;
                }
                if (a[i].Equals('类') == true)
                {
                    asd++;
                }
            }
            Console.WriteLine("文字中“码”字的个数:" + sum);
            Console.WriteLine("文字中“类”字的个数:" + asd);    

            int xzr = 0;
            int xzr1 = 0;
            string b = a.Replace("码", "1");
            char[] arr1 = b.ToArray();
            for (int i = 0; i < arr1.Length; i++)
            {
                if (b[i].Equals('1') == true)
                {
                    xzr++;
                }
            }

            Console.WriteLine("文字中“码”字的个数:" + xzr);

            string c = a.Replace("类", "2");
            char[] arr2 = c.ToArray();
            for (int i = 0; i < arr2.Length; i++)
            {
                if (c[i].Equals('2') == true)
                {
                    xzr1++;
                }
            }

            Console.WriteLine("文字中“类”字的个数:" + xzr1);

            string[] nio = { "码" };
            string[] NIO = a.Split(uio, StringSplitOptions.RemoveEmptyEntries);
            Console.WriteLine("文字中“码”字的个数:" + (NIO.Length - 1));

            string[] skl = { "类" };
            string[] SKL = a.Split(jkl, StringSplitOptions.RemoveEmptyEntries);
            Console.WriteLine("文字中“类”字的个数:" + (SKL.Length - 1));

            Console.WriteLine();
        }

        public static void Test2()
        {
            string o = "C# (英 文名为 CSharp) 是 微  软开发的一种 面向对 象的 编程 语言。C# 语言具备了面向对象 语言 的特 征，  即封装、继承、多态，并且添加了  事件和委托，增强了 编程的灵 活性。C# 语 言是  一种安全的、稳定的、简 单 的、面向对象的编程 语言 ，其语 法与 C++ 类似，但在编程过  程中要比 C++ 简单；它不仅去掉了 C++ 和 Java 语  言中的一些复杂特性，还提  供了可视化 工具，能够高效地 编写程序。C# 是运行  在.NE  T平台之上的  编程 语言。";
            string[] asd = { " " };

            string[] ASD = o.Split(asd, StringSplitOptions.None);

            string p = o.Replace(" ","");
            Console.WriteLine("去空格前：");
            Console.WriteLine(o);

            Console.WriteLine("去空格后：");
            Console.WriteLine(p);

            Console.WriteLine("文字中空格数的个数:" + (ASD.Length - 1));

            Console.WriteLine();
        }

        public static void Test3()
        {
            StringBuilder sb = new StringBuilder();
            sb.ToString();

            Console.WriteLine("输入你的姓名");
            string a = Console.ReadLine();
            sb.Append(a + "，");
            Console.WriteLine("输入你的年龄");
            string b = Console.ReadLine();
            sb.Append(b + "，");
            Console.WriteLine("输入你的家庭住址");
            string c = Console.ReadLine();
            sb.Append(c + "，");
            Console.WriteLine("输入你的兴趣爱好");
            string d = Console.ReadLine();
            sb.Append(d + "。");

            Console.WriteLine(sb);
        }
    }
}
