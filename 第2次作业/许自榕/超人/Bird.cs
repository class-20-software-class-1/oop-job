﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Bird : IFly, IEat
    {

        public Bird(string name)
        {
            Fly(name);
            Eat(name);
        }

        public void Eat(string name)
        {
            Console.WriteLine("{0}会吃饭", name);
        }

        public void Fly(string name)
        {
            Console.WriteLine("{0}会飞", name);
        }
    }
}
