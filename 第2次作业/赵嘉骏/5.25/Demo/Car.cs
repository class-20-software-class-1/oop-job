﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Car
    {
    
        private string brand;
        private string me;
        public string Brand { get => brand; set => brand = value; }
       public string Me { get => me; set => me = value; }
        //父
        public Car(string brand,string me)
        {
            this.brand = brand;
            this.me = me;
        }
      
        public void BatCar()
        {
            Console.WriteLine("我是{0}",this.brand);
        }
        public void Run()
        {
            Console.WriteLine("{0}开着最大马力正在火速奔跑中...",this.me);
        }
        
    }
}
