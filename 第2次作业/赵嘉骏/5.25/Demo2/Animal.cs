﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Animal
    {
        private string brand;
        
        public string Brand { get => brand; set => brand = value; }
        public Animal(string brand)
        {
            this.brand = brand;
        }

        public void Speak()
        {
            Console.WriteLine("我是{0}",brand);
        }
        public void Eat()
        {
            Console.WriteLine("{0}正在吃饭...",brand);
        }
    }
}
