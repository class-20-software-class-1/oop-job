﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Plane:Vehicle,IFlyable
    {
        public Plane(string brand2) : base(brand2)
        { }
        public void Fly()
        {

        }
        public void TakeOff()
        {


        }
        public void Land()
        {

        }
        public void CarryPassange()
        { 
        
        }
    }
}
