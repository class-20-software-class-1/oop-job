﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Animal a = new Animal("");
            Vehicle b = new Vehicle("");
            Plane c = new Plane("");
            Bird d = new Bird("鸟");
            d.Eat();
            Superman e = new Superman("超人");
            e.Eat();
        }
    }
}
