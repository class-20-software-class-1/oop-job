﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Animal
    {
        private string kind;

        public string Kind { get => kind; set => kind = value; }

        public Animal() { }

        public Animal(string kind) { this.kind = kind; }

        public void Eat()
        {
            Console.WriteLine("{0}能吃", this.kind);
        }
    }
}
