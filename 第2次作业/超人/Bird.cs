﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Bird : Animal, IFlyable
    {
        private string layEggs;

        public string LayEggs { get => layEggs; set => layEggs = value; }

        public Bird() { }

        public Bird(string layEggs) { this.layEggs = layEggs; }
        public void Fly()
        {
            throw new NotImplementedException();
        }

        public void Land()
        {
            throw new NotImplementedException();
        }

        public void TakeOff()
        {
            throw new NotImplementedException();
        }
    }
}
