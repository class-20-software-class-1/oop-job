﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class FlyCar:Car,IFly
    {
        public FlyCar(string brand):base(brand)
        { }

        public void Fly()
        {
            Console.WriteLine("一辆{0}在飞...",this.Brand);
        }
    }
}
