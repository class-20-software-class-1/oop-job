﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {

        static void Main(string[] args)
        {
            string str = "与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。";

            Sum1(str); //1、使用循环遍历的方法来实现。
            Sum2(str);//2、使用Replace方法来实现。
            Sum3(str);//3、使用Split()方法来实现。

            string str2 = "C# (英 文名为 CSharp) 是 微  软开发的一种 面向对 象的 编程 语言。C# 语言具备了面向对象 语言 的特 征，  即封装、继承、多态，并且添加了  事件和委托，增强了 编程的灵 活性。C# 语 言是  一种安全的、稳定的、简 单 的、面向对象的编程 语言 ，其语 法与 C++ 类似，但在编程过  程中要比 C++ 简单；它不仅去掉了 C++ 和 Java 语  言中的一些复杂特性，还提  供了可视化 工具，能够高效地 编写程序。C# 是运行  在.NE  T平台之上的  编程 语言。";
            Test4(str2);
            Test5();


        }
        //        1、使用循环遍历的方法来实现。
        static void Sum1(string str)
        {
            char[] arr = str.ToArray();
            char b = '码';
            char a = '类';
            int index1 = 0;
            foreach (char item in str)
            {
                if (item.Equals(a) | item.Equals(b))
                    index1++;
            }
            Console.WriteLine(index1);
        }

        //2、使用Replace方法来实现。
        static void Sum2(string str)
        {
            int index1 = (str.Length - str.Replace("类", "").Length);
            int index2 = (str.Length - str.Replace("码", "").Length);

            Console.WriteLine("类的个数为{0}，码的个数为{1}", index1, index2);
        }

        //3、使用Split()方法来实现。
        static void Sum3(string str)
        {
            string[] arr = str.Split('类');
            int index1 = arr.Length - 1;

            string[] arr2 = str.Split('码');
            int index2 = arr2.Length - 1;

            Console.WriteLine("类的个数为{0}，码的个数为{1}", index1, index2);
        }

        //        C# (英 文名为 CSharp) 是 微  软开发的一种 面向对 象的 编程 语言。
        //C# 语言具备了面向对象 语言 的特 征，  即封装、继承、多态，并且添加了  
        //事件和委托，增强了 编程的灵 活性。C# 语 言是  一种安全的、稳定的、简 单 的、面向对象的编程 语言 ，
        //其语 法与 C++ 类似，但在编程过  程中要比 C++ 简单；它不仅去掉了 C++ 和 Java 语  言中的一些复杂特性，
        //还提  供了可视化 工具，能够高效地 编写程序。C# 是运行  在.NE  T平台之上的  编程 语言。

        //去掉上面一段文字的所有空格，并统计空格数。
        static void Test4(string str2)
        {

            int index = (str2.Length - str2.Replace(" ", "").Length);

            Console.WriteLine("{0}", str2.Replace(" ", ""));
            Console.WriteLine("空格数位：{0}", index);
        }

        //三、在控制台下输入你的姓名、年龄、家庭住址和兴趣爱好，使用StringBuilder类把这些信息连接起来并输出。
        static void Test5()
        {
            Console.WriteLine("请输入你的名字：");
            string name = Console.ReadLine();

            Console.WriteLine(" 请输入你的年龄：");
            int age = int.Parse(Console.ReadLine());

            Console.WriteLine("请输入你的家庭住址：");
            string site = Console.ReadLine();

            Console.WriteLine("请输入你的兴趣爱好：");
            string hobby = Console.ReadLine();

            StringBuilder sb = new StringBuilder(20);

            sb.Append("我的名字是：" + name + " ");
            sb.Append("我的年龄是：" + age + " ");
            sb.Append("我的地址是：" + site + " ");
            sb.Append("我的爱好是：" + hobby + " ");

            Console.WriteLine(sb);
        }
    }
}
