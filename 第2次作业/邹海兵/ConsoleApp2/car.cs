﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp2
{
    class car
    {
        private string brand;

        public string Brand { get => brand; set => brand = value; }

        public car (string brand)
        {
            this.brand = brand;
            Console.WriteLine("{0}正在跑",this.brand);
        }
    }
}
