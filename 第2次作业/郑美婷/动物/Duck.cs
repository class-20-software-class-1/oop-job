﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Class
{
    class Duck:Public,ISwim,IFly
    {
        public Duck(string name,string ask):base(name,ask)
        {
            base.Ask = ask;
            base.Name = name;
        }
        public void swim()
        {
            Console.WriteLine("{0}我会游泳戏水~",base.Name);
        }
        public void fly()
        {
            Console.WriteLine("{0}我会飞高高啊~",base.Name);
        }
    }
}
