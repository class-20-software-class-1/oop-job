﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Class
{
    class Monkey : Public, ICtree
    {
        public Monkey(string name,string ask):base(name,ask)
        {
            base.Ask = ask;
            base.Name = name;
        }
        public void tree()
        {
            Console.WriteLine("{0}我会爬树~",base.Name);
        }
    }
}
