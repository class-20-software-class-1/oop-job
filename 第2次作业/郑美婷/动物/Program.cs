﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Class
{
    class Program
    {
        //2、猫、狗、鸭、猴，（吃、游泳、爬树）
        static void Main(string[] args)
        {
            Cat cat = new Cat("喵喵^·工·^", "汪汪汪");
            cat.Zoo();
            cat.tree();
            Console.WriteLine();
            Dog dog = new Dog("狗勾U·工·*U","汪汪汪");
            dog.Zoo();
            dog.tree();            
            dog.swim();
            Console.WriteLine();
            Monkey mk = new Monkey("猴叽n·v·n","叽叽叽");
            mk.Zoo();
            mk.tree();
            Console.WriteLine();
            Duck duck = new Duck("鸭砸·⑧·","嘎嘎嘎");
            duck.Zoo();
            duck.swim();
            duck.fly();
            Console.WriteLine();
        }
    }
}