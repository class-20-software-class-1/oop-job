﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Class
{
    class Public
    {
        private string name;
        private string ask;
        public string Name { get => name; set => name = value; }
        public string Ask { get => ask; set => ask = value; }

        public Public(string name,string ask)
        {
            this.name = name;
            this.ask = ask;
        }
        public void Zoo()
        { Console.WriteLine("我是一只{0}，我会{1}叫！",this.name,this.ask); }
    }
}