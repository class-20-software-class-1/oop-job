﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Class1
{
    class Animal
    {
        private string name;
        private string eat;

        public string Name { get => name; set => name = value; }
        public string Eat { get => eat; set => eat = value; }

        public Animal(string eat,string name)
        {
            this.Eat = eat;
            this.Name = name;
        }
        public Animal() { }
    }
}
