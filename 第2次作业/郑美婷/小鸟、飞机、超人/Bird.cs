﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Class1
{
    class Bird : Animal,IFlyable
    {
        public Bird(string name,string eat):base(name,eat)
        {
            base.Eat = eat;
            base.Name = name;
        }
        public void Fly()
        {
            Console.WriteLine("{0}我会飞行",base.Name); ;
        }

        public void Land()
        {
            Console.WriteLine("{0}我可以着陆", base.Name); ;
        }

        public void TakeOff()
        {
            Console.WriteLine("{0}我可以起飞", base.Name); ;
        }
        public void LayEggs()
        {
            Console.WriteLine("{0}我会下蛋", base.Name);
        }
        public void eat()
        {
            Console.WriteLine("{0}我会吃{1}，美味！",base.Name,base.Eat);
        }
    }
}
