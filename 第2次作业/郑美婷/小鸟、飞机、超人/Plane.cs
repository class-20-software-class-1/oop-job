﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Class1
{
    class Plane : Vehicle, IFlyable
    {
        public Plane(string brand):base(brand)
        {
            base.Brand = brand;
        }
        public void Fly()
        {
            Console.WriteLine("{0}我会飞",base.Brand);
        }

        public void Land()
        {
            Console.WriteLine("{0}我可以着陆", base.Brand);
        }

        public void TakeOff()
        {
            Console.WriteLine("{0}我可以起飞", base.Brand);
        }
        public void CarryPassange()
        {
            Console.WriteLine("{0}我可以载客",base.Brand);
        }
    }
}
