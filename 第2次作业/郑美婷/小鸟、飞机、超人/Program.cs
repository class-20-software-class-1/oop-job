﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Class1
{
    //4、超人、小鸟、飞机
    class Program
    {
        static void Main(string[] args)
        {
            Superman sman = new Superman("蜘蛛侠","火龙果");
            sman.eat();
            sman.Fly();
            sman.Land();
            sman.TakeOff();
            Console.WriteLine();
            Plane pl = new Plane("SHW46飞机");
            pl.Fly();
            pl.Land();
            pl.TakeOff();
            pl.CarryPassange();
            Console.WriteLine();
            Bird br = new Bird("鹦鹉","瓜子");
            br.eat();
            br.Fly();
            br.Land();
            br.TakeOff();
            br.LayEggs();
            Console.WriteLine();
        }
    }
}
