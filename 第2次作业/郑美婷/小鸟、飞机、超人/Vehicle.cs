﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Class1
{
    class Vehicle
    {
        private string brand;
        public string Brand { get => brand; set => brand = value; }
        public Vehicle(string brand)
        {
            this.brand = brand;
        }
        public Vehicle()
        { 
        
        }
    }
}
