﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P6
{
    class Program
    {
        //一、统计下面一段文字中“类”字和“码”的个数。

        //与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。

        //1、使用循环遍历的方法来实现。
        //2、使用Replace方法来实现。
        //3、使用Split()方法来实现。

        //二、
        //C# (英 文名为 CSharp) 是 微  软开发的一种 面向对 象的 编程 语言。C# 语言具备了面向对象 语言 的特 征，  即封装、继承、多态，并且添加了  事件和委托，增强了 编程的灵 活性。C# 语 言是  一种安全的、稳定的、简 单 的、面向对象的编程 语言 ，其语 法与 C++ 类似，但在编程过  程中要比 C++ 简单；它不仅去掉了 C++ 和 Java 语  言中的一些复杂特性，还提  供了可视化 工具，能够高效地 编写程序。C# 是运行  在.NE  T平台之上的  编程 语言。

        //去掉上面一段文字的所有空格，并统计空格数。

        //三、在控制台下输入你的姓名、年龄、家庭住址和兴趣爱好，使用StringBuilder类把这些信息连接起来并输出。
        static void Main(string[] args)
        {
            
            string str = "与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。";
 
            Console.WriteLine("——————————————————————————————————————");

            //1、使用循环遍历的方法来实现。
            char[] xunhuan = str.ToArray();
            int L = 0;    
            int M = 0;
            for (int i = 0; i < xunhuan.Length; i++)
            {
                if (xunhuan[i].Equals('类') == true)
                {
                    L++;
                }
                else if (xunhuan[i].Equals('码') == true)
                {
                    M++;
                }
                else 
                {
                }
            }
            Console.WriteLine("在句子中含有‘类’有{0}个，‘码’有{1}个", L, M);

            Console.WriteLine("——————————————————————————————————————");

            //2、使用Replace方法来实现。
            int c = 0;
            int d = 0;
            string xunhuan1 = str.Replace("码", "马");
            string xunhuan2 = str.Replace("类", "米");
            char[] arrr = str.ToArray();
            char[] arrrr = str.ToArray();
            for (int i = 0; i < arrr.Length; i++)
            {
                if (xunhuan1[i].Equals('类') == true)
                {
                    c++;
                }
            }         
            for (int i = 0; i < arrrr.Length; i++)
            {
                if (xunhuan2[i].Equals('码') == true)
                {
                    d++;
                }
            }
            Console.WriteLine("‘码’字的个数有{0}个，‘类’字的个数有{1}个", c, d);

            Console.WriteLine("——————————————————————————————————————");
            
            //3、使用Split()方法来实现。
            string[] arr1 = { "类" };
            string[] arr2 = { "码" };
            string[] arr3 = str.Split(arr1, StringSplitOptions.None);
            string[] arr4 = str.Split(arr2, StringSplitOptions.None);
            Console.WriteLine("字符串中含有‘类’的个数有{0}个，含有‘码’的个数有{1}个",(arr3.Length - 1), (arr4.Length - 1));

            Console.WriteLine("——————————————————————————————————————");

        }
    }
    }
