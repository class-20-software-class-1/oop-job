﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P4
{
    class Batmobile : Car, ISwim
    {
        public Batmobile(string brand) : base(brand)
        {
            base.Brand = brand;
        }
        public string swim { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public void flying()
        {
            Console.WriteLine("Look！有一辆{0}在飞！", this.Brand);
        }
        public void swimming()
        {
            Console.WriteLine("Wow!那辆{0}可以在水里前行，厉害！", this.Brand);
        }
    }
}
