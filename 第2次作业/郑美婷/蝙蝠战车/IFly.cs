﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P4
{
    interface IFly
    {
        string fly { get; set; }
        void flying();
    }
}