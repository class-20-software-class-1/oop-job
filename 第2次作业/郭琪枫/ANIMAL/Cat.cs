﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Cat : Animal, IClimb
    {
        public Cat(string name) : base(name)
        {
            this.Name = name;
        }

        public void Climb()
        {
            Console.WriteLine("{0}会爬树", this.Name);
        }
    }
}
