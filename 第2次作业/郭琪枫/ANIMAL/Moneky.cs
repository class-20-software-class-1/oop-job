﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Monkey : Animal, IClimb,ISwim
    {
        public Monkey(string name) : base(name)
        {
            this.Name = name;
        }

        public void Climb()
        {
            Console.WriteLine("{0}会爬树",this.Name);
        }

        public void Swim()
        {
            Console.WriteLine("{0}会游泳",this.Name);
        }
    }
}
