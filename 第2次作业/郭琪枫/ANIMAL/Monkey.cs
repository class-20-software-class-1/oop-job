﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Monkey:Animal,IClimb
    {
        public Monkey(string name) : base(name)
        { }

        public void Climb()
        {
            Console.WriteLine("{0}在爬树");
        }
    }
}
