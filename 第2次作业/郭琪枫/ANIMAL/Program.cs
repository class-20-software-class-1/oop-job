﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Cat cat = new Cat("猫");
            cat.Eat();
            Test(cat);
            Console.WriteLine("-----------------------------");
            Dog dog = new Dog("狗");
            dog.Eat();
            Test1(dog);
            Console.WriteLine("-----------------------------");
            Duck duck = new Duck("鸭子");
            duck.Eat();
            Test2(duck);
            Console.WriteLine("-----------------------------");
            Monkey monkey = new Monkey("猴子");
            monkey.Eat();
            Test3(monkey);
            Test4(monkey);

        }

        public static void Test(IClimb cat)
        {
            cat.Climb();
        }

        public static void Test1(ISwim dog)
        {
            dog.Swim();
        }

        public static void Test2(ISwim duck)
        {
            duck.Swim();
        }

        public static void Test3(ISwim monkey)
        {
            monkey.Swim();
        }

        public static void Test4(IClimb monkey)
        {
            monkey.Climb();
        }
    }
}
