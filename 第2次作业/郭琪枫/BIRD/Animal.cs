﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Animal
    {
        private string name;

        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        public Animal() { }

        public Animal(string name)
        {
            this.Name = name;
        }

        public void Eat()
        {
            Console.WriteLine("{0}能吃", this.name);
        }
    }
}
