﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Bird bird = new Bird("鸟");
            bird.Eat();
            bird.LayEggs();
            Test(bird);
            Console.WriteLine("------------------------");
            Plane plane = new Plane("飞机");
            plane.CarryPassenger();
            Test1(plane);
            Console.WriteLine("------------------------");
            Superman superman = new Superman("超人");
            superman.Eat();
            Test2(superman);
        }
        public static void Test(IFlyable bird)
        {
            bird.TakeOff();
            bird.Fly();
            bird.Land();
        }
        public static void Test1(IFlyable plane)
        {
            plane.TakeOff();
            plane.Fly();
            plane.Land();
        }
        public static void Test2(IFlyable superman)
        {
            superman.TakeOff();
            superman.Fly();
            superman.Land();
        }
    }
}
