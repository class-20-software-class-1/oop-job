﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Superman : Animal, IFlyable
    {
        public Superman(string name) : base(name)
        {
            this.Name = name;
        }
        public void Fly()
        {
            Console.WriteLine("{0}能飞",this.Name);
        }

        public void Land()
        {
            Console.WriteLine("{0}能着陆", this.Name);
        }

        public void TakeOff()
        {
            Console.WriteLine("{0}能起飞", this.Name);
        }
    }
}
