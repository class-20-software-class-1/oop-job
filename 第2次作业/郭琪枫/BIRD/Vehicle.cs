﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Vehicle
    {
        private string name;

        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        public Vehicle() { }

        public Vehicle(string name)
        {
            this.Name = name;
        }

        public void CarryPassenger()
        {
            Console.WriteLine("{0}我能携带乘客", this.name);
        }
    }
}
