﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Computer
    {
        private string brand;
        private IUSB usb1;
        private IUSB usb2;

        public string Brand { get => brand; set => brand = value; }
        internal IUSB Usb1 { get => usb1; set => usb1 = value; }
        internal IUSB Usb2 { get => usb2; set => usb2 = value; }

        public Computer(string brand)
        {
            this.brand = brand;
        }
        public void Start()
        {
            Console.WriteLine("{0}电脑正在开机", this.brand);
        }
        public void ReadData()
        {
            this.usb1.Read();
            this.usb2.Read();
        }

        public void StorageData()
        {
            this.usb1.Storage();
            this.usb2.Storage();
        }
        public void End()
        {
            Console.WriteLine("{0}电脑正在关机", this.brand);
        }
    }
}
