﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class MobileHD:MobileStorage,IUSB
    {
        public MobileHD(string brand) : base(brand)
        { }

        public void Read()
        {
            Console.WriteLine("{0}移动硬盘正在读取数据", this.Brand);
        }
        public void Storage()
        {
            Console.WriteLine("{0}移动硬盘正在存储数据", this.Brand);
        }
    }
}
