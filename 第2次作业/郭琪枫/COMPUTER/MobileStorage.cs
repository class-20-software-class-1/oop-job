﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class MobileStorage
    {
        private string brand;
        public string Brand
        {
            get { return this.brand; }
            set { this.brand = value; }
        }
        public MobileStorage(string brand)
        { this.brand = brand; }
        public MobileStorage() { }
    }
}
