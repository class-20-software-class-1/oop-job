﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            USB uSB = new USB("惠普");
            MobileHD mobileHD = new MobileHD("三星");

            uSB.Read();
            uSB.Storage();
            Console.WriteLine("-------------------------");
            mobileHD.Read();
            mobileHD.Storage();
            Console.WriteLine("-------------------------");
            Computer computer = new Computer("联想");
            computer.Usb1 = uSB;
            computer.Usb2 = mobileHD;

            computer.Start();
            computer.ReadData();
            computer.StorageData();
            computer.End();
        }
    }
}
