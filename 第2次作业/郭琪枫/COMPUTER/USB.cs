﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class USB:MobileStorage,IUSB
    {
        public USB(string brand) : base(brand)
        { }

        public void Read()
        {
            Console.WriteLine("{0}U盘正在读取数据",this.Brand);
        }
        public void Storage()
        {
            Console.WriteLine("{0}U盘正在存储数据", this.Brand);
        }
    }
}
