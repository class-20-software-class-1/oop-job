﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Batmobile:Car,IFly
    {
        public Batmobile(string brand) : base(brand)
        { 
        }

        public void Fly()
        {
            Console.WriteLine("一辆{0}在天上飞驰",this.Brand);
        }
    }
}
