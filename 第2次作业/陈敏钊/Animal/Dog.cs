﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Dog:Animal,ISwim
    {

        public override void Eat()
        {
            //this.Name = "狗";
            Console.WriteLine("我是{0}，我能吃", this.Name);
        }

        public void Swim()
        {
            Console.WriteLine("俺{0}，还能游泳", this.Name);
        }
        public Dog(string name) : base(name)
        {

        }
    }
}
