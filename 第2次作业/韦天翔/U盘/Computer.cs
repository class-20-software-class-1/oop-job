﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Computer
    {
        private string _computername; 
        private IUsb _usb1;
        private IUsb _usb2;

        public string Computername { get => _computername; set => _computername = value; }
        internal IUsb Usb1 { get => _usb1; set => _usb1 = value; }
        internal IUsb Usb2 { get => _usb2; set => _usb2 = value; }

        public Computer(string name)
        {
            this.Computername = name;
        }
        public void Star()
        {
            Console.WriteLine(this.Computername+"开机中");
        }
        public void End()
        {
            Console.WriteLine(this.Computername + "关机中");
        }
        public void ReadDota()
        {
            this.Usb1.Read();
            this.Usb2.Read();
        }
        public void WriteDota()
        {
            this.Usb2.Write();
            this.Usb1.Write();
        }
    }
}
