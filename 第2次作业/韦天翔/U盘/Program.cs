﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Computer c1 = new Computer("拯救者");
            c1.Star();
            Usb1 usb1 = new Usb1("金士顿固态硬盘");
            Usb2 usb2 = new Usb2("闪迪U盘");
            c1.Usb1 = usb1;
            c1.Usb2 = usb2;
            c1.ReadDota();
            c1.WriteDota();
            c1.End();
        }
    }
}
