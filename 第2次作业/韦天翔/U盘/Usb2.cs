﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Usb2:Usb,IUsb
    {
        public Usb2() { }
        public Usb2(string name)
        {
            this.Name = name;
        }
        public void Read()
        {
            Console.WriteLine("我是{0}，我正在读取数据", this.Name);
        }

        public void Write()
        {
            Console.WriteLine("我是{0}，我正在写入数据", this.Name);
        }
    }
}
