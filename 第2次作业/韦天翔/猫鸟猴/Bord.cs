﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Bord : Animal, IFly
    {
        public Bord() { }
        public Bord(string name)
        {
            this.Name = name;
        }
        public void Fly()
        {
            Console.WriteLine("我是{0}，我会飞",this.Name);
        }
    }
}
