﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Cat : Animal, IFly
    {
        public Cat() { }
        public Cat(string name)
        {
            this.Name = name;
        }
        public void Fly()
        {
            Console.WriteLine("我是{0}，我会喵喵叫",this.Name);
        }
    }
}
