﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Monky : Animal, IFly
    {
        public Monky() { }
        public Monky(string name)
        {
            this.Name = name;
        }
        public void Fly()
        {
            Console.WriteLine("我是{0}，我会爬树",this.Name);
        }
    }
}
