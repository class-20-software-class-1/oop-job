﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class BatCat:Cat,IFly
    {
        public BatCat(string name):base(name)
        {

        }
        public BatCat() { }
       public void Fly()
        {
            Console.WriteLine("我不仅能跑，我还能飞");
        }
    }
}
