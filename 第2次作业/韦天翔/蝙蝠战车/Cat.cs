﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Cat
    {
        private string _name;

        public string Name { get => _name; set => _name = value; }
        public Cat(string name)
        {
            this.Name = name;
        }
        public Cat() { }
        public void Run()
        {
            Console.WriteLine("我是{0}，我已经飙到极限了",this.Name);
        }
    }
}
