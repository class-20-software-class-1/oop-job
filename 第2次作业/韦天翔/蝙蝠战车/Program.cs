﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Cat cat1 = new Cat("宝骏730");
            cat1.Run();
            BatCat cat2 = new BatCat("蝙蝠战车");
            cat2.Run();
            cat2.Fly();
        }
    }
}
