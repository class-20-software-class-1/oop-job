﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Kun3
    {
        private string cName;
        private IOperation usb1;
        private IOperation usb2;


        public string CName1
        {
            get
            {
                return cName;
            }

            set
            {
                cName = value;
            }
        }

        internal IOperation Usb1
        {
            get
            {
                return usb1;
            }

            set
            {
                usb1 = value;
            }
        }

        internal IOperation Usb2
        {
            get
            {
                return usb2;
            }

            set
            {
                usb2 = value;
            }
        }

        public Computer(string cName)
        {
            this.cName = cName;
        }

        public void star()
        {
            Console.WriteLine("{0}电脑启动中...",cName);
            Console.WriteLine("启动完毕，读取移动硬盘中...");
            
        }
        public void operationStore()
        {
            usb1.store();
            usb2.store();
        }

        public void operationTake()
        {
            usb1.take();
            usb2.take();
        }
        public void end()
        {
            Console.WriteLine("电脑关机中...");
        }
    }
}
