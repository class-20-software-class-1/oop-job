﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//一、统计下面一段文字中“类”字和“码”的个数。

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            One();
            //Two();
            Console.ReadKey();
         
        }
        public static void One()
        {
            Console.WriteLine("原文为");
            string passage = "与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、 多态封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性减少代码的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，也体现了代码的灵活性它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。";
            Console.WriteLine(passage);
            //1、使用循环遍历的方法来实现。
            char[] arr = passage.ToArray();
            int num1 = 0;
            int num2 = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i].Equals('类') == true)
                {
                    num1++;
                }
                else if(arr[i].Equals('码') == true){
                    num2++;

                }
            }
                Console.WriteLine("码有{0}个",num2);
                Console.WriteLine("类有{0}个",num1);
            //2、使用Replace方法来实现。
            int num3 = 0;
            int num4 = 0;
            string str = passage.Replace("码", "1");
            char[] arr1 = str.ToArray();
            for (int i = 0; i < arr1.Length; i++)
            {
                if (str[i].Equals('1') == true)
                {
                    num3++;
                }
            }

            string st = passage.Replace("类", "2");
            char[] arr2 = st.ToArray();
            for (int i = 0; i < arr2.Length; i++)
            {
                if (st[i].Equals('2') == true)
                {
                    num4++;
                }
            }
            Console.WriteLine("码有{0}个",num3);
            Console.WriteLine("类有{0}个",num4);
            //3、使用Split()方法来实现
            string[] ma = { "码" };
            string[] Ma = passage.Split(ma, StringSplitOptions.RemoveEmptyEntries);
            Console.WriteLine("文字中“码”字的个数:" + (Ma.Length - 1));

            string[] lei = { "类" };
            string[] Lei = passage.Split(lei, StringSplitOptions.RemoveEmptyEntries);
            Console.WriteLine("文字中“类”字的个数:" + (Lei.Length - 1));

            Console.WriteLine();









        }
        public static void Two()
        {
            string passage="C# (英 文名为 CSharp) 是 微  软开发的一种 面向对 象的 编程 语言。C# 语言具备了面向对象 语言 的特 征，  即封装、继承、多态，并且添加了  事件和委托，增强了 编程的灵 活性。C# 语 言是  一种安全的、稳定的、简 单 的、面向对象的编程 语言 ，其语 法与 C++ 类似，但在编程过  程中要比 C++ 简单；它不仅去掉了 C++ 和 Java 语  言中的一些复杂特性，还提  供了可视化 工具，能够高效地 编写程序。C# 是运行  在.NE  T平台之上的  编程 语言。";
            
            string[] arr = {" "};
            string[] arr1 = passage.Split(arr, StringSplitOptions.None);
            string newpassage = passage.Replace(" ", "");
            Console.WriteLine("原文：");

            Console.WriteLine(passage);

            Console.WriteLine("修改后：");
            Console.WriteLine(newpassage);
            

            Console.WriteLine("文字中空格数的个数:" + (arr1.Length));

        }
    }
}
