﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class C
    {
        protected string brand;

        public string Brand
        {
            get { return this.brand; }
            set { this.brand = value; }
        }

        public C(string brand)
        {
            this.brand = brand;    
        }
        public void Run()
        {

            Console.WriteLine("{0},正在天上飞",brand);
        }
    }
}
