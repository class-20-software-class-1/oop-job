﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    class Program
    {
        static void Main(string[] args)
        {
            //与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、 多态。
            //封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节
            //，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。
            ////继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。
            //            多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，
            //                让类或接口中的成员表现出不同的作用。
            string str = "与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。";
            //1、使用循环遍历的方法来实现。
            int b = 0;
            int b1 = 0;
            for (int i = 0; i < str.Length; i++)
            {
                if ('类' == str[i])
                {
                    b++;
                }
                else if ('码' == str[i])
                {
                    b1++;
                }

            }

            Console.WriteLine("类的个数" + b);
            Console.WriteLine("码的个数" + b1);

            Console.WriteLine();
            //2、使用Replace方法来实现。
            str = str.Replace('类', '累');
            str = str.Replace('码', '马');
            int a = 0;
            int c = 0;
            for (int i = 0; i < str.Length; i++)
            {
                if ('累' == str[i])
                {
                    a++;
                }
                if ('马' == str[i])
                {
                    c++;
                }

            }
            Console.WriteLine("累的个数" + b);
            Console.WriteLine("马的个数" + b1);
            Console.WriteLine();
            //3、使用Split()方法来实现。

            string[] condition = { "累" };
            string[] result = str.Split(condition, StringSplitOptions.None);
            Console.WriteLine("字符串中含有累的个数为：" + (result.Length - 1));
            string[] d = { "马" };

            string[] ff = str.Split(d, StringSplitOptions.None);
            Console.WriteLine("字符串中含有马的个数为：" + (ff.Length - 1));
        }
        static void qq()
        {
            //三、在控制台下输入你的姓名、年龄、家庭住址和兴趣爱好，使用StringBuilder类把这些信息连接起来并输出。

            StringBuilder sb = new StringBuilder();
            Console.WriteLine("输入你的姓名");
            string a = Console.ReadLine();
            Console.WriteLine("输入你的年龄");
            string b = Console.ReadLine();
            Console.WriteLine("输入你的家庭住址");
            string c = Console.ReadLine();
            Console.WriteLine("输入你的兴趣爱好");
            string d = Console.ReadLine();
            sb.Append(a);
            sb.Append(b);
            sb.Append(c);
            sb.Append(d);

            Console.WriteLine(sb);
        }
        static void pp()
        {
            //            二、
            //C# (英 文名为 CSharp) 是 微  软开发的一种面向对 象的 编程 语言。C# 语言具备了面向对象 语言 的特 征，
            //即封装、继承、多态，并且添加了  事件和委托，增强了 编程的灵 活性。
            //C# 语 言是  一种安全的、稳定的、简 单 的、面向对象的编程 语言 ，其语 法与 C++ 类似，
            // 但在编程过  程中要比 C++ 简单；它不仅去掉了 C++ 和 Java 语  言中的一些复杂特性，
            //还提 供了可视化 工具，能够高效地 编写程序。C# 是运行  在.NE  T平台之上的  编程 语言。
            //去掉上面一段文字的所有空格，并统计空格数。
            string a = "C# (英 文名为 CSharp) 是 微  软开发的一种 面向对 象的 编程 语言。C# 语言具备了面向对象 语言 的特 征，  即封装、继承、多态，并且添加了  事件和委托，增强了 编程的灵 活性。C# 语 言是  一种安全的、稳定的、简 单 的、面向对象的编程 语言 ，其语 法与 C++ 类似，但在编程过  程中要比 C++ 简单；它不仅去掉了 C++ 和 Java 语  言中的一些复杂特性，还提  供了可视化 工具，能够高效地 编写程序。C# 是运行  在.NE  T平台之上的  编程 语言。";
            string f = a.Replace(" ", "");
            Console.WriteLine("去空格:  " + f);
            string[] b = { " " };
            Console.WriteLine();
            string[] c = a.Split(b, StringSplitOptions.RemoveEmptyEntries);
            Console.WriteLine("空格数 :" + (c.Length - 1));

        }
    }
}
