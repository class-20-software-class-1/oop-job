﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Dog : Amelica, swim
    {

        public Dog(string eat)
        {
            this.eat = eat;
        }

        public void swimm()
        {
            Console.WriteLine("小狗会游泳");
        }

        public override void tell()
        {
            Console.WriteLine("我是小狗，我喜欢吃{0}",this.eat);
        }
    }
}
