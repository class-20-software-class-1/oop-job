﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class duck : Amelica,swim
    {
        public duck()
        {
            
        }
        public duck(string eat)
        {
            this.eat = eat;
        }

        public void swimm()
        {
            Console.WriteLine("小鸭子会游泳");
        }

        public override void tell()
        {
            Console.WriteLine("我是小鸭子，我喜欢吃{0}", this.eat);
        }
    }
}
