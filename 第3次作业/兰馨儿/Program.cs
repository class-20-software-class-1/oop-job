﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//1、生成一个随机整型数组，长度是10，内容是1 ~10，数组内容不重复。

//2、生成0 - 5之间的随机小数，保留两位小数。

//3、生成4 - 7之间的随机小数，保留两位小数。

namespace ConsoleApp7
{
    class Program
    {
        static void Main(string[] args)
        {
            // 1、生成一个随机整型数组，长度是10，内容是1 ~10，数组内容不重复。
            Random rd = new Random();
            int[] arr = new int[10];

            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = rd.Next(10) + 1;
                for (int j = 0; j < i; j++)
                {
                    if (arr[i] == arr[j])
                    {
                        arr[i] = rd.Next(10) + 1;
                        j = -1;
                    }
                }
                Console.Write(arr[i] + " ");
            }
            Console.WriteLine();

            //2、生成0 - 5之间的随机小数，保留两位小数。
            //帮助理解：生成3-5之间的小数     3 + random.NextDouble() * 2

            Console.WriteLine(rd.NextDouble()*5);
            Console.WriteLine(rd.Next(0,5) + rd.NextDouble());

            //3、生成4 - 7之间的随机小数，保留两位小数。
            Console.WriteLine(4 + rd.NextDouble()*3);
            Console.WriteLine(rd.Next(4,7) + rd.NextDouble());

      

        }

    }
}
