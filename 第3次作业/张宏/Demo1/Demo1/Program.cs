﻿using System;

namespace Demo1
{
    class Program
    {
        static void Main(string[] args)
        {
            string str = "与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。";

            ForString(str);
            ReplaceString(str);
            SplitString(str);

        }
        static void ForString(string str)
        {
            char c = str[25];

            int num1 = 0;
            int num2 = 0;

            for (int i = 0; i < str.Length; i++)
            {
                if (str[i]== '类')
                {
                    num1++;
                }
                else if (str[i] == '码')
                {
                    num2++;
                }
            }
            Console.WriteLine("类有{0}个，码有{1}个。",num1,num2);
         }
        static void ReplaceString(string str)
        {
            int num1 = str.Length-str.Replace("类", "").Length;
            int num2 = str.Length-str.Replace("码", "").Length;
            Console.WriteLine("类共有{0}个，码共有{1}个",num1,num2);
        }
        static void SplitString(string str)
        {
            char[] splitnum1 = { '类' };
            char[] splitnum2 = { '码' };

            int num1 = str.Split(splitnum1).Length - 1;
            int num2 = str.Split(splitnum2).Length - 1;

            Console.WriteLine("类出现了{0}次，码出现了{1}次",num1,num2);
        }
    }
}
