using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

//1、生成一个随机整型数组，长度是10，内容是1 ~10，数组内容不重复。



namespace ConsoleApp1
{
    class Program
    {
        private static object rnd;

        static void Main(string[] args)
        {
            int[] intArr = new int[10];
            ArrayList myList = new ArrayList();
            Random rnd = new Random();
            while (myList.Count < 10)
            {
                int num = rnd.Next(1, 11);
                if (!myList.Contains(num))
                    myList.Add(num);
            }
            for (int i = 0; i < 10; i++)
            {
                intArr[i] = (int)myList[i];
                Console.Write("{0} ", intArr[i]);
                Console.WriteLine();
               
            } 
            Console.WriteLine("-------------------------------");
                Text2();
//2、生成0-5之间的随机小数，保留两位小数。

        }
        public static void Text2()   
        {
            Random rd = new Random();
            double a = rd.NextDouble() * (5 - 0);
            a = Math.Round(a, 2);
            Console.WriteLine(a);
            Console.WriteLine("-------------------------------");
            Text3();
        }
//3、生成4-7之间的随机小数，保留两位小数。  
        public static void Text3()
        {
            Random rd = new Random();
            double b= rd. NextDouble() * (7 - 4) + 4;
            b = Math.Round(b, 2);
            Console.WriteLine(b);
        
        }
       

        
    }
}