﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo5
{
    class Program
    {
        static void Main(string[] args)
        {
            string a = "与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。";

            Test5();
        }
        //1、使用循环遍历的方法来实现。
        static void Re(string a)
        {
            int num1 = 0;
            int num2 = 0;
            foreach (char item in a)
            {

                if (item.Equals('类'))
                {
                    num1 += 1;
                }
                if (item.Equals('码'))
                {
                    num2 += 1;
                }
            }
            Console.WriteLine("码的个数是{0}，类的个数是{1}", num1, num2);
            Console.ReadKey();
        }
        //2、使用Replace方法来实现。
        static void Replace(string a)
        {
            string b = a.Replace("类", "");
            int num1 = a.Length - b.Length;
            string c = a.Replace("码", "");
            int num2 = a.Length - c.Length;
            Console.WriteLine("码的个数是{0}，类的个数是{1}", num1, num2);
        }

        //3、使用Split()方法来实现。
        static void Split(string a)
        {
            string[] arr = { "类" };
            string[] arr1 = a.Split(arr, StringSplitOptions.RemoveEmptyEntries);
            int num1 = arr1.Length - 1;

            string[] brr = { "码" };
            string[] brr1 = a.Split(brr, StringSplitOptions.RemoveEmptyEntries);
            int num2 = brr1.Length - 1;
            Console.WriteLine("码的个数是{0}，类的个数是{1}", num1, num2);
        }
        static void S2()
        {
            string a = "C# (英 文名为 CSharp) 是 微  软开发的一种 面向对 象的 编程 语言。C# 语言具备了面向对象 语言 的特 征，  即封装、继承、多态，并且添加了  事件和委托，增强了 编程的灵 活性。C# 语 言是  一种安全的、稳定的、简 单 的、面向对象的编程 语言 ，其语 法与 C++ 类似，但在编程过  程中要比 C++ 简单；它不仅去掉了 C++ 和 Java 语  言中的一些复杂特性，还提  供了可视化 工具，能够高效地 编写程序。C# 是运行  在.NE  T平台之上的  编程 语言。";
            //去掉上面一段文字的所有空格，并统计空格数。
            string b = a.Replace(" ", "");
            Console.WriteLine("空格数为：" + (a.Length - b.Length));
        }

        static void S3()
        {
            //在控制台下输入你的姓名、年龄、家庭住址和兴趣爱好，使用StringBuilder类把这些信息连接起来并输出。
            StringBuilder sb = new StringBuilder();

            string[] a = { };
            Console.WriteLine("请输入你的姓名、年龄、家庭住址和兴趣爱好");
            Console.WriteLine("姓名：");
            string Info = Console.ReadLine();
            sb.Append(Info);
            Console.WriteLine("年龄：");
            Info = Console.ReadLine();
            sb.Append(Info);
            Console.WriteLine("家庭住址：");
            Info = Console.ReadLine();
            sb.Append(Info);
            Console.WriteLine("兴趣爱好：");
            Info = Console.ReadLine();


            Console.WriteLine(sb.Append(Info));

        }
    }
}
