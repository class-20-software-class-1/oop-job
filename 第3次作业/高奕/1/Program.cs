﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            //circulation();
            //Replace();
            //Split();
            //IsSpace();
            StringBuilderTest test = new StringBuilderTest();
            test.Write();
        }
        static void IsSpace() 
        {
            string str = "C# (英 文名为 CSharp) 是 微  软开发的一种 面向对 象的 编程 语言。C# 语言具备了面向对象 语言 的特 征，  即封装、继承、多态，并且添加了  事件和委托，增强了 编程的灵 活性。C# 语 言是  一种安全的、稳定的、简 单 的、面向对象的编程 语言 ，其语 法与 C++ 类似，但在编程过  程中要比 C++ 简单；它不仅去掉了 C++ 和 Java 语  言中的一些复杂特性，还提  供了可视化 工具，能够高效地 编写程序。C# 是运行  在.NE  T平台之上的  编程 语言。";

            //Console.WriteLine(str.Length);

            //string str = "   wdawdaw   ";
            //Console.WriteLine(str.Length);
            //Console.WriteLine(str.Trim());
            int num=0;
            foreach (var item in str)
            {
                if (item == ' ')
                {
                    num = num + 1;
                }
            }

            Console.WriteLine("该段文字里的空格的数量为"+num+"\n");
            Console.WriteLine("重新编译后" + "\n");
            Console.WriteLine(str.Replace(" ",""));

        }

        static void circulation() //循环
        {
             string str = "与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。";

            //char char1 = '类';
            int num1 = 0;
            int num2 = 0;
            for (int i = 0; i < str.Length; i++)
            {
                if ('类' == str[i])   //str[i]是char类型所以不能与string类型比较 要用单引号
                {
                    num1 = num1 + 1;
                }

            }
            Console.WriteLine("类在这一段文字中出现的次数为" + num1);



            foreach (var item in str)
            {
                if (item.Equals('码'))
                {
                    num2 = num2 + 1;
                }
            }
            Console.WriteLine("码在这一段文字中出现的次数为" + num2);
        }
        static void Replace() 
        {
            string str = "与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。";

            int num = str.Length - str.Replace("类", String.Empty).Length;
            Console.WriteLine(num);
            //Console.WriteLine(str.Replace("类","a")); //Replac
        }

        static void Split() //分割字符串
        {
            string str = "与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。";
            int num = 0;
            char[] seprator = new char[] { '码','类' };

            string[] arr = str.Split(seprator);
            for (int i = 0; i < arr.Length; i++)
            {
                Console.Write(arr[i]+" ");
            }
            Console.WriteLine();

            num = arr.Length - 1;
            Console.WriteLine("该段文字中类和码的个数为"+num);



            //Console.WriteLine(str.Length);查看分割后字段长度不同
            //Console.WriteLine(arr.Length);

            //int num  = str.Split(new char[] { '码','类' }).Length - 1;
        }
    }
}
