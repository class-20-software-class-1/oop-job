﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp14
{
    class Program
    {
        static void Main(string[] args)
        {

            //            1、生成一个随机整型数组，长度是10，内容是1~10，数组内容不重复。

            //2、生成0 - 5之间的随机小数，保留两位小数。

            //3、生成4 - 7之间的随机小数，保留两位小数。
            
            int[] arr = new int[10];

            Random ran = new Random();

            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = ran.Next(10) + 1;
                for (int j = 0; j < i; j++)
                {
                    if (arr[i] == arr[j])
                    {
                        arr[i] = ran.Next(10) + 1;
                        j = -1;
                    }
                }
                Console.Write(arr[i] + " ");
            }
            Console.WriteLine();
            Random random = new Random();
            double a = random.Next(5) + random.NextDouble();
            Console.WriteLine(Math.Ceiling(a*100)/100.0);
            Random ra= new Random();
            double c = ra.Next(4, 7) + ra.NextDouble();
            Console.WriteLine( Math.Ceiling(c*100)/100.0);
            
        }
    }
}
