﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Test1();
            Test2();
            Test3();
        }
        //1.生成一个随机整数数组，长度是10，内容是1~10，数组内容不重复。
        public static void Test1()
        {
            Random ran = new Random();

            int[] arr = new int[10];

            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = ran.Next(1, 11);
                for (int j = 0; j < arr.Length; j++)
                {
                    if (arr[i] == arr[j])
                    {
                        arr[i] = ran.Next(1, 11);
                        j = j-1;
                    }
                }
                Console.WriteLine(arr[i] + " ");
            }
            Console.WriteLine();
        }

        //2.生产0-5之间的随机小数，保留两位小数。
        public static void Test2()
        {
            Random ran = new Random();

            double index = ran.Next(0, 6) + ran.NextDouble();

            index = Math.Round(index, 2);

            Console.WriteLine(index);
        }

        //3.生成4-7之间的随机小数，保留两位小数。
        public static void Test3()
        {
            Random ran = new Random();

            double index = ran.Next(4, 8) + ran.NextDouble();

            index = Math.Round(index, 2);

            Console.WriteLine(index);
        }
    }
}
