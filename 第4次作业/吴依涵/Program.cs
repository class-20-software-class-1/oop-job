﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Random ran = new Random();
            //1、生成一个随机整型数组，长度是10，内容是1~10，数组内容不重复。
            int[] vs = new int[10];
            for (int i = 0; i < vs.Length; i++)
            {
                vs[i] = ran.Next(1, 11);
                for (int j = 0; j < i; j++)
                {
                    if (vs[i] == vs[j])
                    {
                        vs[i] = ran.Next(1, 11);
                        j = -1;
                    }
                }
                Console.Write(vs[i] + ",");
            }
            Console.WriteLine("");


            //2、生成0 - 5之间的随机小数，保留两位小数。

            double a = ran.Next(5) + ran.NextDouble();
            string a2 = Convert.ToString(a);
            Console.WriteLine(a2.Substring(0, a2.IndexOf('.') + 3));

    


            //3、生成4 - 7之间的随机小数，保留两位小数。
            double b = ran.Next(4,7) + ran.NextDouble();
            string b2 = Convert.ToString(b);
            Console.WriteLine(b2.Substring(0, b2.IndexOf('.') + 3));
            //1.算出要求的数的长度 2.找出索引的值 3.加上多少索引值会等于需要的长度索引值
        }
    }
}
