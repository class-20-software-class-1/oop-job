﻿using System;

namespace Demo7
{
    class Program
    {
        static void Main(string[] args)
        {
            //3、生成4-7之间的随机小数，保留两位小数。
            Random ran = new Random();

            int num1 = ran.Next(4, 7); 
            //Console.WriteLine(num1);

            double num2 = ran.NextDouble();
            //Console.WriteLine(Math.Round(num2, 2));

            double num3;
            num3 = num1 + num2;
            Console.WriteLine(Math.Round(num3, 2));
        }
    }
}
