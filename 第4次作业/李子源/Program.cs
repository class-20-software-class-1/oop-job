﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Test();
            Test2();
            Test3();
		}

        private static void Test()
        {
            int[] a = new int[10];
            Random ran = new Random();
            int b = ran.Next(1, 11);
            for (int i = 0; i < a.Length; i++)
            {
                a[i] = b;
                for (int j = 0; j < a.Length; j++)
                {
                    if (a[j] == a[i] && i!=j)
                    {
                        a[i] = ran.Next(1, 11);
                        j = -1;
                    }
                }
            }
            for (int i = 0; i < a.Length; i++)
            {
                Console.WriteLine(a[i] + " ");
            }
            Console.WriteLine("---------------------------------------");
        }

        public static void Test2()
        {
            Random ran = new Random();
            for (int i = 0; i < 10; i++)
            {
                double a = ran.NextDouble();
                double b = ran.Next(5);
                double c = a + b;
                int d = (int)(c * 1000);
                Console.WriteLine(d / 100.0);
            }
            Console.WriteLine("---------------------------------------");
        }

        public  static void Test3()
        {
            Random ran = new Random();
            for (int i = 0; i < 10; i++)
            {
                double a = ran.NextDouble();
                double b = ran.Next(4,7);
                double c = a + b;
                int d = (int)(c * 100);
                Console.WriteLine(d/100.0);
            }
            Console.ReadKey();
        }
    }
}
