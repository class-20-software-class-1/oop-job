﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("1、生成一个随机整型数组，长度是10，内容是1 ~10，数组内容不重复");
            Text1();
            Console.WriteLine();
            Console.WriteLine("2、生成0-5之间的随机小数，保留两位小数。");
            Text2();
            Console.WriteLine("3、生成4-7之间的随机小数，保留两位小数。");
            Text3();
            
        }
        public static void Text1()
        {
            //1、生成一个随机整型数组，长度是10，内容是1 ~10，数组内容不重复。
            int[] arr = new int[10];
            Random random = new Random();

            int temp = -1;
            bool result = false;
            for (int i = 0; i < arr.Length; i++)
            {
                result = true;
                while (result)
                {
                    result = false;
                    temp = random.Next(1, 11);
                    for (int j = 0; j < i; j++)
                    {
                        if (temp == arr[j])
                        {
                            result = true;
                            break;
                        }
                    }
                }
                arr[i] = temp;
            }
            for (int i = 0; i < arr.Length; i++)
            {
                Console.Write(arr[i] + " ");
            }
        }
        public static void Text2()
        {
            //2、生成0-5之间的随机小数，保留两位小数。
            Random random = new Random();
            double num = 0 + random.NextDouble() * 5;
            Console.WriteLine("生成的数字:{0}",num = Math.Round(num, 2));
        }
        public static void Text3()
        {
            //3、生成4-7之间的随机小数，保留两位小数。
            Random random = new Random();
            double num = 4 + random.NextDouble() * 7;
            Console.WriteLine("生成的数字:{0}", num = Math.Round(num, 2));
        }
    }
}
