﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            

            Test1();
            Test2();
            Test3();
        }

        public static void Test1() 
        {
            //1、生成一个随机整型数组，长度是10，内容是1~10，数组内容不重复。
            int[] arr = new int[10];
            Random r = new Random();
            int a = r.Next(1, 11);
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = a;
                for (int j = 0; j < arr.Length; j++)

                {
                    if (arr[j] == arr[i] && i != j)
                    {
                        arr[i] = r.Next(1, 11);
                        j = -1;
                    }
                }
            }
            for (int i = 0; i < arr.Length; i++)
            {
                Console.Write(arr[i] + " ");
            }
        }

        public static void Test2()
        {
            //2、生成0 - 5之间的随机小数，保留两位小数。
            Random r = new Random();

            for (int i = 0; i < 10; i++)
            {
                double a = r.NextDouble();
                double b = r.Next(5);
                double c = a + b;

                int o = (int)(c * 100);
                Console.WriteLine(o / 100.0);
            }

                Console.WriteLine();

        }

        public static void Test3()
        {
            //3、生成4 - 7之间的随机小数，保留两位小数。
            Random r = new Random();

            for (int i = 0; i < 10; i++)
            {
                double a = r.NextDouble();
                double b = r.Next(4,7);
                double c = a + b;

                int o = (int)(c * 100);
                Console.WriteLine(o / 100.0);
            }
        }

    }
}
