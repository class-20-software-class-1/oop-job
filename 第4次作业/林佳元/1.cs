﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Test3();
        }

        //1、生成一个随机整型数组，长度是10，内容是1 ~10，数组内容不重复。
        static void Test1() 
        {
            int[] a = new int[10];
            Random ran = new Random();
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(ran.Next(1,11));
                
            }
            
        }

        //2、生成0-5之间的随机小数，保留三位小数。
        static void Test2()
        {
            Random ran = new Random();
            for (int i = 0; i < 10; i++)
            {
                //Console.WriteLine(ran.Next(5)+ran.NextDouble());
                double a = ran.Next(5) + ran.NextDouble();
                //Console.WriteLine(Math.Round(a,3));
                Console.WriteLine(a.ToString("#0.000"));
            }
        }

        //3、生成4-7之间的随机小数，保留两位小数。
        static void Test3()
        {
            Random ran = new Random();
            for (int i = 0; i < 10; i++)
            {
                //Console.WriteLine(ran.Next(4,7)+ran.NextDouble());
                double a = ran.Next(4,7) + ran.NextDouble();
                //Console.WriteLine(Math.Round(a, 2));
                Console.WriteLine(a.ToString("#0.00"));
            }
        }
    }
}
