﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Random ran = new Random();
            //1、生成一个随机整型数组，长度是10，内容是1~10，数组内容不重复。
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(ran.Next(0,11));

            }
            Console.WriteLine("-----------------------------------");
            //2、生成0 - 5之间的随机小数，保留两位小数。
            for (int i = 0; i < 10; i++)
            {
                double d = ran.Next(0, 5) + ran.NextDouble();
                Console.WriteLine(d.ToString("#0.00"));
               
            }
            Console.WriteLine("-----------------------------------");

            //3、生成4 - 7之间的随机小数，保留两位小数。
            
            for (int i = 0; i < 10; i++)
            {
                double e = ran.Next(4, 7) + ran.NextDouble();
                Console.WriteLine( e.ToString("#0.00"));
            }

        }
    }
}
