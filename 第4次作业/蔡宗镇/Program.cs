﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chp
{
    class Program
    {
        static void Main(string[] args)
        {
            Random ra = new Random();
            //1、生成一个随机整型数组，长度是10，内容是1~10，数组内容不重复。
            int[] arr = new int[10];
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = ra.Next(10);            
                    for (int j = 0; j < i; j++)
                    {
                        
                            if (arr[j]==arr[i])
                        {
                           arr[i] = ra.Next(10);
                            j = -1;
                        }
                }
                Console.Write(arr[i]+"  ");
                
            }
            Console.WriteLine();
            //2、生成0 - 5之间的随机小数，保留两位小数。
           
             double num1 = ra.Next(5) + ra.NextDouble();
                string num02 = Convert.ToString(num1);
                Console.WriteLine( num02.Substring(0,num02.IndexOf('.')+3));
            
            Console.WriteLine();
            //3、生成4 - 7之间的随机小数，保留两位小数。
            double num2 = 3*ra.NextDouble() + 4;
            string num03 = Convert.ToString(num2);
            Console.WriteLine(num03.Substring(0, num03.IndexOf('.') + 3));
        }
    }
}
