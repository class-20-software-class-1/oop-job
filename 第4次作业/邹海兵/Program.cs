﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            ran1();//1-10整数
            Console.WriteLine();
            ran2();//0-5浮点
            ran3();//4-7浮点
        }

        //10内的随机数
        static void ran1()
        {
            Random ran = new Random();
            int[] arr = new int[10];
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = ran.Next(1,11);
                for (int j = 0; j < i; j++)
                {
                    if (arr[i] == arr[j])
                    {
                        arr[i] = ran.Next(1,11);
                        j = -1;
                    }
                }
                Console.Write(arr[i] + " ");
            }
        }
        //2、生成0 - 5之间的随机小数，保留两位小数。
        static void ran2()
        {
            Random ran2 = new Random();
            int num1 = ran2.Next(5);
            double num2 = ran2.NextDouble();
            double result = num1 + num2;
            string str = result.ToString();
            string result2 = str.Substring(0, 4);
            Console.WriteLine(result2);
        }
        static void ran3()
        {
            Random ran3 = new Random();
            int num1 = ran3.Next(4);
            double num2 = ran3.NextDouble();
            double a = num1 + num2 + 4;
            string result3 = a.ToString();
            string result = result3.Substring(0, 4);
            Console.WriteLine(result);
        }
    }
}
