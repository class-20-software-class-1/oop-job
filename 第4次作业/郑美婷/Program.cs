﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C
{
    class Program
    {
        static void Main(string[] args)
        {
            //1、生成一个随机整型数组，长度是10，内容是1 ~10，数组内容不重复。
            Random ra = new Random();
            for (int i = 0; i < 10; i++)
            {
                double b = ra.Next(1, 10);
                if (b.Equals(true))
                {
                    Console.Write(b + " ,");
                }
                Console.WriteLine();
            }
            Console.WriteLine();
            //2、生成0-5之间的随机小数，保留两位小数。

            for (int i = 0; i < 20; i++)
            {
                //double a = ra.NextDouble() * (5 - 0) + 0;
                Console.Write(Math.Round(ra.NextDouble() * (5 - 0) + 0, 2) + " ");
            }

            Console.WriteLine();
            //3、生成4-7之间的随机小数，保留两位小数。
            Console.WriteLine();

            for (int i = 0; i < 20; i++)
            {

                Console.Write(Math.Round(ra.NextDouble() * (7 - 4) + 4, 2) + " ");
            }

            Console.WriteLine();

        }
    }
}
