﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {

            //1、生成一个随机整型数组，长度是10，内容是1~10，数组内容不重复。
            Random ran = new Random();
            int[] vs = new int[10];
            for (int i = 0; i < vs.Length; i++)
            {
                vs[i] = ran.Next(1, 11);
                for (int j = 0; j < i; j++)
                {
                    if (vs[i] == vs[j])
                    {
                        vs[i] = ran.Next(1, 11);
                        j = -1;
                    }
                }
                Console.Write(vs[i] + ",");
            }
            Console.WriteLine("");
            ////2、生成0 - 5之间的随机小数，保留两位小数。
            int num1 = ran.Next(5);
            double num = num1 + ran.NextDouble() * (5 - num1);
            Console.WriteLine("生成0 - 5之间的随机小数，保留两位小数" + Math.Round(num, 2));
            ////3、生成4 - 7之间的随机小数，保留两位小数。
            double num2 = 4 + ran.NextDouble() * 3;
            Console.WriteLine("生成4 - 7之间的随机小数，保留两位小数" + Math.Round(num2, 2));
        }
    }
}
