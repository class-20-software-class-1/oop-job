﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {

            ForArray();
            RandomdDou1();
            RandomdDou();


        }
       
            static void ForArray()
        {
            int j = 0;
            ArrayList iList = new ArrayList();
            for (int i = 0; i < 10; i++)
            {
                Random rd = new Random();
                do
                {
                    j = rd.Next(1, 11);
                } while (iList.Contains(j));
                iList.Add(j);
            }
            foreach (int iRand in iList)
            {
                Console.WriteLine(iRand);
            }
        }

        static void RandomdDou1()       //4-7之间的随机小数，保留两位小数。
        {
            Random random = new Random();

            for (int i = 0; i < 10; i++)
            {
                double dou1 = 4 + random.NextDouble() * 3;
                string strdou1 = Convert.ToString(dou1);
                Console.WriteLine(strdou1.Substring(0, strdou1.IndexOf(".") + 3));
            }
        }
        static void RandomdDou()        //0-5之间的随机小数，保留两位小数。
        {
            Random random = new Random();

            for (int i = 0; i < 10; i++)
            {

                double dou = random.NextDouble() * 5;
                string strdou = Convert.ToString(dou);
                //int intdou = (int)(dou * 100);  //先乘 强转为int 然后再除
                //Console.WriteLine(intdou/100.0); 

                //Console.WriteLine(Math.Floor(dou*100)/100.0);  //向下取整取 两位小数

                Console.WriteLine(strdou.Substring(0, strdou.IndexOf(".") + 3)); //截取 先转换为字符串 截取的位数加1才等于要截取的位数            
            }


        }
    }
}
