﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//1、学生类：学号、姓名、年龄
//2、请选择：1、添加学生信息。2、查询学生信息，3、删除学生信息 4、退出
//3、重复的学号不能添加。
//4、查询学生信息功能中有：1、查询所有（按学号排序）2、按学号查询（查没有，则打印查无此学生）

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Hashtable ht = new Hashtable();

            Student s1 = new Student(1, "张三", 18);
            Student s2 = new Student(2, "李四", 20);
            Student s3 = new Student(3, "王五", 19);

            ht.Add(s1.Name, s1);
            ht.Add(s2.Name, s2);
            ht.Add(s3.Name, s3);

            Student(ht);
        }
        static void Student(Hashtable ht)
        {
            while (true)
            {
                Console.WriteLine("请选择：1、添加学生信息。2、查询学生信息，3、删除学生信息 4、退出");
                int num = int.Parse(Console.ReadLine());
                switch (num)
                {
                    case 1:
                        Add(ht);
                        break;
                    case 2:
                        Select(ht);
                        break;
                    case 3:
                        Remove(ht);
                        break;
                    case 4:
                        Console.WriteLine("系统退出"); 
                        break;
                    default:
                        break;
                }
            }
        }

        private static void Add(Hashtable ht)
        {
            Console.WriteLine("请输入学生学号：");
            int num = int.Parse(Console.ReadLine());
            Console.WriteLine("请输入学生姓名：");
            string name = Console.ReadLine();
            Console.WriteLine("请输入学生年龄：");
            int age = int.Parse(Console.ReadLine());

            Student s4 = new Student(num, name, age);
            ht.Add(s4.Num, s4);
            Console.WriteLine("添加成功！");
            Student(ht);

        }



        private static void Remove(Hashtable ht)
        {
            Console.WriteLine("请输入要删除学生的学号");
            string num2 = Console.ReadLine();
            foreach (Student s in ht.Values)
            {
                if (s.Num.Equals(num2))
                {
                    ht.Remove(s.Num);
                    Console.WriteLine("删除成功");
                    Student(ht);
                }
                else
                {
                    Console.WriteLine("查无此人!");
                    Remove(ht);


                }
            }
        }

        //4、查询学生信息功能中有：1、查询所有（按学号排序）2、按学号查询（查没有，则打印查无此学生）
        private static void Select(Hashtable ht)
        {
            while (true)
            {
                Console.WriteLine("请选择：1、查询所有（按学号排序）2、按学号查询（查没有，则打印查无此学生）");
                int num = int.Parse(Console.ReadLine());
                if (num == 1)
                {

                    foreach (Student s in ht.Values)
                    {
                        Console.WriteLine(s);
                    }
                    Student(ht);
                }
                if (num == 2)
                {
                    Console.WriteLine("请输入要查询学生的学号");
                    string num1 = Console.ReadLine();
                    foreach (Student s in ht.Values)
                    {
                        if (s.Num.Equals(num1))
                        {
                            Console.WriteLine(s);
                        }
                        else
                        {
                            Console.WriteLine("查无此人！");
                            Student(ht);
                        }

                    }
                }
            }
        }
    }
                }
            
   

