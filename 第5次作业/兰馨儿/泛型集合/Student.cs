﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//1、学生类：学号、姓名、年龄

namespace ConsoleApp1
{
    class Student: IComparable<Student>
    {
        public int Num { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }

        public Student()
        {

        }
        public Student(int num, string name, int age)
        {
            Num = num;
            Name = name;
            Age = age;
        }
        public override string ToString()
        {
            return $"学号：{Num},姓名:{Name},年龄:{Age}";
        }
        public int CompareTo(Student other)
        {
            return this.Num.CompareTo(other.Num);
        }
    }
}
