﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;


namespace ConsoleApp19
{
    class Program
    {
        static void Main(string[] args)
        {
            Hashtable hashtable = new Hashtable();
            Student s1 = new Student(1, 18, "吴一");
            Student s2 = new Student(2, 19, "吴二");
            Student s3 = new Student(3, 20, "吴三");

            hashtable.Add(s1.Stuid, s1);
            hashtable.Add(s2.Stuid, s2);
            hashtable.Add(s3.Stuid, s3);
            A(hashtable);



        }
        static void A(Hashtable hashtable)
        {
            Console.WriteLine("请选择：1、添加学生信息。2、查询学生信息，3、删除学生信息  4、退出");
            string num = Console.ReadLine();

            switch (num)
            {
                case "1":
                    Add(hashtable);
                    break;
                case "2":
                    Check(hashtable);
                    break;
                case "3":
                    Remove(hashtable);
                    break;
                case "4":
                    if (true)
                    {
                        Console.WriteLine("成功退出");
                    }
                    break;
                default:
                    break;
            }
        }
        static void Add(Hashtable hashtable)
        {
            Console.WriteLine("请输入学生学号");
            int stuid = int.Parse(Console.ReadLine());
            Console.WriteLine("请输入学生年龄");
            int age = int.Parse(Console.ReadLine());
            Console.WriteLine("请输入学生姓名");
            string name = Console.ReadLine();

            Student s4 = new Student(stuid, age, name);
            hashtable.Add(s4.Stuid, s4);
            Console.WriteLine("添加成功");
            A(hashtable);
        }
        static void Check(Hashtable hashtable)
        {

            Console.WriteLine("请输入查询方式：1、查询所有 2、按学号查询");
            int a = int.Parse(Console.ReadLine());
            if (a == 1)
            {

                foreach (var item in hashtable.Values)
                {
                    Console.WriteLine(item);

                }
                A(hashtable);
            }
            if (a == 2)
            {
                Console.WriteLine("请输入要查询学生的学号");
                string num2 = Console.ReadLine();
                foreach (string item in hashtable.Values)
                {
                    if (item.Contains(num2))
                    {
                        Console.WriteLine(item);
                        Console.WriteLine();

                        A(hashtable);
                    }

                }
            }

        }
        static void Remove(Hashtable hash)
        {
            Console.WriteLine("请输入要删除的学生的（姓名或学号）");
            string num3 = Console.ReadLine();
            foreach (Student item in hash.Values)
            {
                if (item.Stuid.Equals(num3))
                {
                    hash.Remove(item.Stuid);
                    Console.WriteLine("删除成功");
                    A(hash);
                }
                else
                {
                    Console.WriteLine("查无此学生");
                    Remove(hash);
                }
            }
         }
}
}
