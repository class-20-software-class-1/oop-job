﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            ASD();
        }

        static void ASD() 
        {
            Console.WriteLine("请输入邮箱：");
            string a = Console.ReadLine();

            if (Regex.IsMatch(a, @"                                                                                                        \w{9} $ " + "@" + @" \w{2}  .(com|org|net|edu|mil|tv|biz|info)"))
            {
                Console.WriteLine("正确");
            }
            else
            {
                Console.WriteLine("错误");
            }
        }

        static void QWE() 
        {
            Console.WriteLine("请输入手机号："); 
            string a = Console.ReadLine();

            if (Regex.IsMatch(a, @"^(+86)?((13)^\d$|(14)(5|7))|(15)(1|2|3|5|6|7|8|9)|(17)(6|7|8)|(18)^\d$\d{8}$"))
            {
                Console.WriteLine("正确");
            }
            else
            {
                Console.WriteLine("错误");
            }
        }
    }
    
}
