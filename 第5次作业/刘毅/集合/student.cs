﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class student:IComparable<Student>
    {
        string id;
        string name;
        string age;
        public string Id
        {
            get { return this.id; }
            set { this.id = value; }
        }
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }
        public string Age
        {
            get { return this.age; }
            set { this.age = value; }
        }
        public student(string id,string name, string age)
        {
            this.id = id;
            this.name = name;
            this.age = age;
        }

        public student()
        {
        }

        public int CompareTo(Student other)
        {
            throw new NotImplementedException();
        }
    }
}
