using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Test1();
            Test2();
            Test3();
        }
//        1、用户输入邮箱，请验证其合法性。
//	1、邮箱一定需要 @符号
//	2、根据 @符号分为两部分，“前半部分 @ 后半部分”，
//		前半部分可以数字、字母、下划线、中划线、 .（符号点）。但是.（符号点）不能 用在开头也不能用在结尾；

//        后半部分可以数字、字母、下划线、中划线、.（符号点），且符号点是必须的，至少出现一次，
        //但不能连续出现，且符号点不能在开头，也不能在结尾。
//		后半部分的符号点后面只能是：com、org、net、edu、mil、tv、biz、info
        static void Test1()
        {
            Console.WriteLine("请输入邮箱：");
            string str = Console.ReadLine();

            if (Regex.IsMatch(str, @"^(\w{10}[-_])@.(com|org|net|edu|mil|tv|biz|info)$"))
            {
                Console.WriteLine("正确");
            }
            else
            {
                Console.WriteLine("错误");
            }

        }



//2、用户输入手机号码，请验证其合法性。
//         手机号码规则：
//  	最开头+86可有可无
//  	13开头第三位是 0-9
//  	14开头第三位是 5或7
//  	15开头第三位是 0-9不包含4
//  	17开头第三位是 678中的一个
//  	18开头第三位是 0-9
//  	剩下的8位，都是0-9的数字。
        static void Test2()
        {
            Console.WriteLine("请输入电话号码");
            string str = Console.ReadLine();

            if (Regex.IsMatch(str,@"^(13\d[0-9]|14\d[5-7]|15\d[0-3,5-9]|17\d[6-8]|18[0-9])\d{8}[0-9]$"))
            {
                Console.WriteLine("正确");
            }
            else
            {
                Console.WriteLine("错误");
            }
        }

        
        static void Test3()
        {
	            Hashtable ht = new Hashtable();
            Students s1 = new Students("张三", 1, 18);
            Students s2 = new Students("李四", 2, 19);
            Students s3 = new Students("王五", 3, 20);

            ArrayList arrayList = new ArrayList();
            Console.WriteLine("请选择：1、添加学生信息。2、查询学生信息，3、删除学生信息  4、退出");
            int a = int.Parse(Console.ReadLine());
            switch (a)
            {   case 1:
                    
                    Console.WriteLine("请输入名字");
                    string name = Console.ReadLine();
                    Console.WriteLine("请输入学号");
                    int num = int.Parse(Console.ReadLine());
                    Console.WriteLine("请输入年龄");
                    int age = int.Parse(Console.ReadLine());
                    Students stu= new Students(name,num, age);



                    break;
                case 2:
                    Console.WriteLine("1、查询所有（按学号排序）2、按学号查询（查没有，则打印查无此学生）");
                    int b = int.Parse(Console.ReadLine());
                    switch (b)
                    {
                        case 1:
                            arrayList.Sort();

                            break;
                        case 2:
                            Console.WriteLine("请输入学号");
                            Students stu1 = new Students();
                            int stuno = int.Parse(Console.ReadLine());
                            stu1.Num = stuno;
                            bool flay = arrayList.Contains(stu1.Stuno);
                            Console.WriteLine(flay);
                            if (flay ==false)
                            {
                                Console.WriteLine("查无此学生");

                            }
                            else
                            {
                                Console.WriteLine("存在");

                            }
                            break;
                        default:
                            Console.WriteLine("选错了");
                            break;
                    }
                    break;
                case 3:

                    ht.Remove(s1.Name);
                    Console.WriteLine("======删除后======");
                    foreach (var s in ht.Values)
                    {
                        Console.WriteLine(s);
                    }
                    break;
                case 4:
                    Environment.Exit(0);
                    break;
                default:
                    break;
            }
        }
        }


    }
}
