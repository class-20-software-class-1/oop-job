﻿using System;
using System.Collections;
using System.Text.RegularExpressions;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Hashtable hash = new Hashtable();
            Student s1 = new Student("s1", "张三", 17);
            Student s2 = new Student("s3", "李四", 16);
            Student s3 = new Student("s2", "王五", 15);
            hash.Add(s1.Num, s1);
            hash.Add(s2.Num, s2);
            hash.Add(s3.Num, s3);

            Test3(hash);

        }
        static void Test1()
        {
            Regex regex = new Regex(@"^(\w)+(\.| \| | \w)*@(\w)+(\.)(com|org|net|edu|mil|tv|biz|info)$");

            Console.WriteLine("请输入邮箱地址：");
           	 string email = Console.ReadLine();

            if (regex.IsMatch(email))
            {
                Console.WriteLine("邮箱格式正确。");
            }
            else
            {
                Console.WriteLine("邮箱格式不正确。");
            }
        }
        static void Test2()
        {
            Regex regex1 = new Regex(@"^(((\+)(86))*(13)[0-9][0-9]{8})
	|(((\+)(86))*(14)(5 | 7)[0-9]{8})|
                (((\+)(86))*(15)[0-9^4[0-9]{8}])|
	(((\+)(86))*(17)[6-7][0-9]{8})|
	(((\+)(86))*(18)[0-9][0-9]{8})$");
            	Console.WriteLine("请输入手机号：");
            string phone = Console.ReadLine();
            if (regex1.IsMatch(phone))
            {
                Console.WriteLine("手机格式正确。");
            }
            else
            {
                Console.WriteLine("手机格式不正确。");
            }


        }
        static void Test3(Hashtable hash)
        {
            Console.WriteLine("请选择：1、添加学生信息2、查询学生信息3、删除学生信息  4、退出");
            int a = Convert.ToInt32(Console.ReadLine());
            switch (a)
            {
                case 1:
                    No1(hash);
                    break;
                case 2:
                    No2(hash);
                    break;
                case 3:
                    No3(hash);
                    break;
                case 4:
                    if (true)
                    {
                        Console.WriteLine("已退出");
                    }
                    break;
                default:
                    break;
            }
        }
        static void No1(Hashtable hash)
        {
            Console.WriteLine(" 请输入添加的学生学号；");
            string num1 = Console.ReadLine();
            Console.WriteLine(" 请输入添加的学生姓名；");
            string name1 = Console.ReadLine();
            Console.WriteLine(" 请输入添加的学生年龄；");
            int age1 = Convert.ToInt32(Console.ReadLine());
            Student s4 = new Student(num1, name1, age1);
            hash.Add(s4.Num, s4);
            Console.WriteLine("添加完毕");
            Test3(hash);
        }
        static void No2(Hashtable hash)
        {
            Console.WriteLine("请输入查询方式：1、查询所有 2、按学号查询");
            int a = Convert.ToInt32(Console.ReadLine());
            if (a == 1)
            {
                foreach (var item in hash.Values)
                {
                    Console.WriteLine(item);
                }
                Test3(hash);
            }
            if (a == 2)
            {
                Console.WriteLine("请输入要查询学生的学号");
                string num2 = Console.ReadLine();
                foreach (Student item in hash.Values)
                {
                    if (item.Num.Contains(num2))
                    {
                        Console.WriteLine(item);
                        Console.WriteLine();

                        Test3(hash);
                    }

                }
            }

        }
        static void No3(Hashtable hash)
        {
            Console.WriteLine("请输入要删除的学生相关数据");
            string num3 = Console.ReadLine();
            foreach (Student item in hash.Values)
            {
                if (item.Num.Equals(num3))
                {
                    hash.Remove(item.Num);
                    Console.WriteLine("删除成功");
                    Test3(hash);
                }
                else
                {
                    Console.WriteLine("查无此学生");
                    No3(hash);
                }
            }
        }
        
    }
}
