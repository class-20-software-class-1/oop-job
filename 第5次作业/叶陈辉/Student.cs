﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Student
    {
        protected string num;
        protected string name;
        protected int age;
        public string Num
        {
            get { return this.num; }
            set { this.num = value; }
        }
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }
        public int Age
        {
            get { return this.age; }
            set { this.age = value; }
        }

        public Student(string num, string name, int age)
        {
            this.num = num;
            this.name = name;
            this.age = age;
        }
        public int CompareTo(Student other)
        {
            return this.Num.CompareTo(other.Num);
        }
        public override string ToString()
        {
            return $"学号：{num} 姓名：{name} 年龄：{age}";
        }
    }
}
