﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp8
{
    class Student : IComparable
    {
        public int Num { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }

        public Student(int num, string name, int age)
        {
            Num = num;
            Name = name;
            Age = age;
        }

        public Student()
        {
        }

        public override string ToString()
        {
            return $"学号：{Num}，姓名：{Name}，年龄：{Age}";
        }

        public int CompareTo(object compare)
        {
            Student student = (Student)compare; 

            return this.Num.CompareTo(student.Num);
            return this.Age.CompareTo(student.Age);           
            return this.Name.CompareTo(student.Name);
        }

    }
}