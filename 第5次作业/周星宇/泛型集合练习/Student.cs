﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Student
    {
        public int Num { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }

        public Student(int num, string name, int age)
        {
            Num = num;

            Name = name;
            
            Age = age;
        }
    }
}
