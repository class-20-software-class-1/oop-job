﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true) 
            {
                Console.WriteLine("请输入邮箱:");
                string str = Console.ReadLine();
                if (Regex.IsMatch(str, @"^[a-zA-Z0-9-_]+(\.[a-zA-Z0-9-_]+)*@[a-zA-Z0-9-_]+(\.(com|org|net|edu|mil|tv|biz|info))$"))
                {
                    Console.WriteLine("正确");
                }
                else 
                {
                    Console.WriteLine("错误");
                }
            }   

            while (true) 
            {
                 Console.WriteLine("请输入手机号：");
                    string str = Console.ReadLine();
                    if (Regex.IsMatch(str, @"^(((\+)(86))*(13)[0-9][0-9]{8})|
                                             (((\+)(86))*(14)[5|7][0-9]{8})|
                                             (((\+)(86))*(15)[0-94][0-9]{8})|
                                             (((\+)(86))*(17)[678][0-9]{8})            
                                                                          $"))
                    {
                        Console.WriteLine("拨号成功");
                    }
                    else {
                        Console.WriteLine("拨号失败，格式错误");
                    }

            }
            
        }
    }
}
