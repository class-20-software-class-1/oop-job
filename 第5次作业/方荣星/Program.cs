﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            //ArrayList list = new ArrayList();
            List<Student> list = new List<Student>();

            Student s1 = new Student("张三", 1, 18);
            Student s2 = new Student("李四", 2, 20);
            Student s3 = new Student("王五", 3, 19);

            list.Add(s2);
            list.Add(s1);
            list.Add(s3);

            Console.WriteLine("排序前：");
            foreach (Student s in list)
            {
                Console.WriteLine(s);
            }

            list.Sort();//排序
            Console.WriteLine("\r\n排序后：");
            foreach (Student s in list)
            {
                Console.WriteLine(s);
            }





            Console.ReadKey();

        }
        static void SortTest()
        {
            ArrayList list = new ArrayList();
            //List<double> list = new List<double>();
            //list.Add(99.5);
            //list.Add(59.0);
            //list.Add(80.5);



            Console.WriteLine("排序前：");
            foreach (double d in list)
            {
                Console.WriteLine(d);
            }

            list.Sort();
            Console.WriteLine("\r\n排序后：");
            foreach (double d in list)
            {
                Console.WriteLine(d);
            }
            list.Reverse();
            Console.WriteLine("\r\n倒序后：");
            foreach (double d in list)
            {
                Console.WriteLine(d);
            }
        }
        static void FXTest()
        {
            //ArrayList Hashtable 有类型不安全的问题
            //ArrayList list = new ArrayList();
            List<double> list = new List<double>();
            list.Add(99.5);
            list.Add(59.0);
            list.Add(80.5);
            //list.Add("ABC");

            //double sum = 0;
            //for (int i = 0; i < list.Count; i++)
            //{
            //    sum += list[i];
            //}
            foreach (double d in list)
            {
                Console.WriteLine(d);
            }

            //Console.WriteLine(sum);


            //Student s1 = new Student("张三", 1, 18);
            //Student s2 = new Student("李四", 2, 20);
            //Student s3 = new Student("王五", 3, 19);
            //Student s4 = new Student("赵六", 4, 21);

            //Dictionary<string, Student> dic = new Dictionary<string, Student>();
            //dic.Add(s1.Name,s1);
        }
        static void HashtableTest()
        {
            Hashtable ht = new Hashtable();//以键值对的形式存储数据。无序，且key不能重复的。

            Student s1 = new Student("张三", 1, 18);
            Student s2 = new Student("李四", 2, 20);
            Student s3 = new Student("王五", 3, 19);
            Student s4 = new Student("赵六", 4, 21);
            Student s5 = new Student("马七", 5, 25);
            Student s6 = new Student("朱八", 6, 17);

            //Hashtable ht1 = new Hashtable() { {s1.Name,s1 },{s2.Name,s2 } };//集合初始化器

            //添加
            ht.Add(s1.Name, s1);
            ht.Add(s2.Name, s2);
            ht.Add(s3.Name, s3);
            ht.Add(s4.Name, s4);
            ht.Add(s5.Name, s5);
            ht.Add(s6.Name, s6);
            //Hashtable 里相当于存了两个数组，一个存key，一个value

            //遍历
            foreach (var s in ht.Keys)
            {
                Console.WriteLine(s);
            }
            Console.WriteLine("=======ht[key]======");
            foreach (var s in ht.Keys)
            {
                Console.WriteLine(ht[s]);
            }
            Console.WriteLine("======Values=======");
            foreach (var s in ht.Values)
            {
                Console.WriteLine(s);
            }

            //删除
            ht.Remove(s1.Name);//是传key
            Console.WriteLine("======删除后======");
            foreach (var s in ht.Values)
            {
                Console.WriteLine(s);
            }

            //修改
            ht[s2.Name] = new Student("Vincent", 9, 26);
            Console.WriteLine("======修改后======");
            foreach (var s in ht.Values)
            {
                Console.WriteLine(s);
            }

            //查询
            Console.WriteLine(ht.Contains(s3.Name));
            Console.WriteLine(ht.ContainsKey(s3.Name));
            Console.WriteLine(ht.ContainsValue(s3));

            //ht.Add("王五",s1);//会报错，key:"王五"已经存在了。

            //清空
            ht.Clear();
            Console.WriteLine(ht.Count);
        }
        static void ArrayListTest()
        {
            //集合，是一种容器，可以动态的改变大小，可以存储各种类型的数据。
            int[] arr = new int[10];
            arr[0] = 10;
            //arr[1] = "abc";//数组只能存储一种固定的数据类型。

            //集合：ArrayList、Hashtable
            //泛型集合：List<>、Dictionary<>

            //集合排序：实现接口：IComparable、IComparer、IComparable<>、IComparer<>

            ArrayList alist = new ArrayList();//可重复，有序
            Student s1 = new Student("张三", 1, 18);
            Student s2 = new Student("李四", 2, 20);
            Student s3 = new Student("王五", 3, 19);
            Student s4 = new Student("赵六", 4, 21);
            Student s5 = new Student("马七", 5, 25);
            Student s6 = new Student("朱八", 6, 17);

            //ArrayList alist1 = new ArrayList() {s1,s2,s3 };//集合初始化器
            //Student s7 = new Student() {Name="啊啊啊",Num=4 };//类初始化器

            //添加
            alist.Add(s1);
            alist.Add(s2);
            alist.Add(s3);
            alist.Add(s4);
            alist.Add(s5);
            alist.Add(s6);

            //插入
            alist.Insert(1, s6);

            //遍历
            for (int i = 0; i < alist.Count; i++)
            {
                Console.WriteLine(alist[i]);//取出来还是object.ToString()
            }
            Console.WriteLine("============");
            foreach (var s in alist)
            {
                Console.WriteLine(s);//object.ToString()
            }

            //删除
            alist.Remove(s1);
            Console.WriteLine("======删除后======");
            foreach (var s in alist)
            {
                Console.WriteLine(s);//object.ToString()
            }
            //删除
            alist.RemoveAt(1);
            Console.WriteLine("======删除后======");
            foreach (var s in alist)
            {
                Console.WriteLine(s);//object.ToString()
            }

            //修改
            alist[0] = new Student("Vincet", 8, 30);
            Console.WriteLine("======修改后======");
            foreach (var s in alist)
            {
                Console.WriteLine(s);//object.ToString()
            }

            //查询
            Console.WriteLine(alist.Contains(s3));
            Console.WriteLine(alist.Contains(s1));

            //清空
            alist.Clear();
            Console.WriteLine("======清空后======");
            Console.WriteLine("长度是：" + alist.Count);
            foreach (var s in alist)
            {
                Console.WriteLine(s);//object.ToString()
            }
        }
        static void Test1(object obj)
        {
            obj.ToString();
        }
        static void DateTest()
        {
            //Regex.IsMatch();//用来【判断】给定的字符串是否匹配某个正则表达式
            //Regex.Match();  //用来从给定的字符串中按照正则表达式的要求，【提取】【一个】匹配的字符串
            //Regex.Matches();//用来从给定的字符串中按照郑则表示式的要求，【提取】【所有】匹配的字符串
            //Regex.Replace();//【替换】所有正则表达式匹配的字符串为另外一个字符串
            //Regex.Split();//匹配拆分字符串成字符串数组

            //检验一个字符串是否是合法的日期格式，如1984 - 05 - 22就是合法的，2100 - 13 - 7就是不合法的。
            //具体要求如下：
            //+日期按顺序由年、月、日三部分组成，每部分之间用 “ - ” 隔开。
            //+年份必须是4位数字，且必须是19或20开头。
            //+月份必须是两位数字，1 - 9月必须写成01 - 09,。
            //+日期必须是两位数字，1 - 9日必须写成01 - 09。


            while (true)
            {
                Console.WriteLine("请输入日期：");
                string input = Console.ReadLine();

                if (Regex.IsMatch(input, @"^(19|20)\d{2}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01])$"))
                {
                    Console.WriteLine("正确！");
                }
                else
                {
                    Console.WriteLine("错误！");
                }
            }
        }
        static void SixNumTest()
        {
            //模糊匹配
            while (true)
            {
                Console.WriteLine("请输入邮政编码：");
                string input = Console.ReadLine();

                //if (Regex.IsMatch(input,@"^\d\d\d\d\d\d$"))//@表示后面的字符串避免转义
                //if (Regex.IsMatch(input, @"^\d{6}$"))
                if (Regex.IsMatch(input, @"^[0-9]{6}$"))
                {
                    Console.WriteLine("正确！");
                }
                else
                {
                    Console.WriteLine("错误！");
                }
            }
        }

        static void RegexTest()
        {
            Regex regex = new Regex("abc");
            string input = "koieusafabclasdfjalsfdjabcalsdfjaldabcljlj";
            Console.WriteLine(regex.IsMatch(input));
            Match m = regex.Match(input);
            Console.WriteLine(m.Index + "  " + m.Value);
            MatchCollection mc = regex.Matches(input);

            foreach (Match m1 in mc)
            {
                Console.WriteLine(m1.Index + "  " + m1.Value);
            }
            Console.WriteLine(regex.Replace(input, "阿百川"));
            string[] arr = regex.Split(input);

            Console.WriteLine("字符串数组元素：");
            Console.WriteLine(string.Join("   ", arr));

            //以上的操作，也可用静态方法来完成。区别在于，使用静态方法的话，每次都要写匹配模式。
            Regex.IsMatch(input, "abc");
            Regex.Match(input, "abc");
            Regex.Matches(input, "abc");
            Regex.Replace(input, "abc", "阿百川");
            Regex.Split(input, "abc");
        }
        static void Test()
        {
            for (int i = 0; i < 1000; i++)
            {
                int[] arr = GenerateRandomArray();
                Console.Write(string.Join("  ", arr));
                Console.WriteLine(IsRight(arr) ? "正确" : "错误！");
            }

        }

        /// <summary>
        /// 生成随机数组，元素不重复
        /// </summary>
        /// <returns></returns>
        static int[] GenerateRandomArray()
        {
            Random r = new Random();
            int[] arr = new int[10];
            int min = 1;
            int max = 10;
            for (int i = 0; i < arr.Length; i++)
            {
                int num = r.Next(min, max + 1);
                while (IsRepeat(arr, num))
                {
                    num = r.Next(min, max + 1);
                }
                arr[i] = num;
            }

            return arr;
        }

        /// <summary>
        /// 判断数组中有没有num
        /// </summary>
        /// <param name="arr"></param>
        /// <param name="num"></param>
        /// <returns></returns>
        private static bool IsRepeat(int[] arr, int num)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] == num)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// 验证数组元素重复不重复
        /// </summary>
        /// <param name="arr"></param>
        /// <returns></returns>
        static bool IsRight(int[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                for (int j = 0; j < arr.Length; j++)
                {
                    if (i != j)
                    {
                        if (arr[i] == arr[j])
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }
    }
}
