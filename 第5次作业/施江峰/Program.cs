﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            telephone();
        }

        static void telephone()
        {
            while (true)
            {
                Console.WriteLine("请输入电话号码");
                string input = Console.ReadLine();

                if (Regex.IsMatch(input, @"^[+86]|\d13|[0-9]\d14|[5-7]\d{0,9}$"))
                {
                    Console.WriteLine("正确！");
                }
                else
                {
                    Console.WriteLine("错误");
                }
            }
        
        }
        static void email() 
        {
            while (true)
            {
                Console.WriteLine("请输入邮政编码");
                string input = Console.ReadLine();

                if (Regex.IsMatch(input, @"^\w{0,20}|[-]@\w{0,20}.(com|org|net|edu|mil|tv|biz|info)$"))
                {
                    Console.WriteLine("正确！");
                }
                else
                {
                    Console.WriteLine("错误");
                }
            }
        }
    }
}
