﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ConsoleApp5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入要验证的东西：1.邮箱格式 2.电话号码格式");
            string str = Console.ReadLine();
            switch (str) {
                case "1":
            email();
                    break;
                case "2":
                    number();
                    break;
        }
        }
        public static void email()
        {
            while (true)
            {
                Console.WriteLine("请输入你的邮箱地址");
                string str = Console.ReadLine();
                if (Regex.IsMatch(str, @"^([\w-])+(@)+([\w-])+(.)+(com|org|net|edu|mil|tv|biz|info)$"))
                {
                    Console.WriteLine("正确");
                }
                else
                {
                    Console.WriteLine("错误");
                }
            }
        }
        public static void number()
        {
            while (true)
            {
                Console.WriteLine("请输入你的电话号码");
                string str = Console.ReadLine();
                if (Regex.IsMatch(str, @"^((\+)(86)(13)[0-9][0-9]{8})|((13)[0-9][0-9]{8})|((\+)(86)(14)[5|7][0-9]{7})|((14)[5|7][0-9]{7})|((\+)(86)(15)[0-9^4][0-9]{7})|((15)[0-9^4][0-9]{7})|((\+)(86)(17)[6|7|8][0-9]{7})|((17)[6|7|8][0-9]{7})|((\+)(86)(18)[0-9][0-9]{8}|((18)[0-9][0-9]{8}))"))
                {
                    Console.WriteLine("电话号码正确");
                }
                else
                {
                    Console.WriteLine("电话号码错误");
                }
            }
        }
    }
}
