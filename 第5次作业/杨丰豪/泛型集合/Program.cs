﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp8
{
    class Program
    {
        static void Main(string[] args)
        {
            Hashtable hash = new Hashtable();
            hash.Add(1, new Student(1, "小东", 18));
            hash.Add(3, new Student(3, "小韦", 17));
            hash.Add(2, new Student(2, "小鑫", 16));
            ArrayList arrayList = new ArrayList(hash.Keys);
            Console.WriteLine("请输入你要选择的功能 1、添加学生信息。2、查询学生信息，3、删除学生信息  4、退出");
            int str = int.Parse(Console.ReadLine());
            switch (str)
            {
                case 1:
                    Add(hash,arrayList);
                    break;
                case 2:
                    ChaXun(hash,arrayList);
                    break;
                case 3:
                    Delect(hash,arrayList);
                    break;
                case 4:
                    Console.WriteLine("退出成功！");
                    break;
                default:
                    Console.WriteLine("选择错误！");
                    break;
            }
        }
        public static void Add(Hashtable hash,ArrayList arrayList)
        {
            Console.WriteLine("请输入学号");
            int num = int.Parse(Console.ReadLine());
            Console.WriteLine("请输入姓名");
            string name = Console.ReadLine();
            Console.WriteLine("请输入年龄");
            int age = int.Parse(Console.ReadLine());
            if (hash.Contains(num))
            {
                Console.WriteLine("该学生已存在");
            }
            else
            {
                hash.Add(num, new Student(num, name, age));
                Console.WriteLine("添加成功！");
            }
        }
        public static void ChaXun(Hashtable hash,ArrayList arrayList)
        {
            Console.WriteLine("查询学生信息功能中有：1、查询所有（按学号排序）2、按学号查询（查没有，则打印查无此学生）");
            int str = int.Parse(Console.ReadLine());
            switch (str)
            {
                case 1:
                    arrayList.Sort();
                    for (int i = 0; i < arrayList.Count; i++)
                    {
                        object e = arrayList[i];
                        Student student = (Student)hash[e];
                        Console.WriteLine(student);
                    }
                    break;
                case 2:
                    Console.WriteLine("请输入学号进行查询学生");
                    int str1 = int.Parse(Console.ReadLine());
                    object teIFind = hash[str1];
                    if (teIFind == null)
                    {
                        Console.WriteLine("无此学生");
                    }
                    else
                    {
                        Console.WriteLine("已找到该学生");
                        Console.WriteLine(teIFind.ToString());
                    }
                    break;
            }
        }
       public static void Delect(Hashtable hash, ArrayList arrayList)
        {
            Console.WriteLine("请输入要删除的学生信息");
            int num = int.Parse(Console.ReadLine());
            hash.Remove(num);
            Console.WriteLine("删除成功！");
        }
    }
 }
