﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp8
{
    class Student
    {
        private int num;
        private string name;
        private int age;

        public int Num { get => num; set => num = value; }
        public string Name { get => name; set => name = value; }
        public int Age { get => age; set => age = value; }

        public Student(int num, string name, int age)
        {
            this.Num = num;
            this.Name = name;
            this.Age = age;
        }
        public override string ToString()
        {
            return $"学号：{Num}，姓名：{Name}，年龄：{Age}";
        }

        public int CompareTo(Student other)
        {
            return this.Num.CompareTo(other.Num);
        }
    }
}
