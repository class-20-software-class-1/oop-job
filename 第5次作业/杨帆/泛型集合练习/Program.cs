﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Student> list = new List<Student>();
            Hashtable ht = new Hashtable();

            Student s1 = new Student("张三",1,18);
            Student s4 = new Student("赵六",4,20);
            Student s3 = new Student("王五",3,19);
            Student s2 = new Student("李四",2,17);

            

            list.Add(s1);
            list.Add(s2);
            list.Add(s3);
            list.Add(s4);

            Console.WriteLine("请选择：1、添加学生信息。2、查询学生信息，3、删除学生信息  4、退出");
            int asd = int.Parse(Console.ReadLine());

            switch (asd)
            {
                case 1:
                    
                    Mnb(list, ht);
                    break;
                case 2:
                    Console.WriteLine("查询学生信息：1、查询所有（按学号排序）2、按学号查询");
                    int xcv = int.Parse(Console.ReadLine());
                    if (xcv == 1)
                    {
                        list.Sort(new NumSort());
                        foreach (Student a in list)
                        {
                            Console.WriteLine(a);
                        }
                    }
                    else if (xcv == 2)
                    {
                        Console.Write("请输入你的学号：");
                        int Id = int.Parse(Console.ReadLine());
                        if (ht.Contains(Id))
                        {
                            Console.WriteLine(ht[Id]);
                        }
                        else
                        {
                            Console.WriteLine("查无此学生!!!");
                        }
                    }
                    
                    break;
                case 3:
                    Console.WriteLine("请输入要删除信息的人的姓名");
                    string qwe = Console.ReadLine();
                    ht.Remove(qwe);
                    break;
                case 4:
                    
                    break;

            }

        }
        static void Mnb (List<Student> list, Hashtable ht) 
        {
            Console.WriteLine("请输入姓名：");
            string name = Console.ReadLine();

            Console.WriteLine("请输入学号：");
            int num = int.Parse(Console.ReadLine());

            Console.WriteLine("请输入年龄：");
            int age = int.Parse(Console.ReadLine());

            Student s5 = new Student(name, num, age);

            if (ht.Contains(s5.Num))
            {
                Console.WriteLine("已有此名字的人，请重新输入：");
                Mnb(list,ht);
            }
            else
            {
                list.Add(s5);
            } 
            
        }
    }
    class NumSort : IComparer<Student>
    {
        public int Compare(Student x, Student y)
        {
            return x.Num.CompareTo(y.Num);
        }
    }
}
