﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Student
    {
        private string name;
        private int num;
        private int age;

        public string Name { get => name; set => name = value; }
        public int Num { get => num; set => num = value; }
        public int Age { get => age; set => age = value; }

        public Student(string name , int num ,int age)
        {
            Name = name;
            Num = num;
            Age = age;
        }

        public override string ToString() 
        {
            return$"姓名：{Name},  学号：{Num}， 年龄：{Age}";
        }
    }

}
