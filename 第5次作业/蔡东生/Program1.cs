﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Test();

        }
        public static void test2() {             
            while (true)
            {
                Console.WriteLine("请输入手机号码");
                string s = Console.ReadLine();

                if (Regex.IsMatch(s, @"^(\+86)?(13\d|14[5,7]|15[0,1,2,3,5,6,7,8,9]|17[6,7,8]|18\d)(\d{8})$"))
                {
                    Console.WriteLine("正确");
                }
                else
                {
                    Console.WriteLine("错误");
                }

            }

        }
        public static void Test() {
            while (true)
            {
                Console.WriteLine("请输入邮箱号码");
                string s = Console.ReadLine();

                if (Regex.IsMatch(s, @"^(\w)+@(\w+)\.(com|org|net|edu|mil|tv|biz|info)$"))
                {
                    Console.WriteLine("正确");
                }
                else
                {
                    Console.WriteLine("错误");
                }

            }
        }
    }
}
