﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Demo6
{
    class Program
    {
        static void Main(string[] args)
        {
            one();
            two();
            three();

        }
        static void one()
        {
            //            2、请选择：1、添加学生信息。2、查询学生信息，3、删除学生信息  4、退出
            //            3、重复的学号不能添加。
            //            4、查询学生信息功能中有：1、查询所有（按学号排序）2、按学号查询（查没有，则打印查无此学生）

            Student one = new Student(1,"卢本伟",18);
            Student two = new Student(2,"刘谋",18);

            ArrayList  note = new ArrayList()
            {
                one,
                two
            };
            Console.WriteLine("请选择：1、添加学生信息。2、查询学生信息，3、删除学生信息  4、退出");
            int num = int.Parse(Console.ReadLine());
            switch (num)
            {
                case 1:
                    Student ass = new Student();
                    Console.WriteLine("请输入学生姓名");
                    ass.Name = Console.ReadLine();
                    Console.WriteLine("请输入学生年龄");
                    ass.Age = int.Parse(Console.ReadLine());
                    Console.WriteLine("请输入学号");
                    ass.Number = int.Parse(Console.ReadLine());
                    note.Add(ass);
                    break;

                case 2:
                    Console.WriteLine("1、查询所有（按学号排序）2、按学号查询（查没有，则打印查无此学生）");
                    int num1 = int.Parse(Console.ReadLine());
                    switch (num1)
                    {
                        case 1:
                            note.Sort();
                            foreach (Student item in note)
                            {
                                Console.WriteLine(item);
                            }

                            break;

                        case 2:
                            Console.WriteLine("请输入学号");
                            Student studentId = new Student();
                            int num3 = int.Parse(Console.ReadLine());
                            studentId.Number = num3;
                            bool query = note.Contains(studentId.Number);
                            Console.WriteLine(query);

                            if (query = false)
                            {
                                Console.WriteLine("输入有误");

                            }
                            else
                            {
                                Console.WriteLine("存在");

                            }
                            break;
                        default:
                            Console.WriteLine("选错了");
                            break;
                    }
                    break;

                case 3:
                    Console.WriteLine("已删除学生信息");
                    note.Clear();
                    break;

                case 4:
                    Console.WriteLine("已退出。");
                    Environment.Exit(0);
                    break;

                default:
                    Console.WriteLine("输入错误");
                    break;
            }
        }
        static void two()
        {

            //            1、用户输入邮箱，请验证其合法性。

            //(1)、邮箱一定需要 @符号


            //(2)、根据 @符号分为两部分，“前半部分 @ 后半部分”，

            //前半部分可以数字、字母、下划线、
            //中划线、 .（符号点）。但是.（符号点）不能 用在开头也不能用在结尾；


            //后半部分可以数字、字母、下划线、
            //中划线、.（符号点），且符号点是必须的，
            //至少出现一次，但不能连续出现，且符号点不能在开头，也不能在结尾。

            //后半部分的符号点后面只能是：
            //com、org、net、edu、mil、tv、biz、info
            while (true)
            {
                Console.WriteLine("请输入邮箱");
                string str = Console.ReadLine();
                if (Regex.IsMatch(str, @"^[\w-]+@[\w]+\.(com|net|org|edu|mil|tv|biz|info)$"))

                {
                    Console.WriteLine("正确");
                }
                else
                {
                    Console.WriteLine("错误");
                }
                break;
            }
        }
        static void three()
        {

            // 2、用户输入手机号码，请验证其合法性。
            //    手机号码规则：

            //   最开头 + 86可有可无

            //   13开头第三位是 0 - 9

            //   14开头第三位是 5或7

            //   15开头第三位是 0 - 9不包含4

            //   17开头第三位是 678中的一个

            //   18开头第三位是 0 - 9

            //   剩下的8位，都是0 - 9的数字。
            while (true)
            {
                Console.WriteLine("请输入手机号码");
                string str = Console.ReadLine();
                if (Regex.IsMatch(str, @"^(13\d{9}|14[57]\d{8}|15[0-35-9]\d{8}|17[678]\d{8}|18\d{9})$"))

                {
                    Console.WriteLine("正确");
                }
                else
                {
                    Console.WriteLine("错误");
                }

            }

        }

    }


    }
}
