﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo6
{
    class Student
    {
        //1、学生类：学号、姓名、年龄
        private int number;
        private string name;
        private int age;

        public int Number
        {
            get { return number; }
            set { number = value; }
        }
        public string Name
        {
            get { return name; }
            set { name = value; }

        }
        public int Age
        {
            get { return age; }
            set { age = value; }
        }
        public Student(int number,string name,int age )
        {
            this.number = number;
            this.name = name;
            this.age = age;
        }

        public Student()
        {
        }

        public int CompareTo(Student other)
        {
            return Number.CompareTo(other.Number);
        }
        public override string ToString()
        {
            return $"学号：{number} 姓名：{name} 年龄：{age}";
        }


    }
}
