﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Hashtable ht = new Hashtable();
            List<Student> list = new List<Student>();
            Student st = new Student();
            Student s1 = new Student(1,"a",20);
            Student s2 = new Student(2, "b", 21);
            Student s3 = new Student(3, "c", 22);
            Student s4 = new Student(4, "d", 23);
            Student s5 = new Student(5, "e", 24);
            ht.Add(s1.Name,s1);
            ht.Add(s2.Name, s2);
            ht.Add(s3.Name, s3);
            ht.Add(s4.Name, s4);
            ht.Add(s5.Name, s5);

            while (true)
            {
                operation();
                int key = int.Parse(Console.ReadLine());
                IComparable<Student> comparable;
                list.Sort();
                switch (key)
                {
                    case 1:
                        add(ht);
                        break;
                    case 2:
                        PriMsg(ht);
                        break;
                    case 3:
                        remove(ht,s1,s2,s3,s4,s5);
                        break;
                    case 4:
                        
                        break;
                    default:
                        break;
                }
            } 
        }

        private static void remove(Hashtable ht, Student s1, Student s2, Student s3, Student s4, Student s5)
        {
            Console.WriteLine("请输入要删除的姓名");
            string index = Console.ReadLine();
            if(index.Equals(s1.Name))
            ht.Remove(s1);
        }

        private static void PriMsg(Hashtable ht)
        {
            foreach (var st in ht.Values)
            {
                Console.WriteLine(st);
            }
        }

        private static void add(Hashtable ht)
        {
            Student st = new Student();
            Console.WriteLine("请输入添加学生学号");
            st.Num =int.Parse(Console.ReadLine());
            Console.WriteLine("请输入添加学生姓名");
            st.Name = Console.ReadLine();
            Console.WriteLine("请输入添加学生年龄");
            st.Age = int.Parse(Console.ReadLine());
            Student s6 = new Student(st.Num, st.Name, st.Age);
            ht.Add(st.Name, s6);
        }

        private static void operation()
        {
            Console.WriteLine("请选择：1.添加学生 2.查询学生 3.删除学生 4.退出 ");
        }
    }
}
