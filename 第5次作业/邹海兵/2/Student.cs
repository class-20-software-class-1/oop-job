﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace ConsoleApp1
{
    class Student:IComparable<Student>
    {
        public int Num { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }

        public Student(int num, string name, int age)
        {
            Num = num;
            Name = name;
            Age = age;
        }
        public Student()
        { }
        public override string ToString()
        {
            return $"学号{Num} 姓名{Name} 年龄{Age}";
        }

        public int CompareTo(Student other)
        {
            return Age.CompareTo(this.Age);
        }
    }
}
