﻿using System;
using System.Text.RegularExpressions;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            //text1();
            text2();
  
        }

        private static void text2()
        {
            //         2、用户输入手机号码，请验证其合法性。
            //      手机号码规则：
            //最开头 + 86可有可无
            //   13开头第三位是 0 - 9
            //   14开头第三位是 5或7
            //   15开头第三位是 0 - 9不包含4
            //   17开头第三位是 678中的一个
            //   18开头第三位是 0 - 9
            //   剩下的8位，都是0 - 9的数字。
            Console.WriteLine("请输入电话");
            string phone = Console.ReadLine();
            if (Regex.IsMatch(phone,@"(13[0-9]|14[57]|15[0-3,5-9]|17[6-8]|18\d[9])"))
            {
                Console.WriteLine("正确");
            }
            else
            {
                Console.WriteLine("错误");
            }
        }

        private static void text1()
        {
            //、用户输入邮箱，请验证其合法性。
            //1、邮箱一定需要 @符号

            //2、根据 @符号分为两部分，“前半部分 @ 后半部分”，
            //前半部分可以数字、字母、下划线、中划线、 .（符号点）。但是.（符号点）不能 
            //用在开头也不能用在结尾；
            //    后半部分可以数字、字母、下划线、中划线、.（符号点），
            //且符号点是必须的，至少出现一次，但不能连续出现，且符号点不能在开头，也不能在结尾。
            //后半部分的符号点后面只能是：com、org、net、edu、mil、tv、biz、info

            Console.WriteLine("请输入的邮箱");
            string str = Console.ReadLine();
            if (Regex.IsMatch(str, @"^(\w{0,}|[_-]|[.])@.{1,}(com|opr|net|edu|mil|tv|biz|info)$"))
            {
                Console.WriteLine("ture");
            }
            else
            {
                Console.WriteLine("false");
            }
        }
    }
}
