﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P7
{
    class Student:IComparable
    {
        private int id;
        private string name;
        private int age;
        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public int Age { get => age; set => age = value; }
        public Student() { }
        public Student(int id,string name,int age)
        {
            this.Id = Id;
            this.Name = name;
            this.Age = age;
        }
        public override string ToString()
        {
            return $"学号：{this.Id}姓名：{this.Name}年龄：{this.Age}";
        }
        public int CompareTo(Student ovo)
        {
            return this.Age.CompareTo(ovo.Age);
        }
            public int CompareTo(object obj)
        {
            Student st = (Student)obj;
            //return this.Age.CompareTo(st.Age);
            if ( this.Id < st.Id )
            {
                return 1;
            }
            else 
            {
                return -1;
            }
        }
    }
}
