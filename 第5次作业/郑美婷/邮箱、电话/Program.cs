﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace P8
{
    class Program
    {
        //1、用户输入邮箱，请验证其合法性。
        //1、邮箱一定需要 @符号
        //2、根据 @符号分为两部分，“前半部分 @ 后半部分”，
        //	 前半部分可以数字、字母、下划线、中划线、 .（符号点）。但是.（符号点）不能 用在开头也不能用在结尾；

        //后半部分可以数字、字母、下划线、中划线、.（符号点），且符号点是必须的，至少出现一次，但不能连续出现，且符号点不能在开头，也不能在结尾。
        //后半部分的符号点后面只能是：com、org、net、edu、mil、tv、biz、info

        //2、用户输入手机号码，请验证其合法性。
        //   手机号码规则：
        //最开头+86可有可无
        //13开头第三位是 0-9
        //14开头第三位是 5或7
        //15开头第三位是 0-9不包含4
        //17开头第三位是 678中的一个
        //18开头第三位是 0-9
        //剩下的8位，都是0-9的数字。
        static void Main(string[] args)
        {
            Regex re = new Regex("djsadfeiownj123");
            while (true)
            {
                Console.WriteLine("请选择：1.邮箱 2.手机号码");
                int cc = int.Parse(Console.ReadLine());
                switch (cc)
                {
                    case 1:
                        youxiang();
                        break;
                    case 2:
                        telephone();
                        break;
                    default:
                        break;
                }
            }
        }
        private static void telephone()
        {
            Console.WriteLine("请输入电话号码：");
            string b = Console.ReadLine();
            if (Regex.IsMatch(b, @"^(((\+)(86))|\s)*(  (13\d)|(14[5-7])|(15([0-3]|[5-9]))|(17[678])|(18\d)  )\d{8} $"))
            {
                Console.WriteLine("正确");
            }
            else
            {
                Console.WriteLine("错误");
            }
        }

        private static void youxiang()
        {
            Console.WriteLine("请输入邮箱：");
            string a = Console.ReadLine();
            if (Regex.IsMatch(a, @"^((\w)|(\.){10})*@(\w)+(\.)(com|org|net|edu|mil|tv|biz|info)$"))
            {
                Console.WriteLine("正确");
            }
            else
            {
                Console.WriteLine("错误");
            }
        }
    }
}
