﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ConsoleApp5
{
    class Program
    {
        static void Main(string[] args)
        {
            Test();
            Test1();

            Hashtable hashtable = new Hashtable();
            Student s1 = new Student(1,18,"吴一");
            Student s2 = new Student(2, 19, "吴二");
            Student s3 = new Student(3, 20, "吴三");

            hashtable.Add(s1.Stuid, s1);
            hashtable.Add(s2.Stuid, s2);
            hashtable.Add(s3.Stuid, s3);
            Test2(hashtable);

        }
        static void Test()
        {
            //            1、用户输入邮箱，请验证其合法性。
            //	1、邮箱一定需要 @符号
            //    2、根据 @符号分为两部分，“前半部分 @ 后半部分”，
            //		前半部分可以数字、字母、下划线、中划线、 .（符号点）。但是.（符号点）不能 用在开头也不能用在结尾；
            //        后半部分可以数字、字母、下划线、中划线、.（符号点），且符号点是必须的，至少出现一次，但不能连续出现，且符号点不能在开头，也不能在结尾。
            //		后半部分的符号点后面只能是：com、org、net、edu、mil、tv、biz、info
            Console.WriteLine("请输入你的邮箱");
            string input = Console.ReadLine();

            while (true)
            {
                if (Regex.IsMatch(input,@"^(\w)+(\.| \| | \w)*@(\w)+(\.)(com|org|net|edu|mil|tv|biz|info)$"))
                {
                    Console.WriteLine("输入正确");
                    break;
                } else
                {
                    Console.WriteLine("输入错误");
                }
            }
        }

        static void Test1()
        {
            //2、用户输入手机号码，请验证其合法性。
            //         手机号码规则：
            //  	最开头 + 86可有可无
            //      13开头第三位是 0 - 9
            //      14开头第三位是 5或7
            //      15开头第三位是 0 - 9不包含4
            //      17开头第三位是 678中的一个
            //      18开头第三位是 0 - 9
            //      剩下的8位，都是0 - 9的数字。
            Console.WriteLine("请输入你的手机号码");
            string input = Console.ReadLine();

            while (true)
            {
                if (Regex.IsMatch(input, "(13[0-9])|（14[57]）|(15[0-3][5-9])|(15[0-9])|(17[678])|(18[0-9])[0-9]"))
                {
                    Console.WriteLine("正确");
                    break;
                }
                else {
                    Console.WriteLine("错误");
                }
            }
        }
        static void Test2(Hashtable hashtable)
        {
            Console.WriteLine("请选择：1、添加学生信息。2、查询学生信息，3、删除学生信息  4、退出");
            string num = Console.ReadLine();

            switch (num)
            {
                case "1":
                    Add(hashtable);
                    break;
                case "2":
                    Check(hashtable);
                    break;
                case "3":
                    Remove(hashtable);
                    break;
                case "4":
                    if (true)
                    {
                        Console.WriteLine("成功退出");
                    }
                    break;
                default:
                    break;
            }
        }
        static void Add(Hashtable hashtable)
        {
            Console.WriteLine("请输入学生学号");
            int stuid = int.Parse(Console.ReadLine());
            Console.WriteLine("请输入学生年龄");
            int age = int.Parse(Console.ReadLine());
            Console.WriteLine("请输入学生姓名");
            string name = Console.ReadLine();

            Student s4 = new Student(stuid, age, name);
            hashtable.Add(s4.Stuid, s4);
            Console.WriteLine("添加成功");
            Test2(hashtable);
        }
        static void Check(Hashtable hashtable)
        {

            Console.WriteLine("请输入查询方式：1、查询所有 2、按学号查询");
            int a = int.Parse(Console.ReadLine());
            if (a == 1)
            {

                foreach (var item in hashtable.Values)
                {
                    Console.WriteLine(item);

                }
                Test2(hashtable);
            }
            if (a == 2)
            {
                Console.WriteLine("请输入要查询学生的学号");
                string num2 = Console.ReadLine();
                foreach (string item in hashtable.Values)
                {
                    if (item.Contains(num2))
                    {
                        Console.WriteLine(item);
                        Console.WriteLine();

                        Test2(hashtable);
                    }

                }
            }

        }
            static void Remove(Hashtable hash)
        {
            Console.WriteLine("请输入要删除的学生的（姓名或学号）");
            string num3 = Console.ReadLine();
            foreach (Student item in hash.Values)
            {
                if (item.Stuid.Equals(num3))
                {
                    hash.Remove(item.Stuid);
                    Console.WriteLine("删除成功");
                    Test2(hash);
                }
                else
                {
                    Console.WriteLine("查无此学生");
                    Remove(hash);
                }
            }
        }
    }
}
