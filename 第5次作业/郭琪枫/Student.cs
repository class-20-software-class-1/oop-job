﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp5
{
    class Student:IComparable<Student>
    {
        //1、学生类：学号、姓名、年龄
        //2、请选择：1、添加学生信息。2、查询学生信息，3、删除学生信息  4、退出
        //3、重复的学号不能添加。
        //4、查询学生信息功能中有：1、查询所有（按学号排序）2、按学号查询（查没有，则打印查无此学生）

        private int stuid;
        private int age;
        private string name;

        public int Stuid { get => stuid; set => stuid = value; }
        public int Age { get => age; set => age = value; }
        public string Name { get => name; set => name = value; }

        public Student(int Stuid,int Age,string Name)
        {
            this.Stuid = Stuid;
            this.Age = Age;
            this.Name = Name;
        }
                public int CompareTo(Student other)
        {
            Student s = (Student)other;
            return this.Stuid.CompareTo(other.Stuid);
        }

        public override string ToString()
        {
            return $"学号：{stuid} 年龄：{age} 姓名：{name} ";
        }
    }
}
