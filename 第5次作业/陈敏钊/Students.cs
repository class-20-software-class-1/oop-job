﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Student
    {
        private string stuno;
        private string name;
        private string age;

        public string Stuno { get => stuno; set => stuno = value; }
        public string Name { get => name; set => name = value; }
        public string Age { get => age; set => age = value; }

        public Student(string name,string age,string stuno)
        {
            Stuno = stuno;
            Name = name;
            Age = age;
        }

        public Student()
        {

        }

        public override string ToString()
        {
            return $"学号：{Num}，姓名：{Name}，年龄：{Age}";
        }

        public int CompareTo(Class other)
        {
            return this.Num.CompareTo(other.Num);
        }

    }
}
