﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Hashtable st = new Hashtable();
            TianJia(st);
            ChaXun(st);
            ShanChu(st);
        }
        public static void TianJia(Hashtable st)
        {
            Console.WriteLine("请输入学号");
            int id = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("请输入姓名");
            string name = Console.ReadLine();
            Console.WriteLine("请输入性别");
            string sex = Console.ReadLine();
            Students s1 = new Students(id, name, sex);
            if (!st.ContainsKey(id))
            {
                st[id] = s1;
            }
            else
            {
                Console.WriteLine("对不起，您添加的对象已存在");
            }
        }
        public static void ChaXun(Hashtable st)
        {
            Console.WriteLine("请输入你需要查询的方法:1.查询所有 2.按学号查询");
            int key = Convert.ToInt32(Console.ReadLine());
            if (key == 1)
            {
                foreach (Students a in st.Keys)
                {
                    st[a].GetType();
                }
            }
            else if (key == 2)
            {
                Console.WriteLine("请输入您需要查询的学号");
                int idkey = Convert.ToInt32(Console.ReadLine());
                if (st.ContainsKey(idkey))
                {
                    Console.WriteLine(st[idkey]);
                }
                else
                {
                    Console.WriteLine("对不起，您所查的学生不存在。");
                }

            }
        }
        public static void ShanChu(Hashtable st)
        {
            Console.WriteLine("请输入您要删除的学号");
            int idkey = Convert.ToInt32(Console.ReadLine());
            st.Remove(idkey);
        }
        
    }
}
