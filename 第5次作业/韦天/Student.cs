﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Students
    {
        private int _id;
        private string _name;
        private string _sex;

        public Students(int id, string name, string sex)
        {
            this.Id = id;
            this.Name = name;
            this.Sex = sex;
        }

        public int Id { get => _id; set => _id = value; }
        public string Name { get => _name; set => _name = value; }
        public string Sex { get => _sex; set => _sex = value; }

        public void Say()
        {
            Console.WriteLine("学号是{0}，姓名是{1}，性别{2}", this.Id, this.Name, this.Sex);
        }

    }
}
