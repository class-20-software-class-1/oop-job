﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Student
    {

        public string Name { get; set; }
        public int Num { get; set; }
        public int Age { get; set; }

        public Class(string name, int num, int age)
        {
            Name = name;
            Num = num;
            Age = age;
        }
        public Student()
        {

        }

        public override string ToString()
        {
            return $"学号：{Num}，姓名：{Name}，年龄：{Age}";
        }

        public int CompareTo(Student other)
        {
            return this.Num.CompareTo(other.Num);
        }
    }
}
