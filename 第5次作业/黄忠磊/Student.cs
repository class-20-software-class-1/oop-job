﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp12
{
    class Student
    {
        public int num { get; set; }
        public string name { get; set; }

        public Student()
        {

        }


        public int age { get; set; }

        public Student(int age, string name, int num)
        {
            this.name = name;
            this.age = age;
            this.num = num;
        }
        public override string ToString()
        {
            return $"学号：{num}，姓名：{name}，年龄：{age}";
        }
       
        }
    }


