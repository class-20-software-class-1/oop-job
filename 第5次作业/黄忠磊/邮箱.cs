﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ConsoleApp11
{
    class Program
    {
        static void Main(string[] args)
        {
            //           1、用户输入邮箱，请验证其合法性。
            //1、邮箱一定需要 @符号

            //   2、根据 @符号分为两部分，“前半部分 @ 后半部分”，
            //	前半部分可以数字、字母、下划线、中划线、 .（符号点）。但是.（符号点）不能 用在开头也不能用在结尾；

            //       后半部分可以数字、字母、下划线、中划线、.（符号点），且符号点是必须的，至少出现一次，但不能连续出现，且符号点不能在开头，也不能在结尾。
            //	后半部分的符号点后面只能是：com、org、net、edu、mil、tv、biz、info
            Console.WriteLine("请用户输入邮箱");
            string a = Console.ReadLine();
            
            if (Regex.IsMatch(a, @"^(\w)+@(\w)+(.(com|org|net|edu|mil|tv|bizinfo))$"))
            {
                Console.WriteLine("正确");
            }
            else
            {
                Console.WriteLine("错误");
            }
        }
    }
}
