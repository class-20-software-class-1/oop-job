﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//一个考试管理系统，需要录入考生成绩，只能录入数字，否则会报类型转换异常。
//请编写相关代码，
//1、捕获FormatException异常，并打印输出“异常已处理；
//2、捕获OverflowException异常，数值超出double范围的异常，并打印输出“异常已处理；
//3、捕获一般异常Exception异常。
//4、最终处理finally
//录入成绩结束后，请输出，总学生数，总分数，平均分。

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = new int[5];
            int ssum = 0;
            int sum = 0;
            double avg ;
            try
            {
               
                for (int i = 0; i < arr.Length; i++)
                {
                    Console.WriteLine("请输入第" + (i+1) + "个考生的分数");
                    arr[i] = int.Parse(Console.ReadLine());
                }
                for (int i = 0; i < arr.Length; i++)
                {
                    sum += arr[i];
                   
                }
                foreach (var item in arr)
                {
                    ssum++;
                }
                avg = sum / arr.Length;
                Console.WriteLine("总学生数:{0}，总分数:{1}，平均分:{2}", ssum, sum, avg);
            }
            catch (FormatException )
            {
                Console.WriteLine("异常已处理");
            }
            catch (OverflowException )
            {
                Console.WriteLine("异常已处理");
            }
            catch (Exception )
            {
                Console.WriteLine("异常已处理");
            }
            finally
            {
                Console.WriteLine("已处理");
            }
        }
    }
}
