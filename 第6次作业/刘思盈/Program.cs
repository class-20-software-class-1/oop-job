﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp20
{
    class Program
    {

        static void Main(string[] args)
        {
            A();
            

        }

        //        一个考试管理系统，需要录入考生成绩，只能录入数字，否则会报类型转换异常。
        //请编写相关代码，
        //1、捕获FormatException异常，并打印输出“异常已处理；
        //2、捕获OverflowException异常，数值超出double范围的异常，并打印输出“异常已处理；
        //3、捕获一般异常Exception异常。
        //4、最终处理finally
        //录入成绩结束后，请输出，总学生数，总分数，平均分。
        private static void A()
        {
            try
            {
                double[] a = new double[5];
                for (int i = 0; i < a.Length; i++)
                {
                    Console.WriteLine("请输入" + (i + 1) + "考生成绩：");
                    a[i] = double.Parse(Console.ReadLine());

                }
                int sum1 = 0;
                for (int i = 0; i < a.Length; i++)
                {
                    sum1++;
                }
                Console.WriteLine("总学生数{0}:", sum1);
                double sum2 = 0;
                for (int i = 0; i < a.Length; i++)
                {
                    sum2 += a[i];

                }
                Console.WriteLine("总分数{0}:", sum2);
                double avg = sum2 / a.Length;
                Console.WriteLine("平均分{0}:", avg);
            }
            catch (FormatException f)
            {
                Console.WriteLine("异常已处理");

            }
            catch (OverflowException o)
            {

                Console.WriteLine("异常已处理");
            }
            catch (Exception e)
            {
                Console.WriteLine("异常已处理");
            }
            finally
            {
                Console.WriteLine("最终处理完释放资源");
            }
        }
    }
}
