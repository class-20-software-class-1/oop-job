﻿using ConsoleApp5;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp5
{
    class age :IComparer<class1>
    {
        public int Compare(class1 x, class1 y)
        {
            return x.Age.CompareTo(y.Age);
        }
    }
}
