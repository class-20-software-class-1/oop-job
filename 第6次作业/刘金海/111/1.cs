﻿using System.Collections.Generic;

namespace Chp
{
    class AgeSort : IComparer<Studnet>
    {
        public int Compare(Studnet x, Studnet y)
        {
            return x.Age.CompareTo(y.Age);
        }
    }
}