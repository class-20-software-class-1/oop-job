﻿using System.Collections.Generic;

namespace Chp
{
    class NameSort : IComparer<Studnet>
    {
        public int Compare(Studnet x, Studnet y)
        {
            return x.Name.CompareTo(y.Name);
        }
    }
}