﻿using System.Collections.Generic;

namespace Chp
{
    class NumSort : IComparer<Studnet>
    {
        public int Compare(Studnet x, Studnet y)
        {
            return x.Num.CompareTo(y.Num);
        }
    }
}