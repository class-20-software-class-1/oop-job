﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Chp
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Studnet> list = new List<Studnet>();
            Hashtable hashtable = new Hashtable();


            Studnet s1 = new Studnet(1, "关羽", 28);
            Studnet s2 = new Studnet(2, "赵云", 25);

            list.Add(s1);
            list.Add(s2);

            hashtable.Add(s1.Num, s1);
            hashtable.Add(s2.Num, s2);
            HQ(hashtable, list);
        }

        private static void HQ(Hashtable hashtable, List<Studnet> list)
        {
            Console.WriteLine("1、添加学生信息      2、查询学生信息        3、删除学生信息 ");
            string key = Console.ReadLine();
            switch (key)
            {
                case "1":
                    add(hashtable, list);
                    break;

                case "2":
                    check(hashtable, list);
                    break;

                case "3":
                    drop(hashtable, list);
                    break;
                default:
                    break;
            }
        }

        private static void drop(Hashtable hashtable, List<Studnet> list)
        {
            hashtable.Clear();
            Console.WriteLine("数据删除成功");
            HQ(hashtable, list);
        }

        private static void check(Hashtable hashtable, List<Studnet> list)
        {
            IComparer<Studnet> comparer;
            Console.WriteLine("1、查询所有（按学号排序）    2、查询所有（按姓名排序）   3、查询所有（按年龄排序）   4、按学号查询（查没有，则打印查无此学生）   5、退出");
            string key = Console.ReadLine();
            switch (key)
            {
                case "1":

                    list.Sort(new NumSort());
                    foreach (var item in list)
                    {
                        Console.WriteLine(item);
                    }
                    HQ(hashtable, list);
                    break;
                case "2":
                    list.Sort(new NameSort());
                    foreach (var item in list)
                    {
                        Console.WriteLine(item);
                    }
                    HQ(hashtable, list);
                    break;
                case "3":
                    list.Sort(new NumSort());
                    foreach (var item in list)
                    {
                        Console.WriteLine(item);
                    }
                    HQ(hashtable, list);
                    break;
                case "4":
                    Console.WriteLine("请输入该学生的学号");
                    int num = int.Parse(Console.ReadLine());
                    foreach (Studnet item in hashtable)
                    {
                        if (hashtable.Contains(num) == true)
                        {
                            Console.WriteLine(item);
                            
                        }
                        else
                        {
                            Console.WriteLine("没有该学号的学生");
                            break;
                        }
                    } 
                    HQ(hashtable, list);
                    break;
                case "5":
                    Console.WriteLine("已退出");
                    break;
                default:
                    break;
            }
        }

        private static void add(Hashtable hashtable, List<Studnet> list)
        {
            Console.WriteLine("请输入该学生的学号");
            int num = int.Parse(Console.ReadLine());
            Console.WriteLine("请输入该学生的姓名");
            string name = Console.ReadLine();
            Console.WriteLine("请输入该学生的年龄");
            int age = int.Parse(Console.ReadLine());
            Studnet s = new Studnet(num, name, age);

            if (hashtable.Contains(num) == true)
            {
                Console.WriteLine("学号重复");
                
            }
            else
            {
                list.Add(s);
                Console.WriteLine("添加成功");
                hashtable.Add(s.Num, s);

            }

            HQ(hashtable, list);
        }
    }
    
    
    
}
   
    

