﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chp
{
    class Studnet 
    {
        public int Num { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }

        public Studnet(int num ,string name,int age) 
        {
            this.Num = num;
            this.Name = name;
            this.Age = age;
        }
        public Studnet() { }
        public override string ToString()
        {
            return $"学号：{Num}       姓名：{Name}       年龄：{Age}";
        }

     
    }
}
