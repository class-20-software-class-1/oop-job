﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Students
    {
        private double score;

        public double Score { get => score; set => score = value; }

        public Students() { }
        public Students(double score) 
        {
            this.score = score;
        }
    }
}
