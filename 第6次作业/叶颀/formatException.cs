﻿using System;
using System.Runtime.Serialization;

namespace ConsoleApp1
{
    [Serializable]
    internal class formatException : Exception
    {
        public formatException()
        {
        }

        public formatException(string message) : base(message)
        {
        }

        public formatException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected formatException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}