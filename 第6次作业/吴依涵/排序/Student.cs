﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Student
    {
        public int Xuehao { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }

        public Student(int xuehao, string name, int age)
        {
            this.Xuehao = xuehao;
            this.Age = age;
            this.Name = name;
        }
        public Student() { }

        public override string ToString()
        {
            return $"学号：{Xuehao} 姓名：{Name} 年龄：{Age}  ";
        }
    }
}
