﻿using System;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            double sum = 0;
            
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine("请输入考生的成绩");
                try
                {
                    double num = double.Parse(Console.ReadLine());
                    
                    sum = sum + num;
                }
                catch (FormatException)
                {
                    Console.WriteLine("FormatException 异常已处理");

                }
                catch (OverflowException)
                {
                    Console.WriteLine("OverflowException异常已处理");
                }
                catch (Exception) 
                {
                    Console.WriteLine("一般异常已捕获Exception");
                }
                finally
                {
                    

                }
                
            }
            Console.WriteLine(sum);
            
        }
    }
}
