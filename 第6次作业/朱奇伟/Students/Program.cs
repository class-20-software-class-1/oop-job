﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            //ArrayList list = new ArrayList();
            List<Student> list = new List<Student>();
            Hashtable ht = new Hashtable();
            Student s1 = new Student(1,"张三",18);
            Student s2 = new Student(2,"李四",20);
            Student s3 = new Student(3,"王五",19);

            list.Add(s1);
            list.Add(s2);
            list.Add(s3);


            ht.Add(s1.Num,s1);
            ht.Add(s2.Num, s2);
            ht.Add(s3.Num, s3);
            while (true)
            {
                Console.WriteLine("请选择：1、添加学生信息。2、查询学生信息，3、删除学生信息。");
                int numkey = int.Parse(Console.ReadLine());

                switch (numkey)
                {
                    case 1:
                        Console.WriteLine("请输入要添加的学生的学号");
                        int num = int.Parse(Console.ReadLine());

                        if (ht.ContainsKey(num) == true)
                        {
                            Console.WriteLine("该学号已被占用");
                        }
                        else 
                        {
                        Console.WriteLine("请输入要添加的学生的姓名");
                        string name = Console.ReadLine();
                        Console.WriteLine("请输入要添加的学生的年龄");
                        int age = Convert.ToInt32(Console.ReadLine());
                        Student student = new Student(num, name, age);
                        list.Add(student);
                        ht.Add(num, student);
                        }
                        break;
                    case 2:
                        while (true)
                        {
                            Console.WriteLine("请输入要选择的选项1、查询所有（按学号排序）2、查询所有（按姓名排序），3、查询所有（按年龄排序）4、按学号查询（查没有，则打印查无此学生）5、退出");
                            int num2 = int.Parse(Console.ReadLine());
                            if (num2 == 1)
                            {
                                list.Sort(new NumSort());
                                foreach (Student student1 in list)
                                {
                                    Console.WriteLine(student1);
                                }
                            }
                            else if (num2 == 2)
                            {
                                list.Sort(new NameSort());
                                foreach (Student student1 in list)
                                {
                                    Console.WriteLine(student1);
                                }
                            }
                            else if (num2 == 3)
                            {
                                list.Sort(new AgeSort());
                                foreach (Student student1 in list)
                                {
                                    Console.WriteLine(student1);
                                }
                            }
                            else if (num2 == 4)
                            {
                                Console.WriteLine("请输入要查询的学生的学号");
                                int num5 = int.Parse(Console.ReadLine());
                                if (ht.ContainsKey(num5)==false)
                                {
                                    Console.WriteLine("不存在该学号");
                                }
                                else
                                {
                                    Console.WriteLine(ht[num5]);
                                }

                            }
                            else if (num2 == 5)
                            {

                                break;
                            }
                            
                        }
                        break;
                    case 3:
                        Console.WriteLine("请输入要删除的学生学号");
                        int num6 = int.Parse(Console.ReadLine());
                        if (ht.ContainsKey(num6)==true)
                        {
                            ht.Remove(ht[num6]);
                            list.RemoveAt(num6);
                            
                            Console.WriteLine("删除成功");
                        }
                        else
                        {
                            Console.WriteLine("不存在该学生");
                        }
                        break;
                   
                    default:
                        Console.WriteLine("选项出错");
                        break;
                }
            }

            


        }
        
    }
}
