﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp3
{
    class NameSort:IComparer<Student>
    {
        public int Compare(Student x, Student y)
        {
            return x.Name.CompareTo(y.Name);
        }
    }
}
