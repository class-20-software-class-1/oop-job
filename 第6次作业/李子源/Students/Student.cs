﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace ConsoleApp3
{
    class Student:IComparable<Student>
    {
        private int num;
        private string name;
        private int age;

        public int Num { get; set; }

        public string Name 
        {
            get { return this.name; }
            set { this.name = value; }
        }

        public int Age { get; set; }

        public Student(int num,string name,int age) 
        {
            this.Name = name;
            this.Age = age;
            this.Num = num;
        }
        public override string ToString()
        {
            return $"学号为{Num},名字是{Name},年龄为{Age}";
        }

        public int CompareTo(Student other)
        {
            return 0;
        }
    }
}
