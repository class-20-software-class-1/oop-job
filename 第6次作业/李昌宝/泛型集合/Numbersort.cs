﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Numbersort : IComparer<Student>
    {
        public int Compare(Student x, Student y)
        {
            return x.Number.CompareTo(y.Number);
        }
    }
}
