﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp10
{
    class Program
    {
        static void Main(string[] args)
        {
            Hashtable hash = new Hashtable();

            Student s1 = new Student("东东", 1, 18);
            Student s3 = new Student("宝宝", 3, 20);
            Student s2 = new Student("鑫鑫", 2, 19);

            List<Student> list = new List<Student>() { s1, s2, s3 };
            
            hash.Add(s1.Num, s1);
            hash.Add(s3.Num, s3);
            hash.Add(s2.Num, s2);

            test1(hash, list);
        }
        public static void test1(Hashtable hash,List<Student> list)
        {
            Console.WriteLine("请选择：1、添加学生信息。2、查询学生信息，3、删除学生信息  4、退出");
            int str = int.Parse(Console.ReadLine());
            switch (str)
            {
                case 1:
                    Add(hash,list);
                    Console.WriteLine();
                    test1(hash, list);
                    break;
                case 2:
                    ChaXun(hash, list);
                    break;
                case 3:
                    Console.WriteLine("请输入要删除的学生信息");//存在BUG
                    int num =int.Parse(Console.ReadLine());
                    if (hash.ContainsKey(num))
                    {
                        foreach (Student i in hash.Values)
                        {
                            hash.Remove(i.Num);
                            Console.WriteLine("删除成功！");
                            Console.WriteLine();
                            test1(hash, list);
                        }
                    }
                    else
                    {
                        Console.WriteLine("不存在此学生！");
                        Console.WriteLine();
                        test1(hash, list);
                    }
                    
                    Console.WriteLine();
                    test1(hash, list);
                    break;
                case 4:
                    Console.WriteLine("退出成功！");
                    break;
            }
        }
        private static void ChaXun(Hashtable hash, List<Student> list)
        {
            Console.WriteLine();
            Console.WriteLine("1、查询所有（按学号排序）2、查询所有（按姓名排序），3、查询所有（按年龄排序）4、按学号查询（查没有，则打印查无此学生）5、退出");
            int str = int.Parse(Console.ReadLine());
            switch (str)
            {
                case 1:
                    list.Sort(new num());
                    foreach (Student i in list)
                    {
                        Console.WriteLine("学号：" + i.Num + "  姓名：" + i.Name + " 年龄：" + i.Age);
                    }
                    Console.WriteLine();
                    ChaXun(hash, list);
                    break;
                case 2:
                    list.Sort(new name());
                    foreach (Student i in list)
                    {
                        Console.WriteLine("学号：" + i.Num + "  姓名：" + i.Name + " 年龄：" + i.Age);
                    }
                    Console.WriteLine();
                    ChaXun(hash, list);
                    break;
                case 3:
                    list.Sort(new age());
                    foreach (Student i in list)
                    {
                        Console.WriteLine("学号：" + i.Num + "  姓名：" + i.Name + " 年龄：" + i.Age);
                    }
                    Console.WriteLine();
                    ChaXun(hash, list);
                    break;
                case 4:
                    Console.WriteLine("请输入学号进行查询学生");
                    int str1 = int.Parse(Console.ReadLine());
                    if (hash.ContainsKey(str1))
                    {
                        Console.WriteLine("无此学生");
                    }
                    else
                    {
                        Console.WriteLine("已找到该学生");
                    }
                    Console.WriteLine();
                    ChaXun(hash, list);
                    break;
                case 5:
                    Console.WriteLine("退出成功");
                    break;
                default:
                    ChaXun(hash, list);
                    break;
            }
        }

        public static void Add(Hashtable hash, List<Student> list)
        {
            Student s6 = new Student();
            Console.WriteLine("请输入姓名");
            s6.Name = Console.ReadLine();
            Console.WriteLine("请输入学号");
            s6.Num = int.Parse(Console.ReadLine());
            Console.WriteLine("请输入年龄");
            s6.Age = int.Parse(Console.ReadLine());
            
            if (hash.ContainsKey(s6.Num))
            {
                Console.WriteLine("已存在此学生");
            }
            else
            {
                list.Add(s6);
                hash.Add(s6.Num, s6);
                Console.WriteLine("添加成功！");
            }
        }
        
    }
}
