﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp10
{
    class Student
    {
        private String name;
        private int num;
        private int age;

        public Student()
        {
        }

        public Student(string name, int num, int age)
        {
            this.Name = name;
            this.Num = num;
            this.Age = age;
        }

        public string Name { get => name; set => name = value; }
        public int Num { get => num; set => num = value; }
        public int Age { get => age; set => age = value; }

        public override string ToString()
        {
            return "学号：" + Num + " 姓名：" + Name + "年龄：" + Age;
        }
    }
}
