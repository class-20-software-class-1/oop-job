﻿using System.Collections.Generic;

namespace ConsoleApp10
{
    internal class age : IComparer<Student>
    {
        public int Compare(Student x, Student y)
        {
            return x.Age.CompareTo(y.Age);
        }
    }
}