﻿using System.Collections.Generic;

namespace ConsoleApp10
{
    internal class name : IComparer<Student>
    {
        public int Compare(Student x, Student y)
        {
            return x.Name.CompareTo(y.Name);
        }
    }
}