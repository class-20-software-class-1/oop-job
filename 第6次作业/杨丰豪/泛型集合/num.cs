﻿using System.Collections.Generic;

namespace ConsoleApp10
{
    internal class num : IComparer<Student>
    {
        public int Compare(Student x, Student y)
        {
            return x.Num.CompareTo(y.Num);
        }
    }
}