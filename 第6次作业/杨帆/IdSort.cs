﻿using ConsoleApp1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{

    class IdSort : IComparer<Studnet>
    {
        public int Compare(Studnet x, Studnet y)
        {
            return x.Id.CompareTo(y.Id);
        }
    }
}
