﻿using ConsoleApp1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    
    class NameSort : IComparer<Studnet>
    {
        public int Compare(Studnet x, Studnet y)
        {
            return x.Name.CompareTo(y.Name);
        }
    }
}
