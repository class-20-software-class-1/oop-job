﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Studnet> list = new List<Studnet>();
            Hashtable h = new Hashtable();

            Studnet s1 = new Studnet("一", 18, 1);
            Studnet s2 = new Studnet("二", 19, 2);
            Studnet s3 = new Studnet("三", 20, 3);

            list.Add(s1);
            list.Add(s2);
            list.Add(s3);

            h.Add(s1.Id, s1);
            h.Add(s2.Id, s2);
            h.Add(s3.Id, s3);

            JKL(list, h);

        }
        static void JKL(List<Studnet> list, Hashtable h) 
        {
            Console.WriteLine("请选择：1、添加学生信息。2、查询学生信息，3、删除学生信息。");
            int a = int.Parse(Console.ReadLine());
            switch (a)
            {
                case 1:
                    Add(list, h);
                    break;
                case 2:
                    Select(list, h);
                    break;
                case 3:
                    Delete(list, h);
                    break;
            }
        }
        static void Add(List<Studnet> list, Hashtable h) 
        {
            Console.WriteLine("请输入学号：");
            int a = int.Parse(Console.ReadLine());

            Console.WriteLine("请输入姓名：");
            string b = Console.ReadLine();

            Console.WriteLine("请输入年龄：");
            int c = int.Parse(Console.ReadLine());

            Studnet st = new Studnet(b,c,a);
            if (h.Contains(st.Id))
            {
                Console.WriteLine("学号重复，请重新输入！");
                Add(list, h);
            }
            else
            {
                h.Add(st.Id,st);
                list.Add(st);
                Console.WriteLine("添加成功！");
                JKL(list,h);
            }

        }

        static void Select(List<Studnet> list, Hashtable h) 
        {
            Console.WriteLine("1、查询所有（按学号排序）2、查询所有（按姓名排序），3、查询所有（按年龄排序）4、按学号查询 5、退出");
            int a = int.Parse(Console.ReadLine());
            switch (a)
            {
                case 1:
                    list.Sort(new IdSort());
                    foreach (Studnet i in list)
                    {
                        Console.WriteLine(i);
                    }
                    Select(list,h);
                    break;
                case 2:
                    list.Sort(new NameSort());
                    foreach (Studnet i in list)
                    {
                        Console.WriteLine(i);
                    }
                    Select(list, h);
                    break;
                case 3:
                    list.Sort(new AgeSort());
                    foreach (Studnet i in list)
                    {
                        Console.WriteLine(i);
                    }
                    Select(list, h);
                    break;
                case 4:
                    Console.WriteLine("请输入学号：");
                    int asd = int.Parse(Console.ReadLine());
                    for (int i = 0; i < h.Count; i++)
                    {
                        if (h.ContainsKey(asd) == true) 
                        {
                            Console.WriteLine(h[asd]);
                            break;
                        }
                        else
                        {
                            Console.WriteLine("查无此学生!");
                        }
                    }
                    
                    Select(list, h);
                    break;
                case 5:
                    JKL(list,h);
                    break;
            }
        }
        static void Delete(List<Studnet> list, Hashtable h)
        {
            Console.WriteLine("请输入学号：");
            int asd = int.Parse(Console.ReadLine());
            for (int i = 0; i < h.Count; i++)
            {
                if (h.Contains(asd) == true)
                {
                    h.Remove(asd);
                    list.RemoveAt(asd);
                    Console.WriteLine("删除成功！");
                    break;
                }
                else
                {
                    Console.WriteLine("查无此学生!");
                }
            }
            JKL(list,h);
        }
    }
}
