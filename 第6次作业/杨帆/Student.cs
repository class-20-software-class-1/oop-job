﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Studnet
    {
        private string name;
        private int age;
        private int id;

        public string Name { get => name; set => name = value; }
        public int Age { get => age; set => age = value; }
        public int Id { get => id; set => id = value; }

        public Studnet(string name, int age,int id)
        {
            Name = name;
            Age = age;
            Id = id;
        }
        public Studnet()
        {

        }
        public override string ToString()
        {
            return $"学号：{id}  姓名：{name}  年龄：{age}  ";
        }
    }
}
