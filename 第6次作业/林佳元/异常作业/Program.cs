﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//一个考试管理系统，需要录入考生成绩，只能录入数字，否则会报类型转换异常。
//请编写相关代码，
//1、捕获FormatException异常，并打印输出“异常已处理；
//2、捕获OverflowException异常，数值超出double范围的异常，
//并打印输出“异常已处理；
//3、捕获一般异常Exception异常。
//4、最终处理finally
//录入成绩结束后，请输出，总学生数，总分数，平均分。
namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            double sum=0;
           
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine("请输入第"+(i+1)+"位考生成绩：");
                try
                {
                    double a = double.Parse(Console.ReadLine());
                    sum = sum + a;

                }
                catch (FormatException)
                {

                    Console.WriteLine("异常已处理！");
                }
                catch (OverflowException)
                {
                    Console.WriteLine("异常已处理！");
                }
                catch (Exception)
                {
                    Console.WriteLine("捕获一般异常Exception异常");
                }
                finally
                { }
            }
            Double avg = sum / 5;
            Console.WriteLine("总学生数为5，总分数为{1}，平均分为{1}",sum,avg);
        }
    }
}
