﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


//添加3个类，分别实现 IComparer<T>接口，实现对Student类的三个字段的排序。
//1、学生类：学号、姓名、年龄
//2、请选择：1、添加学生信息。2、查询学生信息，3、删除学生信息。
//3、重复的学号不能添加。
//4、查询学生信息功能中有：1、查询所有（按学号排序）
//  2、查询所有（按姓名排序），2、查询所有（按年龄排序）
//  4、按学号查询（查没有，则打印查无此学生）5、退出



namespace FanXingJiHe
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Student> list = new List<Student>();
            Hashtable hashtable = new Hashtable();
            Student stu1 = new Student(1,"小一", 18);
            Student stu2 = new Student(2,"小二", 19);
            Student stu3 = new Student(3,"小三", 20);

            list.Add(stu1);
            list.Add(stu2);
            list.Add(stu3);

            hashtable.Add(stu1.Num, stu1);
            hashtable.Add(stu2.Num, stu2);
            hashtable.Add(stu3.Num, stu3);
            while (true)
            {
                Console.WriteLine("请选择：1、添加学生信息。2、查询学生信息，3、删除学生信息!");
                int key1 = int.Parse(Console.ReadLine());

                switch (key1)
                {
                    case 1:
                        Console.WriteLine("请添加该学生的学号：");
                        int xuehao = int.Parse(Console.ReadLine());
                        if (hashtable.ContainsKey(xuehao) == true)
                        {
                            Console.WriteLine("该学号已被占用！");
                        }
                        else
                        {
                            Console.WriteLine("请输入学生姓名：");
                            string xingming = Console.ReadLine();
                            Console.WriteLine("请输入学生年龄：");
                            int nianling = Convert.ToInt32(Console.ReadLine());
                            Student student = new Student(xuehao, xingming, nianling);
                            list.Add(student);
                            hashtable.Add(xuehao, nianling);
                        }
                        break;
                    case 2:
                        while (true)
                        {
                            Console.WriteLine("请输入要选择的选项:1、查询所有（按学号排序）2、查询所有（按姓名排序），3、查询所有（按年龄排序）4、按学号查询（查没有，则打印查无此学生）5、退出");
                            int key2 = int.Parse(Console.ReadLine());
                            if (key2 == 1)
                            {
                                list.Sort(new NumSort());
                                foreach (Student student1 in list)
                                {
                                    Console.WriteLine(student1);
                                }
                            }
                            else if (key2 == 2)
                            {
                                list.Sort(new NameSort());
                                foreach (Student student1 in list)
                                {
                                    Console.WriteLine(student1);
                                }
                            }
                            else if (key2 == 3)
                            {
                                list.Sort(new AvgSort());
                                foreach (Student student1 in list)
                                {
                                    Console.WriteLine(student1);
                                }
                            }
                            else if (key2 == 4)
                            {
                                Console.WriteLine("请输入要查询学生的学号：");
                                int xuehao2 = int.Parse(Console.ReadLine());
                                if (hashtable.ContainsKey(xuehao2) == false)
                                {
                                    Console.WriteLine("不存在该学号！");
                                }
                                else
                                {
                                    Console.WriteLine(hashtable[xuehao2]);
                                }
                            }
                            else if (key2 == 5)
                            {
                                break;
                            }
                        }
                        break;
                    case 3:
                        Console.WriteLine("请输入要删除学生的学号：");
                        int xuehao3 = int.Parse(Console.ReadLine());
                        if (hashtable.ContainsKey(xuehao3) == true)
                        {
                            hashtable.Remove(hashtable[xuehao3]);
                            list.RemoveAt(xuehao3);
                            Console.WriteLine("删除成功！");
                        }
                        else
                        {
                            Console.WriteLine("不存在该学生！");
                        }
                        break;
                    default:
                        Console.WriteLine("请输入正确选项！");
                        break;
                }
            }
        }
    }
}
