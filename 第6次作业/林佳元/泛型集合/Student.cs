﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace FanXingJiHe
{
    class Student:IComparable<Student>
    {
        private int num;
        private string name;
        private int age;

        public int Num { get; set; }
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }
        public int Age { get; set; }

        public Student(int num, string name, int age)
        {
            this.Num = num;
            this.Name = name;
            this.Age = age;
        }
        public override string ToString()
        {
            return $"学号为{num}，学生的名字为{name}，年龄为{age}";
        }
        public int CompareTo(Student other)
        {
            return 0;
        }
    }
}
