﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Student> list = new List<Student>();

            Student s1 = new Student("魏六","s1",15);
            Student s2 = new Student("王五", "s2",18);
            Student s3 = new Student("李四", "s3",21);
            Student s4 = new Student("张三", "s4",17);

            list.Add(s1);
            list.Add(s2);
            list.Add(s3);
            list.Add(s4);

            for (int i = 0; i < list.Count; i++)
            {
                Console.WriteLine(list[i]);
            }
            Test(list);

        }
        static void Test(List<Student> list)
        {
            Console.WriteLine("请选择：1、添加学生信息。2、查询学生信息，3、删除学生信息。");
            int a = Convert.ToInt32(Console.ReadLine());
            switch (a)
            {
                case 1:
                    No1(list);
                    break;
                case 2:
                    No2(list);
                    break;
                case 3:
                    No3(list);
                    break;
                default:
                    Console.WriteLine("输入有误，重新输入");
                    Test(list);
                    break;
            }
        }
        static void No1(List<Student> list)
        {
            Console.WriteLine("请输入姓名");
            string name1 = Console.ReadLine();
            Console.WriteLine("请输入学号");
            string num1 = Console.ReadLine();
            Console.WriteLine(" 请输入年龄");
            int age1 = Convert.ToInt32(Console.ReadLine());

            for (int i = 0; i < list.Count; i++)
            {
                Student s_i = new Student(name1,num1,age1);
            }
        }
        static void No2(List<Student> list)
        {
            Console.WriteLine(" 请选择：1、查询所有（按学号排序）2、查询所有（按姓名排序）3、查询所有（按年龄排序）4、按学号查询（查没有，则打印查无此学生）");
            int a = Convert.ToInt32(Console.ReadLine());
            if (a==1)
            {
                list.Sort(new Num());
                foreach (var item in list)
                {
                    Console.WriteLine(item);
                }

                
            }

            if (a==2)
            {
                list.Sort(new Name());
                foreach (var item in list)
                {
                    Console.WriteLine(item);
                }
            }
            if (a==3)
            {
                list.Sort(new Age());
                foreach (var item in list)
                {
                    Console.WriteLine(item);
                }
            }
            if (a==4)
            {
                Console.WriteLine("请输入查询的学号：");
                string num = Console.ReadLine();
                foreach (var item in list)
                {
                    if (item.Num.Contains(num))
                    {
                        Console.WriteLine(item);
                    }
                }
            }
        }
        static void No3(List<Student> list)
        {
            Console.WriteLine("请输入要删除的学号：");
            string num = Console.ReadLine();
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Num.Equals(num))
                {
                    list.Remove(list[i]);
                }
            }
            Test(list);
        }
    }
}
