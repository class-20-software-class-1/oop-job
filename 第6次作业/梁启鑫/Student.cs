﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Student
    {
        protected string name;
        protected string num;
        protected int age;

        public Student (string name , string num, int age)
        {
            this.name = name;
            this.num = num;
            this.age = age;
        }

        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }
        public string Num
        {
            get { return this.num; }
            set { this.num = value; }
        }
        public int Age
        {
            get { return this.age; }
            set { this.age = value; }
        }
        public override string ToString()
        {
            return $"姓名：{name}  学号：{num} 年龄：{age}";
            
        }
    }
}
