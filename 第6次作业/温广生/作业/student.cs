﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class student:IComparable<student>
    {
        private int id;
        private string name;
        private int age;

        public student(int id, string name, int age)
        {
            this.id = id;
            this.name = name;
            this.age = age;
        }
        public int Age {
            get { return this.age; }
            set { this.age = value; }
        }
        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public student() { 
        }

        public int CompareTo(student other)
        {
           return this.id.CompareTo(other.id);
        }

        public override string ToString()
        {
            return $"学号：{id}，姓名：{name}，年龄：{age}";
        }
    }
}
