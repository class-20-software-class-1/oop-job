﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Student
    {
        private int stuno;
        private string name;
        private int age;

        public int Stuno { get => stuno; set => stuno = value; }
        public string Name { get => name; set => name = value; }
        public int Age { get => age; set => age = value; }

        public Student(string name, int age, int stuno)
        {
            Stuno = stuno;
            Name = name;
            Age = age;
        }

        
        public Student() { }
    }
}