﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Stuno : IComparer<Student>
    {
        public int Compare(Student x, Student y)
        {
            return x.Stuno.CompareTo(y.Stuno);
        }
    }
}
