﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            text1();
            }
        public static void text1() {
            Hashtable ht = new Hashtable();
            List<Student> list = new List<Student>();
            Student s1 = new Student(3, "雪", 20);
            Student s2 = new Student(4, "夜", 21);
            Student s3 = new Student(1, "风", 19);
            Student s4 = new Student(2, "花", 22);
            list.Add(s1);
            list.Add(s2);
            list.Add(s3);
            list.Add(s4);
            ht.Add(s1.Name, s1);
            ht.Add(s2.Name, s2);
            ht.Add(s3.Name, s3);
            ht.Add(s4.Name, s4);
            Console.WriteLine("请选择：1、添加学生信息。2、查询学生信息，3、删除学生信息");
            int s = int.Parse(Console.ReadLine());
            switch (s)
            {
                case 1:

                    Console.WriteLine("请输入名字");
                    string name = Console.ReadLine();
                    Console.WriteLine("请输入学号");
                    int num = int.Parse(Console.ReadLine());
                    Console.WriteLine("请输入年龄");
                    int age = int.Parse(Console.ReadLine());
                    Student student = new Student(num, name, age);
                    if (ht.ContainsKey(name))
                    {
                        Console.WriteLine("该学生已存在！");

                    }
                    else
                    {
                        ht.Add(name, name);
                        list.Add(student);

                    }

                    text1();

                    break;

                case 2:
                    while (true)
                    {
                        Console.WriteLine("请选择排序方式：1、按姓名排；2、按年龄排；3、按学号排；4、按学号查询（查没有，则打印查无此学生）5、退出");
                        int king = int.Parse(Console.ReadLine());
                        IComparer<Student> comparer;
                        switch (king)
                        {
                            case 1:
                                comparer = new Class2();
                                list.Sort(comparer);
                                foreach (Student ff in list)
                                {
                                    Console.WriteLine(ff);
                                }
                                break;
                            case 2:
                                comparer = new Class3();
                                list.Sort(comparer);
                                foreach (Student ff in list)
                                {
                                    Console.WriteLine(ff);
                                }
                                break;
                            case 3:
                                comparer = new Class1();
                                list.Sort(comparer);
                                foreach (Student ff in list)
                                {
                                    Console.WriteLine(ff);
                                }
                                break;

                            case 4:
                                Console.WriteLine("请输入要查找的学号：");
                                string no = Console.ReadLine();
                                if (ht.Contains(no))
                                {
                                    Console.WriteLine(ht[no]);
                                }
                                else
                                {
                                    Console.WriteLine("没有该学号！");
                                }
                                break;
                            case 5:
                                Environment.Exit(0);
                                break;
                            default:
                                comparer = new Class2();
                                break;
                        }

                    }
                    break;
                case 3:

                    Console.WriteLine("请输入要删除的学生的学号");
                    string num3 = Console.ReadLine();
                    foreach (Student item in ht.Values)
                    {
                        if (item.Num.Equals(num3))
                        {
                            ht.Remove(item.Num);
                            Console.WriteLine("删除成功");

                        }
                        else
                        {
                            Console.WriteLine("查无此学生");

                        }
                    }
                    text1();
                    break;
                default:
                    break;
            }




        }
    }
}
