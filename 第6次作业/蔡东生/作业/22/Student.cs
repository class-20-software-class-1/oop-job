﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Student : IComparable<Student>
    {
        public int Num { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }

        public Student(int num, string name, int age)
        {
            Num = num;
            Name = name;
            Age = age;
        }
        public Student() { 
        }

        public int CompareTo(Student other)
        {
            return this.Age.CompareTo(other.Age);
        }

        public override string ToString()
        {
            return $"学号：{Num}，姓名：{Name}，年龄：{Age}";
        }
    }
}
