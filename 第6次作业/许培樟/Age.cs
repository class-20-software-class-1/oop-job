﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Age : IComparer<Student>
    {
        public int Compare(Student x, Student y)
        {
            return x.age.CompareTo(y.age);
        }
    }
}
