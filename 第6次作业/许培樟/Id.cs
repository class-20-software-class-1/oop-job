﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Id : IComparer<Student>
    {
        public int Compare(Student x, Student y)
        {
            return x.id.CompareTo(y.id);
        }
    }
}
