﻿using ConsoleApp3;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static Hashtable ht = new Hashtable();
        static List<Student> students = new List<Student>();
        static void Main(string[] args)
        {
            //添加3个类，分别实现 IComparer<T>接口，实现对Student类的三个字段的排序。
            //1、学生类：学号、姓名、年龄
            //2、请选择：1、添加学生信息。2、查询学生信息。
            //3、重复的学号不能添加。
            //4、查询学生信息功能中有：1、查询所有（按学号排序）2、查询所有（按姓名排序），2、查询所有（按年龄排序）4、按学号查询（查没有，则打印查无此学生）5、退出
            Select1();


        }


        public static void Select1()
        {
            int key;
            Console.WriteLine("请选择：1、添加学生信息。2、查询学生信息。");
            key = int.Parse(Console.ReadLine());
            switch (key)
            {
                case 1:
                    StudentAdd();
                    break;
                case 2:
                    Select2();
                    break;
                default:
                    Console.WriteLine("选择出错！请重新选择");
                    Select1();
                    break;
            }
        }
        private static void StudentAdd()
        {
            int num;
            string name;
            int age;
            Console.Write("请输入学生学号：");
            num = int.Parse(Console.ReadLine());
            Console.Write("请输入学生姓名：");
            name = Console.ReadLine();
            Console.Write("请输入学生年龄：");
            age = int.Parse(Console.ReadLine());
            Student stu = new Student(num, name, age);
            if (ht.Contains(stu.id))
            {
                Console.WriteLine("重复的学号不能添加！请重新添加！");
                StudentAdd();
            }
            else
            {
                ht.Add(stu.id, stu);
                students.Add(stu);
                Console.Write("是否要继续添加（Y/N）");
                char key = char.Parse(Console.ReadLine());
                if (key == ('n' | 'N'))
                {
                    Select1();
                }
                else if (key == ('y' | 'Y'))
                {
                    StudentAdd();
                }
                else
                {
                    Console.WriteLine("输入出错自动返回上一级。");
                    Select1();
                }


            }

        }



        public static void Select2()
        {
            int key;
            Console.WriteLine("1、查询所有（按学号排序）2、查询所有（按姓名排序），3、查询所有（按年龄排序）4、按学号查询（查没有，则打印查无此学生）5、退出");
            key = int.Parse(Console.ReadLine());
            switch (key)
            {
                case 1:
                    students.Sort(new Id());
                    foreach (Student item in students)
                    {
                        Console.WriteLine(item);
                    }
                    break;
                case 2:
                    students.Sort(new Name());
                    foreach (Student item in students)
                    {
                        Console.WriteLine(item);
                    }
                    break;
                case 3:
                    students.Sort(new Age());
                    foreach (Student item in students)
                    {
                        Console.WriteLine(item);
                    }
                    break;
                case 4:
                    Console.Write("请输入你的学号：");
                    int Id = int.Parse(Console.ReadLine());
                    if (ht.Contains(Id))
                    {
                        Console.WriteLine(ht[Id]);
                    }
                    else
                    {
                        Console.WriteLine("查无此学生!!!");
                    }

                    break;
                case 5:
                    Environment.Exit(0);
                    break;
                default:
                    Console.WriteLine("选择出错！请重新选择");
                    Select2();
                    break;
            }
            Select2();

        }


    }
}