﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            //一个考试管理系统，需要录入考生成绩，只能录入数字，否则会报类型转换异常。
            //请编写相关代码，
            //1、捕获FormatException异常，并打印输出“异常已处理；
            //2、捕获OverflowException异常，数值超出double范围的异常，并打印输出“异常已处理；
            //3、捕获一般异常Exception异常。
            //4、最终处理finally
            //录入成绩结束后，请输出，总学生数，总分数，平均分。 



            try
            {
                ArrayList list = new ArrayList();
                int sum = 0;

                while (true)
                {
                    Console.WriteLine("请选择1.录入考生成绩 2.输出，总学生数，总分数，平均分");
                    int num = int.Parse(Console.ReadLine());
                    switch (num)
                    {
                        case 1:
                            Student student = new Student();
                            Console.WriteLine("请输入学生成绩");
                            int score = int.Parse(Console.ReadLine());
                            list.Add(student);
                            sum += score;
                            break;
                        case 2:
                            Console.WriteLine("总人数"+list.Count);
                            Console.WriteLine("总分数"+sum);
                            Console.WriteLine("平均分"+list.Count/sum);
                            break;
                    }
                   

                }
            }
            catch (FormatException F)//输入的数字格式不正确！
            {
                Console.WriteLine("异常已处理",F);
            }
            catch (OverflowException o)//输入的值已经超出 int 类型的最大值！
            {
                Console.WriteLine("异常已处理",o);
            }
            catch (Exception e)//父类
            {
                Console.WriteLine("异常已处理",e);
            }
            finally
            {
                Console.WriteLine("---------");
                
            }
            Console.ReadKey();
        }
    }
}
