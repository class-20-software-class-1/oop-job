﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static ArrayList list = new ArrayList();
        static double sum = 0;

        static void Main(string[] args)
        {
            Text();
        }


        public static void Text()
        {
            try
            {
                while (true)
                {

                    Console.WriteLine("请输入数字选择：1.输入成绩，2.输出成绩");
                    int key = int.Parse(Console.ReadLine());
                    switch (key)
                    {
                        case 1:
                            Console.WriteLine("请输入成绩");
                            double a = double.Parse(Console.ReadLine());
                            Console.WriteLine(a);
                            list.Add(a);
                            sum += a;
                            break;
                        case 2:
                            Console.WriteLine("平均分："+sum / list.Count);
                            Console.WriteLine("总分："+sum);
                            Console.WriteLine("总学生数："+list.Count);
                            break;
                        default:
                            break;
                    }


                }


            }
            catch (FormatException e)
            {
                Console.WriteLine("异常已处理");

            }
            catch (OverflowException e)
            {
                Console.WriteLine("异常已处理");
            }

            catch (Exception e)
            {
                Console.WriteLine("异常已处理");
            }
            finally
            {
                Console.WriteLine("最终处理完释放资源");
            }


            Console.ReadKey();
        }


    }
}
