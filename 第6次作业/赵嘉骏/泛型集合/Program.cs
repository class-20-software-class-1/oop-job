﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Dnmd();
        }

        static void Dnmd()
        {
            //ArrayList list = new ArrayList();
            List<Student> list = new List<Student>();
            Student one = new Student(1, "卢本伟", 20);
            Student two = new Student(2, "皇族white", 21);
            Student three = new Student(3, "五五开", 19);
            list.Add(one);
            list.Add(two);
            list.Add(three);

            ArrayList note = new ArrayList()
            {
                one,
                two,
                three
            };

            Console.WriteLine("请选择：1、添加学生信息。2、查询学生信息，3、删除学生信息  4、退出");
            int num = int.Parse(Console.ReadLine());
            switch (num)
            {
                case 1:
                    Student ass = new Student();
                    Console.WriteLine("请输入学生姓名");
                    ass.Name = Console.ReadLine();
                    Console.WriteLine("请输入学生年龄");
                    ass.Age = int.Parse(Console.ReadLine());
                    Console.WriteLine("请输入学号");
                    ass.Number = int.Parse(Console.ReadLine());
                    note.Add(ass);
                    break;

                case 2:


                    while (true)
                    {
                        Console.WriteLine("请选择功能：1、查询所有（按学号排序）；2、查询所有（按姓名排序）；3、查询所有（按年龄排序）；4、按学号查询");

                        int s = int.Parse(Console.ReadLine());
                        IComparer<Student> comparer;
                        switch (s)
                        {
                            case 1:
                                comparer = new Numbersort();
                                break;
                            case 2:
                                comparer = new Namesort();
                                break;
                            case 3:
                                comparer = new Agesort();
                                break;
                            
                            default:
                                comparer = new Namesort();
                                break;
                        }
                        list.Sort(comparer);

                        foreach (Student student in list)
                        {
                            Console.WriteLine(student);
                        }
                    }
                    break;


                case 3:
                    Console.WriteLine("已删除学生信息");
                    note.Clear();
                    break;

                case 4:
                    Console.WriteLine("已退出。");
                    Environment.Exit(0);
                    break;

                default:
                    Console.WriteLine("输入错误");
                    break;
            }

            

        }

    }
}
