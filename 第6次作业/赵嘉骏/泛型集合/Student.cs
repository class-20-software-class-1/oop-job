﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Student : IComparable<Student>
    {
        public int Number { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }

        public Student(int number, string name, int age)
        {
            Number = number;
            Name = name;
            Age = age;
        }

        public int CompareTo(Student other)
        {
            //return this.Age.CompareTo(other.Age);
            return other.Age.CompareTo(this.Age);
        }

        public override string ToString()
        {
            return $"学号：{Number}，姓名：{Name}，年龄：{Age}";
        }
        public Student()
        { }
    }
}
