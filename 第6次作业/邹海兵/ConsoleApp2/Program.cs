﻿using System;
using System.Collections;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            //ArrayList al = new ArrayList();
            Hashtable ht = new Hashtable();
            Student st = new Student();
            while (true)
            {
                try
                {
                    Console.WriteLine("请输入分数");
                    st.Score = int.Parse(Console.ReadLine());
                    Student s1 = new Student(st.Score);
                    ht.Add(st.Score, s1);
                    foreach (var it in ht.Keys)
                    { 
                        Console.WriteLine(it);
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("异常已处理");
                    throw;
                }
                catch (OverflowException)
                {
                    Console.WriteLine("异常已处理");
                }
                catch (Exception)
                {
                    Console.WriteLine("异常已处理");
                }
                finally
                {
                    int sum = 0;
                    int avg = 0;
                    foreach (int it in ht.Keys)
                    {
                        Console.WriteLine(it);
                        sum =sum+it;
                        avg = sum / ht.Count;
                    }
                    Console.WriteLine("总分："+sum);
                    Console.WriteLine("平均分"+ avg);
                }
            }
        }
    }
}
