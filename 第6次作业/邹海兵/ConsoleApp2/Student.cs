﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp2
{
    class Student
    {
        public int Score { get; set; }

        public Student(int score)
        {
            Score = score;
        }
        public Student()
        { }
    }
}
