﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P9
{
    class ID : IComparer<Student>
    {
        public int Compare(Student x, Student y)
        {
            return x.ID.CompareTo(y.ID);
        }
    }
}
