﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P9
{
    class Student
    {
        private int id;
        private string name;
        private int age;
        public int ID { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public int Age { get => age; set => age = value; }
        public Student()
        { 
        }
        public Student(int id,string name,int age)
        {
            this.Age = age;
            this.Name = name;
            this.ID = id;
        }
        public override string ToString()
        {
            return $"学号：{this.ID}姓名：{this.Name}年龄：{this.Age}";
        }
    }
}
