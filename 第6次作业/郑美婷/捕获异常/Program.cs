﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
//一个考试管理系统，需要录入考生成绩，只能录入数字，否则会报类型转换异常。
//请编写相关代码，
//1、捕获FormatException异常，并打印输出“异常已处理；
//2、捕获OverflowException异常，数值超出double范围的异常，并打印输出“异常已处理；
//3、捕获一般异常Exception异常。
//4、最终处理finally
//录入成绩结束后，请输出，总学生数，总分数，平均分。
        static void Main(string[] args)
        {
            double b = 0;
            double[] a = new double[5];
            while (true)
            {
                Console.WriteLine();
                Console.WriteLine("______________________________________________");
                Console.WriteLine();
                #region
                try
            {
                for (int i = 0; i < 5; i++)
                {
                    Console.WriteLine("请输入第位" + (i + 1) + "考生成绩：");
                    a[i] = double.Parse(Console.ReadLine());
                }
                for (int i = 0; i < a.Length; i++)
                {
                    b += a[i];
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("FormatException异常已处理");
            }
            catch (OverflowException)
            {
                Console.WriteLine("OverflowException异常已处理");
            }
            catch (Exception)
            {
                Console.WriteLine("异常已成功捕捉处理！");
            }
            finally
            {
                Console.WriteLine("捕捉异常！");
            }
            Console.WriteLine("总学生数:{0}，总分数:{1}，平均分:{2}",a.Length,b,b/a.Length);
                Console.WriteLine();
                Console.WriteLine("______________________________________________");
                Console.WriteLine();
                #endregion
            }
        }
    }
}
