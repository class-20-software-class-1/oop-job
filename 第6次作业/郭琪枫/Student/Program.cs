﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    class Program
    {
        static void Main(string[] args)
        {
            //添加3个类，分别实现 IComparer<T>接口，实现对Student类的三个字段的排序。
            //1、学生类：学号、姓名、年龄
            //2、请选择：1、添加学生信息。2、查询学生信息，3、删除学生信息。
            //3、重复的学号不能添加。
            //4、查询学生信息功能中有：
            //1、查询所有（按学号排序）
            //2、查询所有（按姓名排序），
            //2、查询所有（按年龄排序）
            //4、按学号查询（查没有，则打印查无此学生）
            //5、退出
            List<Student> list = new List<Student>();
            Hashtable hashtable = new Hashtable();
            Student student = new Student(1, "学生1号", 20);
            Student student1 = new Student(2, "学生2号", 21);
            list.Add(student);
            list.Add(student1);
            hashtable.Add(student.Stuid, student);
            hashtable.Add(student1.Stuid, student1);
            Test(hashtable);

        }

        static void Test(Hashtable hashtable)
        {
            Console.WriteLine("请选择：1、添加学生信息。2、查询学生信息，3、删除学生信息。");
            int num = int.Parse(Console.ReadLine());
            switch (num)
            {
                case 1:
                    Add(hashtable, list);
                    break;
                case 2:
                    Check(hashtable, list);
                    break;
                case 3:
                    Remove(hashtable, list);
                    break;
                default:
                    break;
            }
        }

        private static void Add(Hashtable hashtable, List<Student> list)
        {
            Console.WriteLine("请输入你的学号：");
            int a = int.Parse(Console.ReadLine());
            Console.WriteLine("请输入你的姓名：");
            string b = Console.ReadLine();
            Console.WriteLine("请输入你的年龄：");
            int c = int.Parse(Console.ReadLine());
            Student student3 = new Student(stuid, name, age);
            hashtable.Add(student3.Stuid, student3);
            Console.WriteLine("学生添加成功！");
            Test(hashtable);
        }
        private static void Check(Hashtable hashtable, List<Student> list)
        {
            IComparer<Student> comparer;
            Console.WriteLine("1、查询所有（按学号排序）2、查询所有（按姓名排序）3、查询所有（按年龄排序）4、按学号查询 5、退出");
            string key = Console.ReadLine();
            switch (key)
            {
                case "1":

                    list.Sort(new StuidSort());
                    foreach (var item in list)
                    {
                        Console.WriteLine(item);
                    }
                    break;
                case "2":
                    list.Sort(new NameSort());
                    foreach (var item in list)
                    {
                        Console.WriteLine(item);
                    }
                    break;
                case "3":
                    list.Sort(new AgeSort());
                    foreach (var item in list)
                    {
                        Console.WriteLine(item);
                    }
                    break;
                case "4":

                    break;
                case "5":
                    Console.WriteLine("已退出");
                    break;
                default:
                    break;
            }
        }
        private static void Remove(Hashtable hashtable, List<Student> list)
        {
            hashtable.Clear();
            Console.WriteLine("学生删除成功！");
            Test(hashtable);
        }




    }
}
