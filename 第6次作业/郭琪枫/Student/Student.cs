﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    class Student
    {
        public int Stuid { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }

        public Student(int stuid, string name, int age)
        {
            this.Stuid = stuid;
            this.Age = age;
            this.Name = name;
        }
        public Student() { }

        public override string ToString()
        {
            return $"学号：{Stuid} 姓名：{Name} 年龄：{Age}  ";
        }
    }
}
