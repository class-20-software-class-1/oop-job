﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    class StuidSort : IComparable<Student>
    {
        public int Compare(Student x, Student y)
        {
            return x.Stuid.CompareTo(y.Stuid);
        }
    }
}
