﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            text();
        }

        static void text()
        {
            //ArrayList list = new ArrayList();
            List<Student> list = new List<Student>();
            Student one = new Student(1, "2", 3);
            Student two = new Student(1, "张三", 3);
            Student three = new Student(3, “李四", 3);
            list.Add(one);
            list.Add(two);
            list.Add(three);

            ArrayList note = new ArrayList()
            {
                one,
                two,
                three
            };

            Console.WriteLine("请选择：1、添加学生信息。2、查询学生信息，3、删除学生信息  4、退出");
            int num = int.Parse(Console.ReadLine());
            switch (num)
            {
                case 1:
                    Student t = new Student();
                    Console.WriteLine("请输入学生姓名");
                    t.Name = Console.ReadLine();
                    Console.WriteLine("请输入学生年龄");
                    t.Age = int.Parse(Console.ReadLine());
                    Console.WriteLine("请输入学号");
                    t.Number = int.Parse(Console.ReadLine());
                    note.Add(t);
                    break;

                case 2:


                    while (true)
                    {
                        Console.WriteLine("请选择功能：1、查询所有（按学号排序）；2、查询所有（按姓名排序）；3、查询所有（按年龄排序）；4、按学号查询");

                        int s = int.Parse(Console.ReadLine());
                        IComparer<Student> comparer;
                        switch (s)
                        {
                            case 1:
                                comparer = new Numbersort();
                                break;
                            case 2:
                                comparer = new Namesort();
                                break;
                            case 3:
                                comparer = new Agesort();
                                break;
                            
                            default:
                                comparer = new Namesort();
                                break;
                        }
                        list.Sort(comparer);

                        foreach (Student student in list)
                        {
                            Console.WriteLine(student);
                        }
                    }
                    break;


                case 3:
                    Console.WriteLine("已删除学生信息");
                    note.Clear();
                    break;

                case 4:
                    Console.WriteLine("已退出。");
                    Environment.Exit(0);
                    break;

                default:
                    Console.WriteLine("输入错误");
                    break;
            }

            

        }

    }
}
