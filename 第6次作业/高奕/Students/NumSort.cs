﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace ConsoleApp3
{
    class NumSort : IComparer<Student>
    {
        public int Compare( Student x,  Student y)
        {
            return x.Num.CompareTo(y.Num);
        }
    }
}
