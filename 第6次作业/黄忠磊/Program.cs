﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {

            //            一个考试管理系统，需要录入考生成绩，只能录入数字，否则会报类型转换异常。
            //请编写相关代码，
            //1、捕获FormatException异常，并打印输出“异常已处理；
            //2、捕获OverflowException异常，数值超出double范围的异常，并打印输出“异常已处理；
            //3、捕获一般异常Exception异常。
            //4、最终处理finally
            //录入成绩结束后，请输出，总学生数，总分数，平均分。
            Console.WriteLine("<-----------考试管理系统--------------<");
            Console.WriteLine();
            Console.WriteLine("录入考生成绩");

            int a = 0;
            int sum = 0;
            int c = 0;

            for (int i = 0; i < 5; i++)
            {
              
                try
                {
                    a = int.Parse(Console.ReadLine());
                    sum += a;
                    c++;
                    Console.WriteLine(a);
                }

                catch (FormatException e)
                {

                    Console.WriteLine("FormatException异常已处理");
                    Console.WriteLine(e.Message);
                    Console.WriteLine(e.StackTrace);

                }
                catch (OverflowException w)
                {

                    Console.WriteLine("OverflowException异常已处理");
                    Console.WriteLine(w.Message);
                    Console.WriteLine(w.StackTrace);
                }

                catch (Exception b)
                {

                    Console.WriteLine("Exception异常已处理");
                    Console.WriteLine(b.Message);
                    Console.WriteLine(b.StackTrace);
                }
                finally
                {
                    Console.WriteLine("学生成绩已录入");
                }
            }
            double avg = (double)sum / c;
            Console.WriteLine("学生总人数" + c);
            Console.WriteLine("学生总成绩" + sum);
            Console.WriteLine("学生平均分" + avg);

        }
    }
        
    }
    

